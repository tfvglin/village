package edu.xidian.village.dao;

import edu.xidian.village.vo.Admin;

public interface AdminDao {

	public Admin getAdmin(String name)throws Exception;
	
	public Admin getAdmin();
	
	public Admin  get(String name);
}
