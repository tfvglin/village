package edu.xidian.village.dao;

import java.util.List;

import edu.xidian.village.vo.Message;

public interface MessageDao {

	//void add(Message message);
	Integer add(Message message);
	void update(Message message);
	void merge(Message message);
	void flush();
	Message get(int mid);
	List<Message> list(int num,int offset);
	List<Message> list();
	
}
