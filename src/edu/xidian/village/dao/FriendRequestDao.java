package edu.xidian.village.dao;

import java.util.List;

import edu.xidian.village.vo.FriendRequest;

public interface FriendRequestDao {
	
	void add(FriendRequest friendRequest);

	List<FriendRequest> list(int senduserid);
	
	FriendRequest get(int frid);
	
	FriendRequest get(FriendRequest friendRequest);
	
	void update(FriendRequest friendRequest);

	FriendRequest merge(FriendRequest friendRequest);
	
	int delete(String where);
	
	void flush();
}
