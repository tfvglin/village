package edu.xidian.village.dao;

import java.io.Serializable;
import java.util.List;

import edu.xidian.village.vo.Notification;

public interface NotificationDao<T> {
	
	List<Notification> get(int n) throws Exception;
	List<Notification> get() ;
	List<Notification> get(String whereHql) ;
	
	Serializable add(T t);

	List<Notification> get(int n, int offset) throws Exception;
	
	Notification get(Class<Notification> c,int id);
	
	public void delet(Notification no);

	public void delet(List<Notification> nolist);

	public int delete(String where);
	
	public void update(Notification no);
	
	public Notification merge(Notification notification);
	
	long count();
	
	void flush();
}
