package edu.xidian.village.dao;

import java.util.List;

import edu.xidian.village.vo.RentHouse;

public interface RentHouseDao {

	public void add();
	public void add(RentHouse rentHouse);
	public RentHouse get(int rid);
	public List<RentHouse> list();
	public List<RentHouse> list(int num,int offset);
	public void delete(int rid);
	public RentHouse merge(RentHouse rentHouse);
	public void flush();
	
}
