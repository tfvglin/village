package edu.xidian.village.dao;

import java.io.Serializable;
import java.util.List;

import edu.xidian.village.vo.User;

public interface UserDao {
	
	
	public List<User> find(String name);
	
	public Serializable add(User user);
	
	public List<User> list(User user);
	
	public List<User> list();

	public List<User> list(String whereHql);

	public void update(User user);

	public void merge(User user);
	
	public User get(Class<? extends User> c,int uid);
	public User get(String username);
	
	public void delete(User user);
	
	public void delete(Integer id);
	
	public void delete(List<User> list);
	
	public User load(Class<User> c,Integer id);
	
	User meger(User user);
	void flush();
	
	User load(int uid);
	
	User search(String propertyName,String value);
	
	long count();
	long activeCount(Long date);
	
	public int delete(String where);
}
