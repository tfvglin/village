package edu.xidian.village.dao.impl;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.stereotype.Repository;

import edu.xidian.village.dao.MessageDao;
import edu.xidian.village.template.VillageHibernateTemplate;
import edu.xidian.village.util.PageUtil;
import edu.xidian.village.vo.Message;
@Repository
public class MessageDaoImpl extends VillageHibernateTemplate implements MessageDao{

	@Override
	public Integer add(Message message) {
		return (Integer) this.getHibernateTemplate().save(message);
		
	}

	@Override
	public void update(Message message) {
		this.getHibernateTemplate().update(message);
		
	}

	@Override
	public void merge(Message message) {
		this.getHibernateTemplate().merge(message);
		
	}

	@Override
	public void flush() {
		this.getHibernateTemplate().flush();
		
	}

	@Override
	public Message get(int mid) {
		// TODO Auto-generated method stub
		return this.getHibernateTemplate().get(Message.class, mid);
	}

	@Override
	public List<Message> list(final int num, final int offset) {
		// TODO Auto-generated method stub
		final String hql="from Message";
				return  getHibernateTemplate().execute(new HibernateCallback<List<Message>>()
						{
							 @Override
							public List<Message> doInHibernate(Session session)throws HibernateException, SQLException 
							 {
								 List<Message> list2 = PageUtil.getList(session,hql,offset,num);
						        return list2;
							 }
						});
	}
	
	@Override
	public List<Message> list() {
		// TODO Auto-generated method stub
		return this.getHibernateTemplate().find("from Message");
	}

}
