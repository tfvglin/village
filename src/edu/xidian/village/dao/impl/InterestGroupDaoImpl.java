package edu.xidian.village.dao.impl;

import java.sql.SQLException;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.stereotype.Repository;

import edu.xidian.village.dao.InterestGroupDao;
import edu.xidian.village.template.VillageHibernateTemplate;
import edu.xidian.village.util.PageUtil;
import edu.xidian.village.vo.InterestGroup;

@Repository
public class InterestGroupDaoImpl extends VillageHibernateTemplate implements InterestGroupDao{

	@Override
	public void add(InterestGroup interestGroup) {
		this.getHibernateTemplate().save(interestGroup);
		
	}

	@Override
	public InterestGroup get(InterestGroup group) {
		return this.getHibernateTemplate().get(InterestGroup.class, group.getId());
	}

	@Override
	public InterestGroup get(int igid) {
		return this.getHibernateTemplate().get(InterestGroup.class, igid);
	}

	

	@Override
	public void update(InterestGroup interestGroup) {
		this.getHibernateTemplate().update(interestGroup);
		
	}

	@Override
	public InterestGroup merge(InterestGroup interestGroup) {
		return 	this.getHibernateTemplate().merge(interestGroup);
	}

	@Override
	public void flush() {
		this.getHibernateTemplate().flush();
		
	}

	@Override
	public List<InterestGroup> list() {
		// TODO Auto-generated method stub
		return this.getHibernateTemplate().find("from InterestGroup");
	}

	@Override
	public InterestGroup getBase(int igid) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<InterestGroup> list(final int n, final int offset) {
		final String hql = " from InterestGroup order by datetime desc";
		/*select new Notification(id,message,datetime,picture,title)*/
	
	return  getHibernateTemplate().execute(new HibernateCallback<List<InterestGroup>>()
	{
		 @Override
		public List<InterestGroup> doInHibernate(Session session)throws HibernateException, SQLException 
		 {
			 List<InterestGroup> list2 = PageUtil.getList(session,hql,offset,n);
	        return list2;
		 }
	});
	}

	@Override
	public InterestGroup find(String propertyName, String value) {
		String hql = "from InterestGroup where "+propertyName+"=\'"+value+"\'";
		List<InterestGroup> list = this.getHibernateTemplate().find(hql);
		if(list.isEmpty())
			return null;
		else
			return list.get(0);
	}

	@Override
	public InterestGroup get(String gid) {
		String hql = "from InterestGroup where gid =?";
		List<InterestGroup> list = this.getHibernateTemplate().find(hql,gid);
		if(list.isEmpty())
			return null;
		else
			return list.get(0);
		
	}

	

}
