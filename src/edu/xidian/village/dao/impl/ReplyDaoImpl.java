package edu.xidian.village.dao.impl;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.stereotype.Repository;

import edu.xidian.village.dao.ReplyDao;
import edu.xidian.village.template.VillageHibernateTemplate;
import edu.xidian.village.util.PageUtil;
import edu.xidian.village.vo.Reply;
@Repository
public class ReplyDaoImpl extends VillageHibernateTemplate implements ReplyDao{

	@Override
	public void add(Reply reply) {
		this.getHibernateTemplate().save(reply);
		
	}

	@Override
	public void get(int rid) {
		this.getHibernateTemplate().get(Reply.class, rid);
		
	}

	@Override
	public void get(Reply reply) {
		this.getHibernateTemplate().get(Reply.class, reply.getId());
		
	}

	@Override
	public List<Reply> list() {
		
		return this.getHibernateTemplate().find("from Reply");
	}

	@Override
	public List<Reply> list(final int n, final int offset,int tid) {
		final String hql = " from Reply where topic_id=\'"+tid+"\' order by datetime desc";
		/*select new Notification(id,message,datetime,picture,title)*/
	
	return  getHibernateTemplate().execute(new HibernateCallback<List<Reply>>()
	{
		 @Override
		public List<Reply> doInHibernate(Session session)throws HibernateException, SQLException 
		 {
			 List<Reply> list2 = PageUtil.getList(session,hql,offset,n);
	       System.out.print(list2.size());
			 return list2;
		 }
	});
	}

	@Override
	public void update(Reply reply) {
		this.getHibernateTemplate().update(reply);
		
	}

	@Override
	public Reply merge(Reply reply) {
		// TODO Auto-generated method stub
		return this.getHibernateTemplate().merge(reply);
	}

	@Override
	public void flush() {
		this.getHibernateTemplate().flush();
		
	}

	@Override
	public int delete(String where) {
		String hql = "delete Reply where id in"+where;
		return this.getHibernateTemplate().bulkUpdate(hql);
	}

}
