package edu.xidian.village.dao.impl;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.stereotype.Repository;

import edu.xidian.village.dao.WantedHouseDao;
import edu.xidian.village.template.VillageHibernateTemplate;
import edu.xidian.village.util.PageUtil;
import edu.xidian.village.vo.WantedHouse;
@Repository
public class WantedHouseDaoImpl extends VillageHibernateTemplate implements WantedHouseDao{

	@Override
	public void add() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void add(WantedHouse wantedHouse) {
		this.getHibernateTemplate().save(wantedHouse);
		
	}

	@Override
	public void delete(WantedHouse wantedHouse) {
		this.getHibernateTemplate().delete(wantedHouse);
		
	}

	@Override
	public List<WantedHouse> list() {
		// TODO Auto-generated method stub
		return this.getHibernateTemplate().find("from WantedHouse");
	}

	@Override
	public List<WantedHouse> list(final int num, final int offset) {
		final String hql = "from WantedHouse order by datetime desc";
		return  getHibernateTemplate().execute(new HibernateCallback<List<WantedHouse>>()
				{
					 @Override
					public List<WantedHouse> doInHibernate(Session session)throws HibernateException, SQLException 
					 {
						 List<WantedHouse> list2 = PageUtil.getList(session,hql,offset,num);
				        return list2;
					 }
				});		
	}

	@Override
	public WantedHouse merge(WantedHouse wantedHouse) {
		// TODO Auto-generated method stub
		return this.getHibernateTemplate().merge(wantedHouse);
	}

	@Override
	public void flush() {
		this.getHibernateTemplate().flush();
		
	}

}
