package edu.xidian.village.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import edu.xidian.village.dao.FriendRequestDao;
import edu.xidian.village.template.VillageHibernateTemplate;
import edu.xidian.village.vo.FriendRequest;
@Repository("friendRequestDaoImpl")
public class FriendRequestDaoImpl extends VillageHibernateTemplate implements FriendRequestDao{

	@Override
	public void add(FriendRequest friendRequest) {
		this.getHibernateTemplate().save(friendRequest);
		this.getHibernateTemplate().flush();
		
	}

	@Override
	public List<FriendRequest> list(int senduserid) {
		
		return null;
	}

	@Override
	public void update(FriendRequest friendRequest) {
		this.getHibernateTemplate().update(friendRequest);
		
		
	}

	@Override
	public FriendRequest merge(FriendRequest friendRequest) {
		return this.getHibernateTemplate().merge(friendRequest);
		
	}

	@Override
	public void flush() {
		this.getHibernateTemplate().flush();
		
	}

	@Override
	public FriendRequest get(int frid) {
		
		return this.getHibernateTemplate().get(FriendRequest.class, frid);
	}

	@Override
	public FriendRequest get(FriendRequest friendRequest) {
		
		return this.getHibernateTemplate().get(FriendRequest.class, friendRequest.getId());
	}

	@Override
	public int delete(String where) {
		String hql = "delete FriendRequest where user_receiver_id in "+where;
		
		return this.getHibernateTemplate().bulkUpdate(hql);
	}

}
