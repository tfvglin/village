package edu.xidian.village.dao.impl;

import java.io.Serializable;
import java.util.List;

import org.springframework.stereotype.Repository;

import edu.xidian.village.dao.UserDao;
import edu.xidian.village.template.VillageHibernateTemplate;
import edu.xidian.village.vo.User;

@Repository("userDAOImpl")
public class UserDaoImpl extends VillageHibernateTemplate implements UserDao{

	
	@Override
	@SuppressWarnings("unchecked")
	public  List<User> find(String name) {
		String hql = "from User where name = ?";
		return getHibernateTemplate().find(hql, name);
	}

	
	@Override
	public Serializable add(User user) {
		return	getHibernateTemplate().save(user);
	}

	
	@Override
	public List<User> list(User user){
		String hql = "from User where name=? and password = ?";
		return getHibernateTemplate().find(hql, user.getName(),user.getPassword());
	}


	@Override
	public void update(User user) {
		getHibernateTemplate().update(user);
		this.getHibernateTemplate().flush();
	}


	@Override
	public User get(Class<? extends User> c, int uid) {
		return getHibernateTemplate().get(User.class, uid);
		
	}


	@Override
	public List<User> list() {
		return getHibernateTemplate().find("from User");
	}


	@Override
	public List<User> list(String whereHql) {
		return getHibernateTemplate().find("from User"+whereHql);

	}


	@Override
	public void delete(User user) {
	
			this.getHibernateTemplate().delete(user);
			getHibernateTemplate().flush();
		
		
	}


	@Override
	public void delete(Integer id) {
		
		
	}


	@Override
	public User load(Class<User> c, Integer id) {
		return this.getHibernateTemplate().load(User.class, id);
	}


	@Override
	public void delete(List<User> list) {
		this.getHibernateTemplate().deleteAll(list);
		this.getHibernateTemplate().flush();
		
	}


	@Override
	public void merge(User user) {
		this.getHibernateTemplate().merge(user);
		
	}


	@Override
	public User meger(User user) {
		return this.getHibernateTemplate().merge(user);
	}


	@Override
	public void flush() {
		this.getHibernateTemplate().flush();
		
	}


	@Override
	public User load(int uid) {
		return  this.getHibernateTemplate().load(User.class, uid);
	}


	@Override
	public User search(String propertyName, String value) {
		String hql = "from User where "+propertyName+"=\'"+value+"\'";
		List<User> list = this.getHibernateTemplate().find(hql);
		if(list.isEmpty())
			return null;
		else
			return list.get(0);
	}


	@Override
	public long count() {
		List list =this.getHibernateTemplate().find("select count(*) from User");
		
		return (Long) list.get(0);
	}


	@Override
	public User get(String username) {
	
		String hql = "from User where name=?";
		List list = this.getHibernateTemplate().find(hql, username);
		if(list.isEmpty())
		{
		
			return null;
		}
		else
			return (User) list.get(0);
	}


	@Override
	public long activeCount(Long date) {
		List list =this.getHibernateTemplate().find("select count(*) from User where lasttime>?",date);
		
		return (Long) list.get(0);
	}


	@Override
	public int delete(String where) {
		// TODO Auto-generated method stub
		String hql = "delete User where id in "+where;
		return this.getHibernateTemplate().bulkUpdate(hql);
	}




	
	

}
