package edu.xidian.village.dao.impl;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.stereotype.Repository;

import edu.xidian.village.dao.NotificationDao;
import edu.xidian.village.template.VillageHibernateTemplate;
import edu.xidian.village.util.PageUtil;
import edu.xidian.village.vo.Notification;

@Repository("notificationDaoImpl")
public class NotificationDaoImpl<T> extends VillageHibernateTemplate implements NotificationDao<T> {

	@SuppressWarnings("unchecked")
	@Override
	public List<Notification> get(final int n,final int offset)throws Exception {
		
		final String hql = "select new Notification(id,message,datetime,picture,title) from Notification order by datetime desc";
		
		
		return  getHibernateTemplate().execute(new HibernateCallback<List<Notification>>()
		{
			 @Override
			public List<Notification> doInHibernate(Session session)throws HibernateException, SQLException 
			 {
				 List<Notification> list2 = PageUtil.getList(session,hql,offset,n);
		        return list2;
			 }
		});
	
	}

	@Override
	public Serializable add(T t) {
		
		return getHibernateTemplate().save(t);
	}

	@Override
	public List<Notification> get(int n) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Notification get(Class<Notification> c, int id) {
		return getHibernateTemplate().get(c, id);
		
	}

	@Override
	public List<Notification> get()  {
		// TODO Auto-generated method stub
		return getHibernateTemplate().find("from Notification");
	}

	@Override
	public List<Notification> get(String whereHql) {
		return	getHibernateTemplate().find("from Notification"+whereHql);
		 
	}

	@Override
	public void delet(Notification no) {
		getHibernateTemplate().delete(no);
		getHibernateTemplate().flush();
		
	}



	@Override
	public void delet(List<Notification> nolist) {
		
		this.getHibernateTemplate().deleteAll(nolist);
		this.getHibernateTemplate().flush();
		
	}

	@Override
	public void update(Notification no) {
		this.getHibernateTemplate().update(no);
		
	}

	@Override
	public Notification merge(Notification notification) {
		// TODO Auto-generated method stub
		return this.getHibernateTemplate().merge(notification);
	}

	@Override
	public void flush() {
		this.getHibernateTemplate().flush();
		
	}

	@Override
	public long count() {
		List list =this.getHibernateTemplate().find("select count(*) from Notification");
		return (Long) list.get(0);
	}

	@Override
	public int delete(String where) {
		String hql = "delete Notification where id in "+where;
		
		return this.getHibernateTemplate().bulkUpdate(hql);
		
	}

}
