package edu.xidian.village.dao.impl;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.stereotype.Repository;

import edu.xidian.village.dao.RentHouseDao;
import edu.xidian.village.template.VillageHibernateTemplate;
import edu.xidian.village.util.PageUtil;
import edu.xidian.village.vo.RentHouse;
@Repository
public class RentHouseDaoImpl extends VillageHibernateTemplate implements RentHouseDao{

	@Override
	public void add() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void add(RentHouse rentHouse) {
		this.getHibernateTemplate().save(rentHouse);
		
	}

	@Override
	public RentHouse get(int rid) {
		// TODO Auto-generated method stub
		return this.getHibernateTemplate().get(RentHouse.class, rid);
	}

	@Override
	public List<RentHouse> list() {
		// TODO Auto-generated method stub
		return this.getHibernateTemplate().find("from RentHouse");
	}

	@Override
	public List<RentHouse> list(final int num, final int offset) {
		final String hql = " from RentHouse order by datetime desc";
		/*select new Notification(id,message,datetime,picture,title)*/
	
		return  getHibernateTemplate().execute(new HibernateCallback<List<RentHouse>>()
		{
			 @Override
			public List<RentHouse> doInHibernate(Session session)throws HibernateException, SQLException 
			 {
				 List<RentHouse> list2 = PageUtil.getList(session,hql,offset,num);
		        return list2;
			 }
		});
	}

	@Override
	public void delete(int rid) {
		this.getHibernateTemplate().delete(this.getHibernateTemplate().get(RentHouse.class, rid));
		
	}

	@Override
	public RentHouse merge(RentHouse rentHouse) {
		return this.getHibernateTemplate().merge(rentHouse);
		
	}

	@Override
	public void flush() {
		this.getHibernateTemplate().flush();
		
	}

}
