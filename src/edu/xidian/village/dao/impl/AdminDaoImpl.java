package edu.xidian.village.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import edu.xidian.village.dao.AdminDao;
import edu.xidian.village.template.VillageHibernateTemplate;
import edu.xidian.village.vo.Admin;
@Repository("adminDaoImpl")
public class AdminDaoImpl extends VillageHibernateTemplate implements AdminDao{

	@Override
	public Admin getAdmin(String name)throws Exception {
		
		return (Admin) getHibernateTemplate().find("from Admin where name = ?",name).get(0);
	}

	@Override
	public Admin getAdmin() {
	
		return null;
	}

	@Override
	public Admin get(String name) {
		List<Admin> list= this.getHibernateTemplate().find("from Admin where name = ?",name);
		if(list.isEmpty())
			return null;
		else
			return list.get(0);
	}

}
