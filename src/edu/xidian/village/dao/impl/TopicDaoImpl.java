package edu.xidian.village.dao.impl;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.stereotype.Repository;

import edu.xidian.village.dao.TopicDao;
import edu.xidian.village.template.VillageHibernateTemplate;
import edu.xidian.village.util.PageUtil;
import edu.xidian.village.vo.Topic;
@Repository
public class TopicDaoImpl extends VillageHibernateTemplate implements TopicDao{

	@Override
	public int add(Topic topic) {
		return  (Integer) this.getHibernateTemplate().save(topic);
		
	}

	@Override
	public Topic get(int tid) {
		// TODO Auto-generated method stub
		return this.getHibernateTemplate().get(Topic.class, tid);
	}

	@Override
	public Topic get(Topic topic) {
		// TODO Auto-generated method stub
		return this.getHibernateTemplate().get(Topic.class, topic.getId());
	}

	@Override
	public List<Topic> list() {
		
		return this.getHibernateTemplate().find("from Topic");
	}

	@Override
	public void update(Topic topic) {
		this.getHibernateTemplate().update(topic);
		
	}

	@Override
	public Topic merge(Topic topic) {
		// TODO Auto-generated method stub
		return this.getHibernateTemplate().merge(topic);
	}

	@Override
	public void flush() {
		this.getHibernateTemplate().flush();
		
	}

	@Override
	public List<Topic> list(final int num, final int offset) {
			final String hql = " from Topic order by datetime desc";
			/*select new Notification(id,message,datetime,picture,title)*/
		
		return  getHibernateTemplate().execute(new HibernateCallback<List<Topic>>()
		{
			 @Override
			public List<Topic> doInHibernate(Session session)throws HibernateException, SQLException 
			 {
				 List<Topic> list2 = PageUtil.getList(session,hql,offset,num);
		        return list2;
			 }
		});
	}

	@Override
	public int delete(String where) {
		String hql = "delete Topic where id in "+where;
		
		return this.getHibernateTemplate().bulkUpdate(hql);
	}

	@Override
	public List<Topic> search(String whereHql) {
		String Hql = "from Topic "+whereHql;
		
		return this.getHibernateTemplate().find(Hql);
	}

}
