package edu.xidian.village.dao.impl;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.stereotype.Repository;

import edu.xidian.village.dao.NotificationCommentDao;
import edu.xidian.village.template.VillageHibernateTemplate;
import edu.xidian.village.util.PageUtil;
import edu.xidian.village.vo.NotificationComment;
@Repository("notificationCommentDaoImpl")
public class NotificationCommentDaoImpl extends VillageHibernateTemplate implements NotificationCommentDao{

	@Override
	public void add(NotificationComment nc)throws Exception {
		getHibernateTemplate().save(nc);
		
	}

	@Override
	public List<NotificationComment> get(int nid) {
	return	getHibernateTemplate().find("select content,datetime,user.nearName,user.name from NotificationComment where notification_id =? order by datetime desc",nid);
		//from NotificationComment where notification_id =? order by datetime desc
	}

	@Override
	public void delete(List<NotificationComment> list) {
		this.getHibernateTemplate().deleteAll(list);
		this.getHibernateTemplate().flush();
		
	}

	@Override
	public void delete(NotificationComment nc) {
		this.getHibernateTemplate().delete(nc);
		this.getHibernateTemplate().flush();
	}

	@Override
	public List<NotificationComment> list(final int n, final int offset, int noid) {
		final String hql = " from NotificationComment where notification_id=\'"+noid+"\' order by datetime desc";
		/*select new Notification(id,message,datetime,picture,title)*/
	
	return  getHibernateTemplate().execute(new HibernateCallback<List<NotificationComment>>()
	{
		 @Override
		public List<NotificationComment> doInHibernate(Session session)throws HibernateException, SQLException 
		 {
			 List<NotificationComment> list2 = PageUtil.getList(session,hql,offset,n);
	      
			 return list2;
		 }
	});
	}

	@Override
	public int delete(String where) {
		String hql = "delete NotificationComment where id in "+where;
		return this.getHibernateTemplate().bulkUpdate(hql);
		
	}

	@Override
	public List<NotificationComment> list(int nid) {
		return this.getHibernateTemplate().find("from NotificationComment where notification_id=?",nid);
		
	}

	
	
}
