package edu.xidian.village.dao;

import java.util.List;

import edu.xidian.village.vo.Reply;

public interface ReplyDao {

	void add(Reply reply);
	int delete(String where);
	void get(int rid);
	void get(Reply reply);
	List<Reply> list();
	List<Reply> list(int n,int offset,int tid);
	void update(Reply reply);
	Reply merge(Reply reply);
	void flush();
	
}
