package edu.xidian.village.dao;

import java.util.List;

import edu.xidian.village.vo.NotificationComment;

public interface NotificationCommentDao {

	public void add(NotificationComment nc)throws Exception;
	
	public List<NotificationComment> get(int nid);
	
	public void delete(List<NotificationComment> list);
	public void delete(NotificationComment nc);
	
	List<NotificationComment> list(int n,int offset,int noid);
	
	public int delete(String where);
	
	public List<NotificationComment> list(int nid);
}
