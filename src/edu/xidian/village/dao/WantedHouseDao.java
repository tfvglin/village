package edu.xidian.village.dao;

import java.util.List;

import edu.xidian.village.vo.WantedHouse;

public interface WantedHouseDao {

	public void add();
	public void add(WantedHouse wantedHouse);
	public void delete(WantedHouse wantedHouse);
	public List<WantedHouse> list();
	public List<WantedHouse> list(int num,int offset);
	public WantedHouse merge(WantedHouse wantedHouse);
	public void flush();
}
