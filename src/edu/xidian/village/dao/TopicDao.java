package edu.xidian.village.dao;

import java.util.List;

import edu.xidian.village.vo.Topic;

public interface TopicDao {
	
	int add(Topic topic);
	
	public int delete(String where);
	
	
	Topic get(int tid);
	
	Topic get(Topic topic);
	
	List<Topic> list();
	List<Topic> list(int num,int offset);
	
	void update(Topic topic);
	
	Topic merge(Topic topic);
	
	List<Topic> search(String whereHql);
	
	void flush();

}
