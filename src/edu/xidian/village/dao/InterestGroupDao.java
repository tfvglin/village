package edu.xidian.village.dao;

import java.util.List;
import edu.xidian.village.vo.InterestGroup;

public interface InterestGroupDao {

	void add(InterestGroup interestGroup);
	InterestGroup get(InterestGroup interestGroup);
	InterestGroup get(int igid);
	InterestGroup get(String gid);
	InterestGroup getBase(int igid);
	void update(InterestGroup interestGroup);
	InterestGroup merge(InterestGroup interestGroup);
	void flush();
	
	List<InterestGroup> list();
	
	List<InterestGroup> list(int n,int offset);
	
	InterestGroup find(String propertyName,String value);
	
	
}
