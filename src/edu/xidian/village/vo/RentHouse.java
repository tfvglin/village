package edu.xidian.village.vo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
@Entity
public class RentHouse {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	@ManyToOne
	@JoinColumn(name="ownUser_id")
	private User ownUser;
	
	private String address;
	private String introduce;
	private String picture;

	private Long datetime;
	private Character type;  // 1 整租     2 合租
	private Character area;  // 1 北校区   2 南校区
	private Character status; // 1 未出租  2 已出租
	private String telephone;
	private String money;
	public RentHouse(){}
	public RentHouse(String address, String introduce, String picture,
			Character type, Character area, String telephone, String money)
	{
		this.address=address;
		this.introduce=introduce;
		this.picture=picture;
		this.type=type;
		this.area=area;
		this.telephone=telephone;
		this.money=money;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public User getOwnUser() {
		return ownUser;
	}
	public void setOwnUser(User ownUser) {
		this.ownUser = ownUser;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getIntroduce() {
		return introduce;
	}
	public void setIntroduce(String introduce) {
		this.introduce = introduce;
	}
	public String getPicture() {
		return picture;
	}
	public void setPicture(String picture) {
		this.picture = picture;
	}
	public Long getDatetime() {
		return datetime;
	}
	public void setDatetime(Long datetime) {
		this.datetime = datetime;
	}
	public Character getType() {
		return type;
	}
	public void setType(Character type) {
		this.type = type;
	}
	public Character getArea() {
		return area;
	}
	public void setArea(Character area) {
		this.area = area;
	}

	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	public String getMoney() {
		return money;
	}
	public void setMoney(String money) {
		this.money = money;
	}
	public Character getStatus() {
		return status;
	}
	public void setStatus(Character status) {
		this.status = status;
	}

	
	
	
	
}
