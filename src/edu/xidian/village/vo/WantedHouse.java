package edu.xidian.village.vo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
@Entity
public class WantedHouse {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	private Character area;	//1 北校区      2 南校区
	private Character sex;	//1 男	2 女
	private String telephone;
	@ManyToOne
	@JoinColumn(name="wantedUser_id")
	private User wantedUser;
	private String money;
	private String description;
	private Long datetime;
	private Character type; //1 整租   2 合租
	private Character status; //1 未找到  2 已找到
	public WantedHouse(){}
	public WantedHouse(Character type,Character area,Character sex,String telephone,String money,String description)
	{
		this.type = type;
		this.area = area;
		this.sex = sex;
		this.telephone = telephone;
		this.money = money;
		this.description = description;
		
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Character getArea() {
		return area;
	}
	public void setArea(Character area) {
		this.area = area;
	}
	public Character getSex() {
		return sex;
	}
	public void setSex(Character sex) {
		this.sex = sex;
	}

	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	public User getWantedUser() {
		return wantedUser;
	}
	public void setWantedUser(User wantedUser) {
		this.wantedUser = wantedUser;
	}
	public String getMoney() {
		return money;
	}
	public void setMoney(String money) {
		this.money = money;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Long getDatetime() {
		return datetime;
	}
	public void setDatetime(Long datetime) {
		this.datetime = datetime;
	}
	public Character getType() {
		return type;
	}
	public void setType(Character type) {
		this.type = type;
	}
	public Character getStatus() {
		return status;
	}
	public void setStatus(Character status) {
		this.status = status;
	}
	
}
