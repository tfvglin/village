package edu.xidian.village.vo;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Topic {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	private String message;
	private String picture;
	private String praise_u;
	private Integer praise_num;
	private Long datetime;
	private Character isHot;
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="user_landlord_id")
	private User landlord;
	@OneToMany(mappedBy="topic")
	private List<Reply> reply;
	public Topic()
	{
		
	}

	public Topic(int id,Long datetime,String message,String picture,int praise_num)
	{
		this.id=id;
		this.datetime=datetime;
		this.message=message;
		this.picture=picture;
		this.praise_num=praise_num;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getPicture() {
		return picture;
	}
	public void setPicture(String picture) {
		this.picture = picture;
	}

	public List<Reply> getReply() {
		return reply;
	}
	public void setReply(List<Reply> reply) {
		this.reply = reply;
	}
	public User getLandlord() {
		return landlord;
	}
	public void setLandlord(User landlord) {
		this.landlord = landlord;
	}
	public Long getDatetime() {
		return datetime;
	}
	public void setDatetime(Long datetime) {
		this.datetime = datetime;
	}
	public String getPraise_u() {
		return praise_u;
	}
	public void setPraise_u(String praise_u) {
		this.praise_u = praise_u;
	}
	public Integer getPraise_num() {
		return praise_num;
	}
	public void setPraise_num(Integer praise_num) {
		this.praise_num = praise_num;
	}

	public char getIsHot() {
		return isHot;
	}

	public void setIsHot(char isHot) {
		this.isHot = isHot;
	}

	
	
}
