package edu.xidian.village.vo;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="user")
public class User {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	private String name="";
	private String password="";
	private String realName="";
	private String email="";
	private String phone="";
	private String nearName="";
	private String photo;          //2 没有头像  1 有头像
	private Long lasttime;
	@OneToMany(mappedBy="creatUser")  
	private Set<InterestGroup> interestGroup;  
	
	@ManyToMany(mappedBy="member")  
	private Set<InterestGroup> gset ;
	
	@OneToMany(mappedBy="user")
	private List<NotificationComment> notificationComment = new ArrayList<NotificationComment>();
	
	@OneToMany(mappedBy="replyer")
	private List<Reply> reply ;
	
	@OneToMany(mappedBy="landlord")
	private List<Topic> topic;
	
	@OneToMany(mappedBy="ownUser")
	private List<RentHouse> rentHouse;
	
	@OneToMany(mappedBy="wantedUser")
	private List<WantedHouse> wantedHouse;
	
	@OneToMany(mappedBy="userSender")
	private List<FriendRequest> friendRequestSender = new ArrayList<FriendRequest>();

	
	@OneToMany(mappedBy="userReceiver")
	private List<FriendRequest> friendRequestReceiver = new ArrayList<FriendRequest>();
	
	@ManyToMany(cascade={CascadeType.ALL},targetEntity = User.class)  
	@JoinTable(name = "user_friend", joinColumns = { @JoinColumn(name = "userId") }, inverseJoinColumns = { @JoinColumn(name = "friendId") })
	private Set<User> friends = new HashSet<User>();
	
	public User()
	{
		
	}
	public User(String name)
	{
		this.name=name;
	}
	public User(String realName,String nearName)
	{
		this.realName=realName;
		this.nearName=nearName;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}

	
	public String getRealName() {
		return realName;
	}
	public void setRealName(String realName) {
		this.realName = realName;
	}
	public String getNearName() {
		return nearName;
	}
	public void setNearName(String nearName) {
		this.nearName = nearName;
	}
	
	
	public List<NotificationComment> getNotificationComment() {
		return notificationComment;
	}
	public void setNotificationComment(List<NotificationComment> notificationComment) {
		this.notificationComment = notificationComment;
	}
	@Override
	public String toString()
	{
		return name+","+realName+","+email+","+id;
	}
	public Set<User> getFriends() {
		return friends;
	}
	public void setFriends(Set<User> friends) {
		this.friends = friends;
	}
	public List<FriendRequest> getFriendRequestSender() {
		return friendRequestSender;
	}
	public void setFriendRequestSender(List<FriendRequest> friendRequestSender) {
		this.friendRequestSender = friendRequestSender;
	}
	public List<FriendRequest> getFriendRequestReceiver() {
		return friendRequestReceiver;
	}
	public void setFriendRequestReceiver(List<FriendRequest> friendRequestReceiver) {
		this.friendRequestReceiver = friendRequestReceiver;
	}
	public Set<InterestGroup> getGset() {
		return gset;
	}
	public void setGset(Set<InterestGroup> gset) {
		this.gset = gset;
	}
	public Set<InterestGroup> getInterestGroup() {
		return interestGroup;
	}
	public void setInterestGroup(Set<InterestGroup> interestGroup) {
		this.interestGroup = interestGroup;
	}
	public String getPhoto() {
		return photo;
	}
	public void setPhoto(String photo) {
		this.photo = photo;
	}
	public List<Reply> getReply() {
		return reply;
	}
	public void setReply(List<Reply> reply) {
		this.reply = reply;
	}
	public List<Topic> getTopic() {
		return topic;
	}
	public void setTopic(List<Topic> topic) {
		this.topic = topic;
	}
	public Long getLasttime() {
		return lasttime;
	}
	public void setLasttime(Long lasttime) {
		this.lasttime = lasttime;
	}
	public List<RentHouse> getRentHouse() {
		return rentHouse;
	}
	public void setRentHouse(List<RentHouse> rentHouse) {
		this.rentHouse = rentHouse;
	}
	public List<WantedHouse> getWantedHouse() {
		return wantedHouse;
	}
	public void setWantedHouse(List<WantedHouse> wantedHouse) {
		this.wantedHouse = wantedHouse;
	}

	
}
