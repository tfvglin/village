package edu.xidian.village.vo;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Reply {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	private String message;
	private Long datetime;
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="user_replyer_id")
	private User replyer;
	@ManyToOne
	@JoinColumn(name="topic_id")
	private Topic topic;
	
	public Reply()
	{
		
	}
	public Reply(int id,String message,Long datetime,User replyer)
	{
		this.id=id;
		this.message=message;
		this.datetime=datetime;
		this.replyer=replyer;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Long getDatetime() {
		return datetime;
	}
	public void setDatetime(Long datetime) {
		this.datetime = datetime;
	}
	public User getReplyer() {
		return replyer;
	}
	public void setReplyer(User replyer) {
		this.replyer = replyer;
	}
	public Topic getTopic() {
		return topic;
	}
	public void setTopic(Topic topic) {
		this.topic = topic;
	}

	
}

