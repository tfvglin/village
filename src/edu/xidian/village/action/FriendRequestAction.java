package edu.xidian.village.action;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;

import edu.xidian.village.service.FriendRequestService;
import edu.xidian.village.service.UserService;
import edu.xidian.village.vo.FriendRequest;
import edu.xidian.village.vo.User;

@Controller("friendRequestAction")
public class FriendRequestAction extends SuperAction{

	FriendRequestService friendRequestServiceImpl;
	UserService userServiceImpl;
	private FriendRequest friendRequest;
	private List frlist;
	public InputStream responseStream;
	
	public InputStream getResponseStream() {
		return responseStream;
	}
	public void setResponseStream(InputStream responseStream) {
		this.responseStream = responseStream;
	}
	public UserService getUserServiceImpl() {
		return userServiceImpl;
	}
	@Resource
	public void setUserServiceImpl(UserService userServiceImpl) {
		this.userServiceImpl = userServiceImpl;
	}
	public FriendRequestService getFriendRequestServiceImpl() {
		return friendRequestServiceImpl;
	}
	@Resource
	public void setFriendRequestServiceImpl(
			FriendRequestService friendRequestServiceImpl) {
		this.friendRequestServiceImpl = friendRequestServiceImpl;
	}

	public String friendRequestAddStream()
	{
		int sendid = Integer.parseInt(request.getParameter("sendid"));
		int receiveid = Integer.parseInt(request.getParameter("receiveid"));
		String message = request.getParameter("message");
		Long datetime = new Date().getTime();
		friendRequest = new FriendRequest();
		User senduser = userServiceImpl.getUser(sendid);
		User receiveUser = userServiceImpl.getUser(receiveid);
		friendRequest.setDatetime(datetime);
		friendRequest.setMessage(message);
		friendRequest.setState("1");
		friendRequest.setUserReceiver(receiveUser);
		friendRequest.setUserSender(senduser);
		friendRequestServiceImpl.sendRequest(friendRequest);
		responseStream = new ByteArrayInputStream("ok".getBytes());
		return SUCCESS;
	}
	public String friendRequestReceiveJson()
	{
		int receiveid = Integer.parseInt(request.getParameter("uid"));
		//User receiveuser = userServiceImpl.getUser(receiveid);
		frlist = friendRequestServiceImpl.receiveRequestJson(receiveid);
	
		return SUCCESS;
	}
	public String friendRequestSendJson()
	{
		int sendid = Integer.parseInt(request.getParameter("uid"));
		//User senduser = userServiceImpl.getUser(sendid);
		frlist = friendRequestServiceImpl.sendRequestJson(sendid);
		
		return SUCCESS;
	}
	public String friendRequestHandleStream()
	{
		String state = request.getParameter("state");
		int frid = Integer.parseInt(request.getParameter("frid"));
		friendRequestServiceImpl.modifyState(frid, state);
		if(state.equals("2")||state=="2")
		{
			FriendRequest friendRequest = friendRequestServiceImpl.getFriendRequest(frid);
			int fid = friendRequest.getUserReceiver().getId();
			int uid = friendRequest.getUserSender().getId();
			request.setAttribute("fid", fid);
			request.setAttribute("uid", uid);
			return "redirect";
		}
		responseStream = new ByteArrayInputStream("ok".getBytes());
		return SUCCESS;
	}
	public FriendRequest getFriendRequest() {
		return friendRequest;
	}
	public void setFriendRequest(FriendRequest friendRequest) {
		this.friendRequest = friendRequest;
	}
	public List getFrlist() {
		return frlist;
	}
	public void setFrlist(List frlist) {
		this.frlist = frlist;
	}
}
