package edu.xidian.village.action;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import edu.xidian.village.service.MessageService;
import edu.xidian.village.util.PagerUtil;
import edu.xidian.village.util.jpush.JPushUtil;
import edu.xidian.village.vo.Message;



@Controller("messageAction")
@Scope("prototype")
public class MessageAction extends SuperAction{
	
		private JPushUtil jPushUtil;
		private PagerUtil pagerUtil;
		private MessageService messageServiceImpl;
		private InputStream responseStream;
		
	
	    public String messagePushStream()
	    {
	    	String content = request.getParameter("content");
	    	String title = request.getParameter("title");
	    	Message message = this.messageServiceImpl.addMessage(title,content);
	    	String sign = jPushUtil.sendPush(message);
	    	if(sign.equals("success"))
	    	{
	    		responseStream = new ByteArrayInputStream("success".getBytes());
	    	}
	    	else
	    		responseStream = new ByteArrayInputStream("error".getBytes());
	    	return SUCCESS;
	    	
	    }
	    
	    public String messageListJsp()
	    {
	    	List<Message> mlist = messageServiceImpl.getMessageList();
	    	this.pagerUtil.Pager(mlist, request);
	    	return SUCCESS;
	    }




		public JPushUtil getjPushUtil() {
			return jPushUtil;
		}



		@Resource
		public void setjPushUtil(JPushUtil jPushUtil) {
			this.jPushUtil = jPushUtil;
		}




		public MessageService getMessageServiceImpl() {
			return messageServiceImpl;
		}



		@Resource
		public void setMessageServiceImpl(MessageService messageServiceImpl) {
			this.messageServiceImpl = messageServiceImpl;
		}




		public InputStream getResponseStream() {
			return responseStream;
		}




		public void setResponseStream(InputStream responseStream) {
			this.responseStream = responseStream;
		}

		public PagerUtil getPagerUtil() {
			return pagerUtil;
		}
		@Resource
		public void setPagerUtil(PagerUtil pagerUtil) {
			this.pagerUtil = pagerUtil;
		}

	    
	    
	}
	

