package edu.xidian.village.action;

import java.io.InputStream;
import java.util.Date;
import javax.annotation.Resource;

import org.springframework.stereotype.Controller;

import edu.xidian.village.service.AdminService;
import edu.xidian.village.service.NotificationService;
import edu.xidian.village.service.UserService;
import edu.xidian.village.util.DateUtil;
import edu.xidian.village.util.ImgUtil;
import edu.xidian.village.util.PagerUtil;
import edu.xidian.village.vo.Admin;

@Controller("adminAction")
public class AdminAction extends SuperAction {

	private Admin admin;
	

	private AdminService adminServiceImpl;
	private UserService userServiceImpl;
	private NotificationService notificationServiceImpl;

	private PagerUtil pagerUtil;
	private ImgUtil imgUtil;
	private InputStream responseStream;

	private DateUtil dateUtil;
	
	
	public DateUtil getDateUtil() {
		return dateUtil;
	}
	@Resource
	public void setDateUtil(DateUtil dateUtil) {
		this.dateUtil = dateUtil;
	}
	public NotificationService getNotificationServiceImpl() {
		return notificationServiceImpl;
	}
	@Resource
	public void setNotificationServiceImpl(
			NotificationService notificationServiceImpl) {
		this.notificationServiceImpl = notificationServiceImpl;
	}
	public InputStream getResponseStream() {
		return responseStream;
	}
	public void setResponseStream(InputStream responseStream) {
		this.responseStream = responseStream;
	}
	public ImgUtil getImgUtil() {
		return imgUtil;
	}
	@Resource
	public void setImgUtil(ImgUtil imgUtil) {
		this.imgUtil = imgUtil;
	}


	public PagerUtil getPagerUtil() {
		return pagerUtil;
	}

	@Resource
	public void setPagerUtil(PagerUtil pagerUtil) {
		this.pagerUtil = pagerUtil;
	}

	public AdminService getAdminServiceImpl() {
		return adminServiceImpl;
	}

	@Resource
	public void setAdminServiceImpl(AdminService adminServiceImpl) {
		this.adminServiceImpl = adminServiceImpl;
	}

	public Admin getAdmin() {
		return admin;
	}

	public void setAdmin(Admin admin) {
		this.admin = admin;
	}



	public String adminLogin() {
		return SUCCESS;
	}
	public String adminLogout() {
		request.getSession().removeAttribute("admin");
		return SUCCESS;
	}

/*	public String userModify()
	{
		User u = adminServiceImpl.getUser(user);
		if(!user.getEmail().equals(""))
			u.setEmail(user.getEmail());
		if(!user.getName().equals(""))
			u.setName(user.getName());
		if(!user.getNearName().equals(""))
			u.setNearName(user.getNearName());
		if(!user.getRealName().equals(""))
			u.setRealName(user.getRealName());
		if(!user.getPhone().equals(""))
			u.setPhone(user.getPhone());
		adminServiceImpl.updateUser(u);
		
		return SUCCESS;
	}*/

/*	public String notificationImgSave()
	{
		Float x =  Float.parseFloat(request.getParameter("x"));
		Float y =  Float.parseFloat(request.getParameter("y"));
		Float width =  Float.parseFloat(request.getParameter("width"));
		Float height = Float.parseFloat(request.getParameter("height"));
		String title = request.getParameter("title");
		String srcpath = application.getRealPath("/") + "uploads\\image\\" + title + ".jpg";
		String subpath = application.getRealPath("/") + "uploads\\notification\\" + title + ".jpg";
		System.out.println(x);
		System.out.println(y);
		System.out.println(width);
		System.out.println(height);
		try {
			imgUtil.cut(srcpath, subpath, x.intValue(), y.intValue(), width.intValue(), height.intValue());
			responseStream = new ByteArrayInputStream("success".getBytes());
			
		} catch (IOException e) {
			
			// TODO Auto-generated catch block
			e.printStackTrace();
			responseStream = new ByteArrayInputStream("error".getBytes());
		
		}
		return SUCCESS;
	}*/
	

	
	public String adminIndex() {
		this.clearMessages();
		String randomCode = request.getParameter("randomCode").toLowerCase();
		
		String sessionCode = (String) request.getSession().getAttribute(
				"randomCode");

		
		if (randomCode == sessionCode || randomCode.equals(sessionCode)) {
			Admin ad = adminServiceImpl.getAdmin(admin);
			if (ad!=null) {
				if(admin.getPassword()==ad.getPassword()||admin.getPassword().equals(ad.getPassword()))
				{
					request.getSession().setAttribute("admin",ad);
					request.getSession().setAttribute("logintime", new Date().getTime());
					this.flushIndex();
					return SUCCESS;
				}
				else
				{
					this.addActionMessage("用户名和密码不匹配！");
					return INPUT;
				}
			} else {
				this.addActionMessage("用户名不存在！");
			//	this.addFieldError("name or password error", "用户名或密码错误！");
				return INPUT;
			}
			
		}

		else
		{
			this.addActionMessage("验证码错误！");
			//this.addFieldError("randomError", "验证码错误!");
			return INPUT;
		}
	}

	
	public String indexPage()
	{
		this.flushIndex();
		return SUCCESS;
	}
	private void flushIndex()
	{
		request.getSession().setAttribute("usernum",userServiceImpl.getUserNum());
		request.getSession().setAttribute("notificationnum",notificationServiceImpl.getNotificationNum());
		request.getSession().setAttribute("activeusernum",userServiceImpl.getActiveUserNum(dateUtil.getStartTime()));
	}

	public UserService getUserServiceImpl() {
		return userServiceImpl;
	}
	@Resource
	public void setUserServiceImpl(UserService userServiceImpl) {
		this.userServiceImpl = userServiceImpl;
	}
}
