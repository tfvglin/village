package edu.xidian.village.action;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;

import edu.xidian.village.service.WantedHouseService;
import edu.xidian.village.util.PagerUtil;
import edu.xidian.village.vo.WantedHouse;

@Controller
public class WantedHouseAction extends SuperAction{

	private WantedHouseService wantedHouseServiceImpl;
	private List list;
	private InputStream responseStream;
	private PagerUtil pagerUtil;
	private WantedHouse wantedHouse;
	
	public String wantedHouseListJsp()
	{
		List<WantedHouse> list = wantedHouseServiceImpl.getWantedHouseList();
		pagerUtil.Pager(list, request);
		return SUCCESS;
	}
	
	public String wantedHouseUserListJson()
	{
		String username = request.getParameter("username");
		int num = Integer.valueOf(request.getParameter("num"));
		int page = Integer.valueOf(request.getParameter("page"));
		list = wantedHouseServiceImpl.getWantedHouseList(username, num, page);
		return SUCCESS;
	}
	public String wantedHouseListJson()
	{
		int num = Integer.valueOf(request.getParameter("num"));
		int page = Integer.valueOf(request.getParameter("page"));
		list = wantedHouseServiceImpl.getWantedHouseList(num, page);
		return SUCCESS;
		
	}
	public String wantedHouseAddStream()
	{
		Character area = request.getParameter("wantedHouse.area").charAt(0);
		Character sex = request.getParameter("wantedHouse.sex").charAt(0);
		Character type = request.getParameter("wantedHouse.type").charAt(0);
		String telephone = request.getParameter("wantedHouse.telephone");
		String money = request.getParameter("wantedHouse.money");
		String description = request.getParameter("wantedHouse.description");
		//String username = request.getParameter("username");
		Integer uid = Integer.valueOf(request.getParameter("uid"));
		wantedHouseServiceImpl.addWantedHouse(type, area, sex, telephone, money, description, uid);
		responseStream = new ByteArrayInputStream("ok".getBytes());
		return SUCCESS;
		
	}
	
	public WantedHouseService getWantedHouseServiceImpl() {
		return wantedHouseServiceImpl;
	}
	@Resource
	public void setWantedHouseServiceImpl(WantedHouseService wantedHouseServiceImpl) {
		this.wantedHouseServiceImpl = wantedHouseServiceImpl;
	}
	public List getList() {
		return list;
	}
	public void setList(List list) {
		this.list = list;
	}
	public InputStream getResponseStream() {
		return responseStream;
	}
	public void setResponseStream(InputStream responseStream) {
		this.responseStream = responseStream;
	}
	public PagerUtil getPagerUtil() {
		return pagerUtil;
	}
	@Resource
	public void setPagerUtil(PagerUtil pagerUtil) {
		this.pagerUtil = pagerUtil;
	}
	public WantedHouse getWantedHouse() {
		return wantedHouse;
	}
	public void setWantedHouse(WantedHouse wantedHouse) {
		this.wantedHouse = wantedHouse;
	}
	
}
