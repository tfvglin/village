package edu.xidian.village.action;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;

import edu.xidian.village.service.ReplyService;

@Controller
public class ReplyAction extends SuperAction{

	private ReplyService replyServiceImpl;
	private InputStream responseStream;
	private List list;
	
	public ReplyService getReplyServiceImpl() {
		return replyServiceImpl;
	}
	@Resource
	public void setReplyServiceImpl(ReplyService replyServiceImpl) {
		this.replyServiceImpl = replyServiceImpl;
	}
	public InputStream getResponseStream() {
		return responseStream;
	}
	public void setResponseStream(InputStream responseStream) {
		this.responseStream = responseStream;
	}
	
	public String replyAddStream()
	{
		int uid = Integer.parseInt(request.getParameter("uid"));
		int tid = Integer.parseInt(request.getParameter("tid"));
		String message = request.getParameter("message");
		Long datetime = Long.parseLong(request.getParameter("datetime"));
		replyServiceImpl.addReply(tid, uid, message,datetime);
		responseStream = new ByteArrayInputStream("ok".getBytes());
		return SUCCESS;
	}
	public String replyListJson()
	{
		int tid = Integer.parseInt(request.getParameter("id"));
		int n = Integer.parseInt(request.getParameter("num"));
		int offset = Integer.parseInt(request.getParameter("offset"));
		setList(replyServiceImpl.getReplyJson(tid, n, offset));
		return SUCCESS;
	}
	public String replyDeleteStream()
	{
		String[] rlist = request.getParameterValues("rlist[]");
		
		int[] rids = new int[rlist.length];  
		for(int i=0;i<rlist.length;i++){  
			rids[i]=Integer.parseInt(rlist[i]);   
		}
		if(replyServiceImpl.deleteReply(rids))
			responseStream = new ByteArrayInputStream("success".getBytes());
		else
			responseStream =  new ByteArrayInputStream("error".getBytes());
		return SUCCESS;
	}
	public List getList() {
		return list;
	}
	public void setList(List list) {
		this.list = list;
	}


	
}
