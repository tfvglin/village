package edu.xidian.village.action;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;

import edu.xidian.village.service.NotificationCommentService;
import edu.xidian.village.util.Fetch;
import edu.xidian.village.util.PagerUtil;
import edu.xidian.village.vo.NotificationComment;

@Controller("notificationCommentAction")
public class NotificationCommentAction extends SuperAction{

	private NotificationCommentService notificationCommentServiceImpl;
	private Fetch fetch;
	private String sign;
	private PagerUtil pagerUtil;
	private InputStream responseStream;
	private List list;
	
	public String getSign() {
		return sign;
	}
	public void setSign(String sign) {
		this.sign = sign;
	}
	

	public PagerUtil getPagerUtil() {
		return pagerUtil;
	}
	@Resource
	public void setPagerUtil(PagerUtil pagerUtil) {
		this.pagerUtil = pagerUtil;
	}
	public List getList() {
		return list;
	}
	public void setList(List list) {
		this.list = list;
	}

	public InputStream getResponseStream() {
		return responseStream;
	}
	public void setResponseStream(InputStream responseStream) {
		this.responseStream = responseStream;
	}
	public Fetch getFetch() {
		return fetch;
	}
	@Resource
	public void setFetch(Fetch fetch) {
		this.fetch = fetch;
	}

	public NotificationCommentService getNotificationCommentServiceImpl() {
		return notificationCommentServiceImpl;
	}

	@Resource
	public void setNotificationCommentServiceImpl(
			NotificationCommentService notificationCommentServiceImpl) {
		this.notificationCommentServiceImpl = notificationCommentServiceImpl;
	}
	
	public String addNotificationComment()
	{
		NotificationComment nc = new NotificationComment();
		try {
			
			nc.setContent(request.getParameter("content"));
			nc.setDatetime(Long.parseLong(request.getParameter(("datetime"))));
			int nid=Integer.parseInt(request.getParameter("nid"));
			int uid=Integer.parseInt(request.getParameter("uid"));
		
			
			sign=notificationCommentServiceImpl.addNotificationComment(nc, nid,uid)?"ok":"error";	
				
		
		} catch (Exception e) {
			sign="error";
			e.printStackTrace();
		} 
		responseStream = new ByteArrayInputStream(sign.getBytes());
		return SUCCESS;
	}
	
/*	public String notificationCommentList()
	{
		try{
			int nid = Integer.parseInt(request.getParameter("nid"));
			list = notificationCommentServiceImpl.getNotificationCommentList(nid);
		
			if(list.isEmpty())
				sign="empty";
			else
				sign="ok";
		}
		catch(Exception e)
		{
			sign="error";
			e.printStackTrace();
		}
	
			return SUCCESS;
	}*/
	public String notificationCommentListJsp()
	{
		int noid = Integer.parseInt(request.getParameter("noid"));
		List<NotificationComment> nclist =notificationCommentServiceImpl.getNotificationCommentList(noid);
		this.pagerUtil.Pager(nclist, request);
		return SUCCESS;
	}
	public String notificationCommentListJson()
	{
		int n = Integer.parseInt(request.getParameter("num"));
		int offset = Integer.parseInt(request.getParameter("offset"));
		int nid = Integer.parseInt(request.getParameter("id"));
		list = notificationCommentServiceImpl.getNotificationCommentList(n, offset, nid);
		
		return "list";
	}
	
	public String notificationCommentDeleteStream()
	{
		String[] ncidlist = request.getParameterValues("nclist[]");
		
		int[] ncids = new int[ncidlist.length];  
		for(int i=0;i<ncidlist.length;i++){  
			ncids[i]=Integer.parseInt(ncidlist[i]);   
		}
		if(notificationCommentServiceImpl.deleteNC(ncids))
			responseStream = new ByteArrayInputStream("success".getBytes());
		else
			responseStream =  new ByteArrayInputStream("error".getBytes());
		return SUCCESS;
	}
	
	
}
