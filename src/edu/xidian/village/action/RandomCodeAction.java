
package edu.xidian.village.action;

import javax.annotation.Resource;
import org.springframework.stereotype.Controller;

import edu.xidian.village.util.CreateImageCode;


@Controller("randomCodeAction")
public class RandomCodeAction extends SuperAction {

	private CreateImageCode createImageCode;
	
	
  public CreateImageCode getCreateImageCode() {
		return createImageCode;
	}

  	@Resource
	public void setCreateImageCode(CreateImageCode createImageCode) {
		this.createImageCode = createImageCode;
	}


public void getRandomCode() throws Exception
  {
	 response.setContentType("image/jpeg");
     //禁止图像缓存。
     response.setHeader("Pragma", "no-cache");
     response.setHeader("Cache-Control", "no-cache");
     response.setDateHeader("Expires", 0);
     
     
     CreateImageCode vCode = new CreateImageCode(100,30,5,10);
     request.getSession().setAttribute("randomCode", vCode.getCode());
    
     vCode.write(response.getOutputStream());
	
  }
    
	
    
    
}