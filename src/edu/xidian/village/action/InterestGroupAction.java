package edu.xidian.village.action;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.json.JSONObject;
import org.springframework.stereotype.Controller;

import edu.xidian.village.service.InterestGroupService;
import edu.xidian.village.util.Fetch;
import edu.xidian.village.util.ImgStream;
import edu.xidian.village.util.PagerUtil;
import edu.xidian.village.vo.InterestGroup;
import edu.xidian.village.vo.Topic;
import edu.xidian.village.vo.User;

/*
 *  gid：极光群id
 *  igid:本地群id
 */

@Controller
public class InterestGroupAction extends SuperAction{

	
	
	private InterestGroupService interestgroupServiceImpl;
	private PagerUtil pagerUtil;
	private InputStream responseStream;
	private List iglist;
	private Map<String, String> igmap;
	public InterestGroupService getGroupServiceImpl() {
		return interestgroupServiceImpl;
	}
	@Resource
	public void setGroupServiceImpl(InterestGroupService interestgroupServiceImpl) {
		this.interestgroupServiceImpl = interestgroupServiceImpl;
	}
	public String interestGroupListJsp()
	{
		List<InterestGroup> list = interestgroupServiceImpl.getInterestGroupList();
		this.pagerUtil.Pager(list, request);
		return SUCCESS;
	}
	public String interestGroupViewJsp()
	{
		int igid = Integer.parseInt(request.getParameter("igid"));
		InterestGroup interestGroup =interestgroupServiceImpl.getInterestGroup(igid);
		request.setAttribute("interestGroup", interestGroup);
		List<User> list = new ArrayList<User>(interestGroup.getMember());
		this.pagerUtil.Pager(list, request);
		return SUCCESS;
	}
	public String interestGroupAddStream()
	{
		String imgCode;
		String name;
		String introduce;
		String message;
		String picture;
		Long datetime;
		int uid;
	
		try {
			JSONObject jsonObject ;
			String jsonStr = Fetch.fetchInputStream(request.getInputStream());
			
			jsonObject = new JSONObject(jsonStr);
			
			imgCode = jsonObject.getString("img");
			message= jsonObject.getString("message");
			introduce = jsonObject.getString("introduce");
			name=jsonObject.getString("name");
			datetime = Long.parseLong(jsonObject.getString("datetime"));
			uid = Integer.parseInt(jsonObject.getString("uid"));
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			responseStream =  new  ByteArrayInputStream("error".getBytes());
			return SUCCESS;
		}
		if(imgCode!=null&&!imgCode.equals("null")&&!imgCode.equals(""))
		{
			picture = String.valueOf(datetime);
			String path = application.getRealPath("/")+"uploads/interestGroup/";
			ImgStream.base64StringToImage(imgCode,picture,path);
		}
		else{
			picture="";
		}
		String re = interestgroupServiceImpl.addInterestGroup(name, introduce, uid, message, picture, datetime);
		if(!re.equals("error"))
			responseStream = new ByteArrayInputStream(("ok"+re).getBytes());
		else
			responseStream = new ByteArrayInputStream(re.getBytes());
		return SUCCESS;
		
		
		
		
		/*String name = request.getParameter("name");
		String introduce = request.getParameter("introduce");
		int creatid = Integer.parseInt(request.getParameter("creatid"));
		String ids[] = request.getParameterValues("mids");
		if(ids!=null)
		{
			int mids[] = new int[ids.length];
			for(int i=0;i<mids.length;i++)
			{
				mids[i]=Integer.parseInt(ids[i]);
			}
			interestgroupServiceImpl.addInterestGroup(name, introduce, creatid, mids);
		}
		else
		{
			interestgroupServiceImpl.addInterestGroup(name, introduce, creatid);
		}
		responseStream = new ByteArrayInputStream("ok".getBytes());
		return SUCCESS;*/
	}
	
	public String interestGroupModifyStream()
	{
		String name = request.getParameter("name");
		String introduce = request.getParameter("introduce");
		String message = request.getParameter("message");
		int igid = Integer.parseInt(request.getParameter("igid"));
		interestgroupServiceImpl.modifyBaseInfo(igid, name, introduce,message);
		responseStream = new ByteArrayInputStream("ok".getBytes());
		return SUCCESS;
	}
	public String interestGroupAddMemberStream()
	{
		int igid = Integer.parseInt(request.getParameter("igid"));
		String ids[] = request.getParameterValues("mids");
		int mids[] = new int[ids.length];
		for(int i=0;i<mids.length;i++)
		{
			mids[i]=Integer.parseInt(ids[i]);
		
		}
		interestgroupServiceImpl.addmember(igid, mids);
		responseStream = new ByteArrayInputStream("ok".getBytes());
		return SUCCESS;
	}
	public String interestGroupDeleteMemberStream()
	{
		int igid = Integer.parseInt(request.getParameter("igid"));
		String ids[] = request.getParameterValues("mids");
		int mids[] = new int[ids.length];
		for(int i=0;i<mids.length;i++)
		{
			mids[i]=Integer.parseInt(ids[i]);
			
		}
		interestgroupServiceImpl.deletemember(igid, mids);
		responseStream = new ByteArrayInputStream("ok".getBytes());
		return SUCCESS;
	}
	public	String interestGroupListJson()
	{
		int num = Integer.parseInt(request.getParameter("num"));
		int offset = Integer.parseInt(request.getParameter("offset"));
		iglist = interestgroupServiceImpl.getInterestGroupJson(num, offset);
		return "list";
	}
	public String interestGroupMineListJson()
	{
		int uid = Integer.parseInt(request.getParameter("uid"));
		iglist = interestgroupServiceImpl.getInterestGroupJson(uid);
		return "list";
	}
	public String interestGroupBaseInfoJson()
	{
		int igid = Integer.parseInt(request.getParameter("igid"));
		InterestGroup interestGroup = interestgroupServiceImpl.getInterestGroup(igid);
		igmap = new HashMap<String, String>();
		igmap.put("id", String.valueOf(interestGroup.getId()));
		igmap.put("introduce", interestGroup.getIntroduce());
		igmap.put("name", interestGroup.getName());
		igmap.put("master_name", interestGroup.getCreatUser().getName());
		igmap.put("message", interestGroup.getMessage());
		igmap.put("gid", interestGroup.getGid());
		return "map";
	}
	public InputStream getResponseStream() {
		return responseStream;
	}
	public void setResponseStream(InputStream responseStream) {
		this.responseStream = responseStream;
	}
	public List getIglist() {
		return iglist;
	}
	public void setIglist(List iglist) {
		this.iglist = iglist;
	}
	public Map getIgmap() {
		return igmap;
	}
	public void setIgmap(Map igmap) {
		this.igmap = igmap;
	}
	public PagerUtil getPagerUtil() {
		return pagerUtil;
	}
	@Resource
	public void setPagerUtil(PagerUtil pagerUtil) {
		this.pagerUtil = pagerUtil;
	}
	
}
