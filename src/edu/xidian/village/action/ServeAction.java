package edu.xidian.village.action;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import org.springframework.stereotype.Controller;

import edu.xidian.village.util.Fetch;

@Controller("serveAction")
public class ServeAction extends SuperAction {

	private String result;
	private InputStream inputStream;

	public InputStream getInputStream() {
		return inputStream;
	}

	public void setInputStream(InputStream inputStream) {
		this.inputStream = inputStream;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getMaintain() {

		String username = request.getParameter("username");

		String pagenum = request.getParameter("pagenum");
		try {
			String result =Fetch
					.fetchMaintainInputStream(username, pagenum);
			if(result.equals("false")||result=="false")
				inputStream = new ByteArrayInputStream("error".getBytes());
			else
				inputStream = new ByteArrayInputStream(result.getBytes());
			
		} catch (Exception e) {
			inputStream = new ByteArrayInputStream("error".getBytes());
		}
		return SUCCESS;
	}

}
