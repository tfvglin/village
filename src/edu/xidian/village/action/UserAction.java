package edu.xidian.village.action;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.json.JSONObject;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import edu.xidian.village.service.UserService;
import edu.xidian.village.util.Fetch;
import edu.xidian.village.util.ImgStream;
import edu.xidian.village.util.PagerUtil;
import edu.xidian.village.util.jpush.JIMUtil;
import edu.xidian.village.vo.User;

@Controller("userAction")
@Scope("prototype") 
public class UserAction<T> extends SuperAction {
	
	private User user;
	private UserService userServiceImpl;
	private JIMUtil jImUtil;
	private PagerUtil pagerUtil;

	private Map<String,Object> umap = new HashMap<String, Object>();
	private Set<User> uset;
	private List ulist;
	private List list;
	private String imageCode;
	private String sign ;
	private InputStream responseStream;
	
	 
	public PagerUtil getPagerUtil() {
		return pagerUtil;
	}
	@Resource
	public void setPagerUtil(PagerUtil pagerUtil) {
		this.pagerUtil = pagerUtil;
	}



	public String getImageCode() {
		return imageCode;
	}

	public void setImageCode(String imageCode) {
		this.imageCode = imageCode;
	}




	public InputStream getResponseStream() {
		return responseStream;
	}
	public void setResponseStream(InputStream responseStream) {
		this.responseStream = responseStream;
	}
	public String getSign() {
		return sign;
	}


	public void setSign(String sign) {
		this.sign = sign;
	}


	public Map<String, Object> getUmap() {
		return umap;
	}


	public void setUmap(Map<String, Object> umap) {
		this.umap = umap;
	}


	
	public User getUser() {
		return user;
	}


	public void setUser(User user) {
		this.user = user;
	}


	
	public UserService getUserServiceImpl() {
		return userServiceImpl;
	}

	@Resource
	public void setUserServiceImpl(UserService userServiceImpl) {
		this.userServiceImpl = userServiceImpl;
	}
	public String userInfoJsp()
	{
		int uid = Integer.parseInt(request.getParameter("user.id"));
		User user = userServiceImpl.getUser(uid);
		request.setAttribute("user", user);
		return SUCCESS;
	}
	
	public String userInfo()
	{
		String name = request.getParameter("user.name");
		user=userServiceImpl.getUser(name);
		umap.put("id", user.getId());
		umap.put("name", user.getName());
		umap.put("nearname", user.getNearName());
		umap.put("realname", user.getRealName());
		umap.put("phone", user.getPhone());
		umap.put("photo", user.getPhoto());
		
		return "map";
	}


	public String userModify()
	{
		String imgCode;
		User u;
		try {
			JSONObject jsonObject ;
			String jsonStr = Fetch.fetchInputStream(request.getInputStream());
			jsonObject = new JSONObject(jsonStr);
			
			imgCode = jsonObject.getString("img");
			String email = jsonObject.getString("user.email");
			String name = jsonObject.getString("user.name");
			String nearName = jsonObject.getString("user.nearName");
			String realName = jsonObject.getString("user.realName");
			String phone = jsonObject.getString("user.phone");
			u = userServiceImpl.getUser(name);
			
			if(email!=null&&!email.equals("null"))
			u.setEmail(email);
			if(name!=null&&!name.equals("null"))
			u.setName(name);
			if(nearName!=null&&!nearName.equals("null"))
			u.setNearName(nearName);
			if(realName!=null&&!realName.equals("null"))
			u.setRealName(realName);
			if(phone!=null&&!phone.equals("null"))
			u.setPhone(phone);
			
/*			user =userServiceImpl.getUser(u.getName());
			u.setId(user.getId());
			u.setPassword(user.getPassword());*/
		} catch (Exception e1) {
			
			e1.printStackTrace();
			sign="error";
			responseStream =  new   ByteArrayInputStream(sign.getBytes());
			return SUCCESS;
		}
		if(imgCode!=null&&!imgCode.equals("null")&&!imgCode.equals(""))
		{
			u.setPhoto("1");
			String path = application.getRealPath("/")+"uploads/userhead/";
			sign=(ImgStream.base64StringToImage(imgCode,u.getName(),path)&&userServiceImpl.saveUserInfo(u))?"ok":"error";
		}
		else
		
			sign=(userServiceImpl.saveUserInfo(u))?"ok":"error";
		
		responseStream =  new   ByteArrayInputStream(sign.getBytes());
		return SUCCESS;
		}

	public String userListJson()
	{
		ulist = userServiceImpl.list();
		return SUCCESS;
	}
	public String userDeleteStream()
	{
		String[] ulist = request.getParameterValues("ulist[]");
		
		int[] uids = new int[ulist.length];  
	for(int i=0;i<ulist.length;i++){  
		uids[i]=Integer.parseInt(ulist[i]);   
	}
	if(userServiceImpl.deleteUser(uids))
		responseStream = new ByteArrayInputStream("success".getBytes());
	else
		responseStream =  new ByteArrayInputStream("error".getBytes());
	return SUCCESS;
	}
	
	public String userFriendAddStream()
	{
		
		String username =  request.getParameter("username");
		String friendname = request.getParameter("friendname");
		if(username!=null&&friendname!=null)
		{
			String s = userServiceImpl.checkFriend(username, friendname);
			if(s.equals("yes"))
				sign="isfriend";
			else{
				userServiceImpl.addFriend(username,friendname);
				sign="ok";
			}
			
		}
		
		String fid = request.getParameter("fid");
		String uid = request.getParameter("uid");
		if(fid!=null&&uid!=null)
		{
			String s = userServiceImpl.checkFriend(uid,fid);
			if(s.equals("yes"))
				sign="isfriend";
			else{
				userServiceImpl.addFriend(Integer.valueOf(fid), Integer.valueOf(uid));
				
				sign="ok";
			}
		}
			
		responseStream = new ByteArrayInputStream(sign.getBytes());
		return SUCCESS;
	}
	public String userFriendDeleteStream(){
		String username =  request.getParameter("username");
		String friendname = request.getParameter("friendname");
		if(username!=null&&friendname!=null)
		{
			String s = userServiceImpl.checkFriend(username, friendname);
			if(s.equals("no"))
				sign="isnotfriend";
			else{
				userServiceImpl.deleteFriend(username,friendname);
				sign="ok";
			}
			
		}
		String fid = request.getParameter("fid");
		String uid = request.getParameter("uid");
		if(fid!=null&&uid!=null)
		{
			String s = userServiceImpl.checkFriend(uid,fid);
			if(s.equals("no"))
				sign="isnotfriend";
			else{
				userServiceImpl.deleteFriend(Integer.valueOf(fid), Integer.valueOf(uid));
				
				sign="ok";
			}
		}
		
		responseStream = new ByteArrayInputStream(sign.getBytes());
		return SUCCESS;
	}
	public String userFriendCheckStream()
	{
		
		String username =  request.getParameter("username");
		String friendname = request.getParameter("friendname");
		
		String sign =userServiceImpl.checkFriend(username,friendname);
		responseStream = new ByteArrayInputStream(sign.getBytes());
		return SUCCESS;
	}
	public String userFriendListJson()
	{
		int uid = Integer.parseInt(request.getParameter("uid"));
		//int offset = Integer.parseInt(request.getParameter("offset"));
		//int num = Integer.parseInt(request.getParameter("num"));
		
		ulist = userServiceImpl.getFriendList(uid);
		return "list";
	}
	
	public String userRegistStream()
	{
	
		if(user==null)
		{
			sign="格式错误";
			responseStream =  new   ByteArrayInputStream(sign.getBytes());
			return SUCCESS;
		}
		if(userServiceImpl.find(user)!=null)
			sign="exist";
		else{
			//String userImg = request.getParameter("userImg");
			user.setPhoto("2");
			if(userServiceImpl.addUser(user))
			{
				
				if(jImUtil.userRegist(user).equals("success")){
					sign="ok";
				}
				else
				{
					userServiceImpl.deleteUser(user);
					sign="IM error";
				}
			}
			else
				sign="error";
		}
		responseStream =  new   ByteArrayInputStream(sign.getBytes());
		return SUCCESS;
	}
	public String userCheckLoginStream()
	{
		String token = request.getParameter("token");
		if(userServiceImpl.checkToken(user.getName(), token))
		{
			sign="ok";
		}
		else
			sign="error";
		responseStream =  new   ByteArrayInputStream(sign.getBytes());
		return SUCCESS;
	}
	public String userLoginJson()
	{
		if(userServiceImpl.find(user)==null)
			umap.put("sign","notexist");
		else if(userServiceImpl.checkPassword(user))
		{
			User u = userServiceImpl.getUser(user.getName());
			userServiceImpl.updateLastTime(u);
			Map<String,Object> userInfo = new HashMap<String, Object>();
			
			userInfo.put("token", u.getId());
			userInfo.put("username",u.getName());
			userInfo.put("realname",u.getRealName());
			userInfo.put("email",u.getEmail());
			userInfo.put("nearname",u.getNearName());
			userInfo.put("photo",u.getPhoto());
			userInfo.put("userid",u.getId());
			userInfo.put("phone", u.getPhone());
		
			
			umap.put("userInfo",userInfo);
			umap.put("sign","ok");
			
		}
		else
			umap.put("sign","error");
			
		//inputStream = new StringBufferInputStream(sign);

		return "map";
	}

	public String userLogout()
	{
		request.getSession().removeAttribute("user");
		sign="ok";
		responseStream =  new   ByteArrayInputStream(sign.getBytes());
		return SUCCESS;
	}

	public String userInterestGroupListJson()
	{
		int igid = Integer.parseInt(request.getParameter("igid"));
		int num = Integer.parseInt(request.getParameter("num"));
		int offset = Integer.parseInt(request.getParameter("offset"));
		ulist = userServiceImpl.getInterestGroupUserListJson(num,offset,igid);
		return "list";
	}
	public String userListJsp()
	{
		List<User> ulist =userServiceImpl.list();
		
		this.pagerUtil.Pager(ulist, request);
		return SUCCESS;
	}
	public String userSearchJsp()
	{
		List<User> ulist = userServiceImpl.searchUser(user);
		this.pagerUtil.Pager(ulist, request);
		return "LIST";
	}
	public Set<User> getUset() {
		return uset;
	}
	public void setUset(Set<User> uset) {
		this.uset = uset;
	}
	public List getUlist() {
		return ulist;
	}
	public void setUlist(List ulist) {
		this.ulist = ulist;
	}
	

	public JIMUtil getjImUtil() {
		return jImUtil;
	}
	@Resource
	public void setjImUtil(JIMUtil jImUtil) {
		this.jImUtil = jImUtil;
	}
	public List getList() {
		return list;
	}
	public void setList(List list) {
		this.list = list;
	}
	
	
	 
}
