package edu.xidian.village.action;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;

import edu.xidian.village.service.InterestGroupService;
import edu.xidian.village.service.UserService;
import edu.xidian.village.vo.InterestGroup;
import edu.xidian.village.vo.User;
@Controller
public class MobileAction extends SuperAction{

	private UserService userServiceImpl;
	private InterestGroupService interestGroupServiceImpl;
	private Map<String,Object> map;
	private List list;
	public UserService getUserServiceImpl() {
		return userServiceImpl;
	}
	@Resource
	public void setUserServiceImpl(UserService userServiceImpl) {
		this.userServiceImpl = userServiceImpl;
	}
	public InterestGroupService getInterestGroupServiceImpl() {
		return interestGroupServiceImpl;
	}
	@Resource
	public void setInterestGroupServiceImpl(
			InterestGroupService interestGroupServiceImpl) {
		this.interestGroupServiceImpl = interestGroupServiceImpl;
	}
	
	public String mobileUserAndInterestGroupSearchJson()
	{
		map=new HashMap<String, Object>();
		List<Map<String,Object>> list = new ArrayList<Map<String,Object>>();
		String key = request.getParameter("key");
		User user = userServiceImpl.findUser("realName", key);
		if(user!=null)
			list.add(searchRecombine(user));
		user =userServiceImpl.findUser("name", key);
		if(user!=null)
			list.add(searchRecombine(user));
		if(!list.isEmpty())
			map.put("user", list);
		
		InterestGroup interestGroup = interestGroupServiceImpl.findInterestGroup("name", key);

		if(interestGroup!=null)
		{
			List<Map<String,Object>> interestGrouplist = new ArrayList<Map<String,Object>>();
			Map<String,Object> searchmap =  new HashMap<String, Object>();
			searchmap.put("name",interestGroup.getName());
			searchmap.put("introduce", interestGroup.getIntroduce());
			searchmap.put("message", interestGroup.getMessage());
			searchmap.put("id", interestGroup.getId());
			searchmap.put("picture", interestGroup.getPicture());
			searchmap.put("datetime", interestGroup.getDatetime());
			searchmap.put("master_name", interestGroup.getCreatUser().getName());
			searchmap.put("gid", interestGroup.getGid());
			interestGrouplist.add(searchmap);
			map.put("interestGroup", interestGrouplist);
		}
		return "map";
	}
	private Map<String,Object> searchRecombine(User user)
	{
		Map<String,Object> searchmap =  new HashMap<String, Object>();
		searchmap.put("id", user.getId());
		searchmap.put("name", user.getName());
		searchmap.put("email", user.getEmail());
		searchmap.put("phone", user.getPhone());
		searchmap.put("realname", user.getRealName());
		searchmap.put("nearname", user.getNearName());
		searchmap.put("photo", user.getPhoto());
		return searchmap;
	}
	public List getList() {
		return list;
	}
	public void setList(List list) {
		this.list = list;
	}
	public Map<String, Object> getMap() {
		return map;
	}
	public void setMap(Map<String, Object> map) {
		this.map = map;
	}
	
	
}
