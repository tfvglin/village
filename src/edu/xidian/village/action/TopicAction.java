package edu.xidian.village.action;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.List;

import javax.annotation.Resource;

import org.json.JSONObject;
import org.springframework.stereotype.Controller;

import edu.xidian.village.service.TopicService;
import edu.xidian.village.util.Fetch;
import edu.xidian.village.util.ImgStream;
import edu.xidian.village.util.PagerUtil;
import edu.xidian.village.vo.Topic;

@Controller
public class TopicAction extends SuperAction{

	private TopicService topicServiceImpl;
	private InputStream responseStream;
	private List tlist;
	private PagerUtil pagerUtil;
	
	public PagerUtil getPagerUtil() {
		return pagerUtil;
	}
	@Resource
	public void setPagerUtil(PagerUtil pagerUtil) {
		this.pagerUtil = pagerUtil;
	}
	public TopicService getTopicServiceImpl() {
		return topicServiceImpl;
	}
	@Resource
	public void setTopicServiceImpl(TopicService topicServiceImpl) {
		this.topicServiceImpl = topicServiceImpl;
	}
	public String topicAddStream()
	{
		
		String imgCode;
		String message;
		String picture;
		Long datetime;
		int uid;
	
		try {
			JSONObject jsonObject ;
			String jsonStr = Fetch.fetchInputStream(request.getInputStream());
			jsonObject = new JSONObject(jsonStr);
			
			imgCode = jsonObject.getString("img");
			message= jsonObject.getString("message");
			datetime = Long.parseLong(jsonObject.getString("datetime"));
			uid = Integer.parseInt(jsonObject.getString("uid"));
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			responseStream =  new  ByteArrayInputStream("error".getBytes());
			return SUCCESS;
		}
		if(imgCode!=null&&!imgCode.equals("null"))
		{
			picture = String.valueOf(datetime);
			String path = application.getRealPath("/")+"uploads/topic/";
			ImgStream.base64StringToImage(imgCode,picture,path);
		}
		else{
			picture="";
		}
		int tid =topicServiceImpl.addTopic(message, datetime, picture, uid);
		responseStream = new ByteArrayInputStream("ok".getBytes());
		return SUCCESS;
	}
	public String topicPraiseStream()
	{
		int uid = Integer.parseInt(request.getParameter("uid"));
		int tid = Integer.parseInt(request.getParameter("tid"));
		if(topicServiceImpl.Praise(tid, uid))
			responseStream = new ByteArrayInputStream("ok".getBytes());
		else
			responseStream = new ByteArrayInputStream("repeat".getBytes());
		return SUCCESS;
	}

	public String topicDeleteStream()
	{
		String[] tlist = request.getParameterValues("tlist[]");
		
		int[] tids = new int[tlist.length];  
		for(int i=0;i<tlist.length;i++){  
			tids[i]=Integer.parseInt(tlist[i]);   
		}
		if(topicServiceImpl.deleteTopic(tids))
			responseStream = new ByteArrayInputStream("success".getBytes());
		else
			responseStream =  new ByteArrayInputStream("error".getBytes());
		return SUCCESS;
	}
	
	
	public String topicViewJsp()
	{
		int tid = Integer.parseInt(request.getParameter("tid"));
		Topic topic =topicServiceImpl.getTopic(tid);
		request.setAttribute("topic", topic);
		this.pagerUtil.Pager(topic.getReply(), request);
		return SUCCESS;
	}
	public String topicListJsp()
	{
		List<Topic> tlist =topicServiceImpl.getTopicList();
		this.pagerUtil.Pager(tlist, request);
		return SUCCESS;
	}
	public String topicListJson()
	{
		int n = Integer.parseInt(request.getParameter("num"));
		int offset = Integer.parseInt(request.getParameter("offset"));
		//List<Topic> list = topicServiceImpl.getTopicList(n, offset);
		tlist = topicServiceImpl.getTopicJson(n, offset);
		return SUCCESS;
	}
	public String topicFriendListJson()
	{
		int n = Integer.parseInt(request.getParameter("num"));
		int offset = Integer.parseInt(request.getParameter("offset"));
		int uid = Integer.parseInt(request.getParameter("uid"));
		tlist = topicServiceImpl.getTopicJson(n, offset, uid);
		return SUCCESS;
	}

	public String topicSetHotStream()
	{
		int tid = Integer.valueOf(request.getParameter("tid"));
		topicServiceImpl.setTopicHot(tid);
		responseStream = new ByteArrayInputStream("success".getBytes());
		return SUCCESS;
	}
	public String topicSetNormalStream()
	{
		int tid = Integer.valueOf(request.getParameter("tid"));
		topicServiceImpl.setTopicNormal(tid);
		responseStream = new ByteArrayInputStream("success".getBytes());
		return SUCCESS;
	}
	public String topicSearchJsp()
	{
		String username= request.getParameter("username");
		String temp = request.getParameter("isHot");
		if(temp==null)
		{
			Character i = null;
		}
		Character isHot =(temp==null||temp.equals(""))?null:temp.charAt(0);
		this.pagerUtil.Pager(topicServiceImpl.searchTopic(username, isHot), request);
		return "LIST";
	}
	public String topicHotJson()
	{
		tlist = topicServiceImpl.getHotTopic();
		return SUCCESS;
	}
	
	
	public InputStream getResponseStream() {
		return responseStream;
	}
	public void setResponseStream(InputStream responseStream) {
		this.responseStream = responseStream;
	}
	public List getTlist() {
		return tlist;
	}
	public void setTlist(List tlist) {
		this.tlist = tlist;
	}

}
