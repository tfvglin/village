package edu.xidian.village.action;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.json.JSONObject;
import org.springframework.stereotype.Controller;

import edu.xidian.village.service.RentHouseService;
import edu.xidian.village.util.Fetch;
import edu.xidian.village.util.ImgStream;
import edu.xidian.village.util.PagerUtil;
import edu.xidian.village.vo.RentHouse;

@Controller
public class RentHouseAction extends SuperAction{

	private RentHouseService rentHouseServiceImpl;
	private String sign;
	private InputStream responseStream;
	private Fetch fetch;
	private List list;
	private PagerUtil pagerUtil;
	
	public String rentHouseListJsp()
	{
		List<RentHouse> list = rentHouseServiceImpl.getRentHouseList();
		this.pagerUtil.Pager(list, request);
		return SUCCESS;
	}
	
	public String rentHouseAddStream()
	{
		String imgCode;
		String picture,address,introduce,telephone,money;
		Character type,area;
		Integer uid;
		try {
			JSONObject jsonObject ;
			String jsonStr = Fetch.fetchInputStream(request.getInputStream());
			jsonObject = new JSONObject(jsonStr);
			
			imgCode = jsonObject.getString("picture");
			address = jsonObject.getString("address");
			introduce = jsonObject.getString("introduce");
			type = jsonObject.getString("type").charAt(0);
			area = jsonObject.getString("area").charAt(0);
			telephone = jsonObject.getString("telephone");
			money = jsonObject.getString("money");
			uid = Integer.valueOf(jsonObject.getString("uid"));
			
		} catch (Exception e1) {
			
			e1.printStackTrace();
			sign="error";
			setResponseStream(new   ByteArrayInputStream(sign.getBytes()));
			return SUCCESS;
		}
		if(imgCode!=null&&!imgCode.equals("null")&&!imgCode.equals(""))
		{
			picture = type.toString()+area.toString()+new Date().getTime();
			String path = application.getRealPath("/")+"uploads/RentHouse/";
			
			if(ImgStream.base64StringToImage(imgCode,picture,path)){
				rentHouseServiceImpl.addRentHouse(address, introduce, picture, type, area, telephone, money, uid);
				sign = "ok";
			}
		}
		else
		{
			picture ="1";
			rentHouseServiceImpl.addRentHouse(address, introduce, picture, type, area, telephone, money, uid);
			sign="ok";
		}
		
			
		
		setResponseStream(new ByteArrayInputStream(sign.getBytes()));
		return SUCCESS;
	}
	
	public String rentHouseListJson()
	{
		Integer num = Integer.valueOf(request.getParameter("num"));
		Integer page = Integer.valueOf(request.getParameter("page"));
		list = rentHouseServiceImpl.getRentHouseList(num, page);
		return SUCCESS;
	}
	public String rentHouseUserListJson()
	{
		Integer num = Integer.valueOf(request.getParameter("num"));
		Integer page = Integer.valueOf(request.getParameter("page"));	
		String username = request.getParameter("username");
		list = rentHouseServiceImpl.getRentHouseList(username, num, page);
		return SUCCESS;
	}
	
	public RentHouseService getRentHouseServiceImpl() {
		return rentHouseServiceImpl;
	}
	@Resource
	public void setRentHouseServiceImpl(RentHouseService rentHouseServiceImpl) {
		this.rentHouseServiceImpl = rentHouseServiceImpl;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

	public InputStream getResponseStream() {
		return responseStream;
	}

	public void setResponseStream(InputStream responseStream) {
		this.responseStream = responseStream;
	}

	public Fetch getFetch() {
		return fetch;
	}
	@Resource
	public void setFetch(Fetch fetch) {
		this.fetch = fetch;
	}

	public List getList() {
		return list;
	}
	
	public void setList(List list) {
		this.list = list;
	}

	public PagerUtil getPagerUtil() {
		return pagerUtil;
	}
	@Resource
	public void setPagerUtil(PagerUtil pagerUtil) {
		this.pagerUtil = pagerUtil;
	}
	
}
