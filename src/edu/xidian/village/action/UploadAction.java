package edu.xidian.village.action;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;

import org.springframework.stereotype.Controller;

@Controller("uploadAction")
public class UploadAction extends SuperAction {
	
	private String title;
	private File uploadify;
	private String uploadContentType;
	private String fileName;
	private String savePath;
	private InputStream responseStream;

	public InputStream getResponseStream() {
		return responseStream;
	}

	public void setResponseStream(InputStream responseStream) {
		this.responseStream = responseStream;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public File getUploadify() {
		return uploadify;
	}

	public void setUploadify(File uploadify) {
		this.uploadify = uploadify;
	}

	public String getUploadContentType() {
		return uploadContentType;
	}

	public void setUploadContentType(String uploadContentType) {
		this.uploadContentType = uploadContentType;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getSavePath() {
		return savePath;
	}

	public void setSavePath(String savePath) {
		this.savePath = savePath;
	}

	public String notificationUpload() {
	
		
		
		String path = application.getRealPath("/") + "uploads\\notification\\" + getTitle() + ".jpg";
	
		if(Upload(path))
		{
			String imgPath = application.getContextPath()+"/uploads/notification/" + getTitle() + ".jpg";
			responseStream = new ByteArrayInputStream(imgPath.getBytes());
		}
		else
			responseStream = new ByteArrayInputStream("error".getBytes());
			
		return SUCCESS;
	}

	private boolean Upload(String path) {
		try {

			
			File w2 = new File(path);
			FileOutputStream fos = new FileOutputStream(w2);
			FileInputStream fis = new FileInputStream(getUploadify());
			byte[] buffer = new byte[1024];
			int len = 0;
			while ((len = fis.read(buffer)) > 0) {
				fos.write(buffer, 0, len);
			}
			fos.close();
			fis.close();
			return true;
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
	}

}
