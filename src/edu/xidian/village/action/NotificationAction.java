package edu.xidian.village.action;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import edu.xidian.village.service.NotificationService;
import edu.xidian.village.util.PagerUtil;
import edu.xidian.village.vo.Notification;

@Controller("notificationAction")
@Scope("prototype") 
public class NotificationAction extends SuperAction {

	private String sign;

	private Notification notification;
	
	private NotificationService<Notification> notificationServiceImple;
	private PagerUtil pagerUtil;
	private List<Notification> nlist = new ArrayList<Notification>();
	private InputStream responseStream ;

	
	


	public InputStream getResponseStream() {
		return responseStream;
	}
	public void setResponseStream(InputStream responseStream) {
		this.responseStream = responseStream;
	}
	public String getSign() {
		return sign;
	}
	public void setSign(String sign) {
		this.sign = sign;
	}
	public Notification getNotification() {
		return notification;
	}
	public void setNotification(Notification notification) {
		this.notification = notification;
	}
	
	public NotificationService<Notification> getNotificationServiceImple() {
		return notificationServiceImple;
	}
	@Resource
	public void setNotificationServiceImple(
			NotificationService<Notification> notificationServiceImple) {
		this.notificationServiceImple = notificationServiceImple;
	}
	public List<Notification> getNlist() {
		return nlist;
	}
	public void setNlist(List<Notification> nlist) {
		this.nlist = nlist;
	}
	public String notificationModifyJsp()
	{
		int noid = Integer.parseInt(request.getParameter("noid"));
		Notification notification = notificationServiceImple.getNotification(noid);
		
		request.setAttribute("no", notification);
	
		return SUCCESS;
	}
	public String notificationModifyStream()
	{
		int noid = Integer.parseInt(request.getParameter("noid"));
		String title = request.getParameter("title");
		String message = request.getParameter("message");
		String text = request.getParameter("text");
		notificationServiceImple.modifyBase(title, message,text, noid);
		responseStream = new ByteArrayInputStream("success".getBytes());
		return SUCCESS;
	}
	public String notificationList()
	{
		
	
		try {
			int notificationcount =Integer.parseInt(request.getParameter("count")); 
			int offset =Integer.parseInt(request.getParameter("offset")); 
			nlist = notificationServiceImple.getNotificationList(notificationcount,offset);
			
			if(nlist.isEmpty())
				sign="error";
			else
				sign="ok";
		} catch (Exception e) {
			
			e.printStackTrace();
			sign="error";
		}
		
		return SUCCESS;
	}
	
	public String notificationListJsp()
	{
		try {
			List<Notification> nlist = notificationServiceImple.getNotificationList();
			this.pagerUtil.Pager(nlist, request);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return SUCCESS;
	}
	public String notificationIssueStream()
	{
		notification.setDatetime(new Date().getTime());	
		
		if(notificationServiceImple.addNotification(notification))
			
			sign="success";
		else
		
			sign="error";
		responseStream = new ByteArrayInputStream(sign.getBytes());
		return SUCCESS;
	}
	
	public String notificationDeleteStream(){
		String[] nidlist = request.getParameterValues("nolist[]");
	
			int[] nids = new int[nidlist.length];  
		for(int i=0;i<nidlist.length;i++){  
			nids[i]=Integer.parseInt(nidlist[i]);   
		}
		if(notificationServiceImple.deleteNotification(nids))
			responseStream = new ByteArrayInputStream("success".getBytes());
		else
			responseStream =  new ByteArrayInputStream("error".getBytes());
		return SUCCESS;
	}
	

	
	public String notificationSearchJsp()
	{
		String title = request.getParameter("title");
		List<Notification> nlist = notificationServiceImple.searchNotification(title);
		this.pagerUtil.Pager(nlist, request);
	
		return "LIST";
	}
	public PagerUtil getPagerUtil() {
		return pagerUtil;
	}
	@Resource
	public void setPagerUtil(PagerUtil pagerUtil) {
		this.pagerUtil = pagerUtil;
	}
	
	

}
