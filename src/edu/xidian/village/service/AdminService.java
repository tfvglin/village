package edu.xidian.village.service;

import java.util.List;

import edu.xidian.village.vo.Admin;
import edu.xidian.village.vo.Notification;
import edu.xidian.village.vo.NotificationComment;
import edu.xidian.village.vo.User;

public interface AdminService {
	
	public boolean login(Admin admin);
	
	public boolean logout();

	public boolean checkPassword(Admin admin);
	
	public Admin getAdmin(Admin admin);
	
	public List<User> getUserList();
	public List<User> getUserList(User user);
	public User getUser(User user);
	public void updateUser(User user);
	
	public void deleteUser(User user);
	public void deleteUser(List<User> ulist);
	
	public void deleteNotificationComment(NotificationComment nc);
	public void deleteNotificationComment(List<NotificationComment> nclist);
	
	public Notification getNotification(Notification no);
	public void updateNotification(Notification no);
	public void deleteNotification(Notification no);
	public void deleteNotification(List<Notification> nolist);
	
	public List<Notification> getNotificationList() throws Exception;
	public List<Notification> getNotificationList(Notification notification) throws Exception;
	
	public void postMessage();
	
	public void sentNotification();
}
