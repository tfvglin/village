package edu.xidian.village.service;

import java.util.List;
import java.util.Map;

import edu.xidian.village.vo.Reply;

public interface ReplyService {

	void addReply(int tid,int uid,String message,Long datetime);
	
	List<Reply> getReplyList(int tid,int n, int offset);
	List<Reply> getReplyList(int tid);
	
	List<Map<String,String>> getReplyJson(int tid,int n,int offset);
	
	boolean deleteReply(int[] rids);
}
