package edu.xidian.village.service;

import java.util.List;
import java.util.Map;
import java.util.Set;

import edu.xidian.village.vo.User;

public interface UserService {
	
	public boolean addUser(User user);
	public void deleteUser(User user);
	
	public boolean deleteUser(int[] uids);
	
	public boolean checkPassword(User user);
	
	public boolean logout();
	
	public void postTopic();

	public User find(User user);
	User findUser(String propertyName,String value);
	public User getUser(String name);
	public User getUser(int id);
	public User getUser(User user);
	
	public boolean saveUserInfo(User user);

	public boolean checkToken(String name,String token);

	public void addFriend(int[] ids, int uid);
	public void addFriend(int fid, int uid);
	public void addFriend(String username, String friendname);
	
	public void deleteFriend(int fid, int uid);
	public void deleteFriend(String username, String friendname);

	public String checkFriend(String username,String friendname);
	public String checkFriend(int uid,int fid);
	
	public Set<User> getFriend(User user);
	public Set<User> getFriend(int uid);
	public Set<User> getFriend(String  name);
	public List<Map<String,String>> getFriendList(int offset,int num,int uid);
	public List<Map<String,String>> getFriendList(int uid);
	
	public void update(User user);
	
	List<User> list();
	
	List<Map<String,String>> getInterestGroupUserListJson(int num,int offset,int igid);
	
	User getUserLa(int uid);
	
	long getUserNum();
	long getActiveUserNum(Long date);
	
	public void updateLastTime(User user);
	
	public List<User> searchUser(User user);
}
