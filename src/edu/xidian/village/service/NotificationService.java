package edu.xidian.village.service;

import java.util.List;

import edu.xidian.village.vo.Notification;



public interface NotificationService<T> {
	
	public List<Notification> getNotificationList(int n,int offset) throws Exception;
	public List<Notification> getNotificationList();
	
	public boolean addNotification(T t);

	public Notification getNotification(int noid);

	public void modifyBase(String title,String message,String text,int noid);
	
	public long getNotificationNum();
	
	public boolean deleteNotification(int[] nids);
	
	public List<Notification> searchNotification(String title);
}
