package edu.xidian.village.service;

import java.util.List;
import java.util.Map;

import edu.xidian.village.vo.FriendRequest;

public interface FriendRequestService {

	void sendRequest(FriendRequest friendRequest);
	
	void modifyState(FriendRequest friendRequest,String state);
	
	void modifyState(int frid,String state);
	
	FriendRequest getFriendRequest(int frid);
	
	List<Map<String,String>> sendRequestJson(int uid);
	
	List<Map<String,String>> receiveRequestJson(int uid);
	
	boolean deleteFriendRequest(int[] uids);
}
