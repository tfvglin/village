package edu.xidian.village.service;

import java.util.List;

import edu.xidian.village.vo.Message;

public interface MessageService {

	Message addMessage(Message message);
	
	Message addMessage(String title,String content);
	
	List<Message> getMessageList();
}
