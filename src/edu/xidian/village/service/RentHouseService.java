package edu.xidian.village.service;

import java.util.List;
import java.util.Map;

import edu.xidian.village.vo.RentHouse;

public interface RentHouseService {

	public void addRentHouse(String address,String introduce,String picture,Character type,Character area,String telephone,String money,int uid);
	
	public List<RentHouse> getRentHouseList();
	
	public List<Map<String, String>> getRentHouseList(int num,int page);

	public List<Map<String, String>> getRentHouseList(String username,int num,int page);
	
	public List<RentHouse> getRentHouseList(String username);
	
}
