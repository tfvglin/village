package edu.xidian.village.service.impl;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import edu.xidian.village.dao.MessageDao;
import edu.xidian.village.service.MessageService;
import edu.xidian.village.vo.Message;
@Component
public class MessageServiceImpl implements MessageService{

	private MessageDao messageDaoImpl;
	
	
	@Override
	public Message addMessage(Message message) {
		Integer mid = messageDaoImpl.add(message);
		return messageDaoImpl.get(mid);
		
	}


	public MessageDao getMessageDaoImpl() {
		return messageDaoImpl;
	}

	@Resource
	public void setMessageDaoImpl(MessageDao messageDaoImpl) {
		this.messageDaoImpl = messageDaoImpl;
	}


	@Override
	public Message addMessage(String title,String content) {
		Message message = new Message(content,title);
		message.setDatetime(new Date().getTime());
		Integer mid= this.messageDaoImpl.add(message);
		return messageDaoImpl.get(mid);
		
	}


	@Override
	public List<Message> getMessageList() {
		return messageDaoImpl.list();
		
	}

}
