package edu.xidian.village.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import edu.xidian.village.dao.UserDao;
import edu.xidian.village.service.FriendRequestService;
import edu.xidian.village.service.InterestGroupService;
import edu.xidian.village.service.UserService;
import edu.xidian.village.vo.User;

@Service("userServiceImpl")
public class UserServiceImpl implements UserService{

	private UserDao userDaoImpl;
	private InterestGroupService interestGroupServiceImpl;
	private FriendRequestService friendRequestServiceImpl;
	
	
	public FriendRequestService getFriendRequestServiceImpl() {
		return friendRequestServiceImpl;
	}
	@Resource
	public void setFriendRequestServiceImpl(
			FriendRequestService friendRequestServiceImpl) {
		this.friendRequestServiceImpl = friendRequestServiceImpl;
	}
	public InterestGroupService getInterestGroupServiceImpl() {
		return interestGroupServiceImpl;
	}
	@Resource
	public void setInterestGroupServiceImpl(
			InterestGroupService interestGroupServiceImpl) {
		this.interestGroupServiceImpl = interestGroupServiceImpl;
	}
	public UserDao getUserDaoImpl() {
		return userDaoImpl;
	}
	@Resource
	public void setUserDaoImpl(UserDao userDaoImpl) {
		this.userDaoImpl = userDaoImpl;
	}
	
	@Override
	public boolean addUser(User user) {
		user.setLasttime(new Date().getTime());
		if(userDaoImpl.add(user)!=null)
		{
			
			return true;
		}
		else
			return false;
	}

	
	@Override
	public boolean checkPassword(User user) {
		// TODO Auto-generated method stub
		if( userDaoImpl.list(user).isEmpty())
			return false;
		else
			return true;
	}

	@Override
	public boolean logout() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void postTopic() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public User find(User user) {
		List<User> t =userDaoImpl.find(user.getName());
		if(t.isEmpty())
				return null;
		else
			return t.get(0);
			
	
	}
	@Override
	public User getUser(String name) {
		// TODO Auto-generated method stub
		return userDaoImpl.find(name).get(0);
	}
	@Override
	public boolean saveUserInfo(User user) {
		try{
			
		userDaoImpl.meger(user);
		userDaoImpl.flush();
		return true;
		}
		catch(Exception ex)
		{
			return false;
		}
	}
	@Override
	public boolean checkToken(String name,String token) {
		try{
			User u = userDaoImpl.find(name).get(0);
			u.setLasttime(new Date().getTime());
			userDaoImpl.meger(u);
			userDaoImpl.flush();
			if(u.getId().toString().equals(token))
				return true;
			else
				return false;
		}
		catch(Exception e)
		{
			return false;
		}
	
	}
	@Override
	public void addFriend(int[] ids, int uid) {
		User user = userDaoImpl.get(User.class, uid);
		Set<User> fset= user.getFriends();
		//Set<User> newfset = new HashSet<User>();
		 for (int fId : ids) {  
		        if (fId != uid) {  
		            User f = this.userDaoImpl.get(User.class, fId);  
		            if (!fset.contains(f))  
		            	fset.add(f);  
		        }  
		    } 
		 user.setFriends(fset);
		 userDaoImpl.merge(user);
	}
	@Override
	public void addFriend(int fid, int uid) {
		
		User user = userDaoImpl.get(User.class, uid);
		if(fid!=uid)
		{
			
			User fuser = userDaoImpl.get(User.class, fid);
			Set<User> fset = user.getFriends();
			if(!fset.contains(fuser))
			{
				
				fset.add(fuser);
			}
			
			user.setFriends(fset);
		}
		userDaoImpl.merge(user);
		userDaoImpl.flush();
		
	}
	@Override
	public Set<User> getFriend(User user) {
		return userDaoImpl.get(User.class, user.getId()).getFriends();
		 
	}
	@Override
	public Set<User> getFriend(int uid) {
		return userDaoImpl.get(User.class, uid).getFriends();
	}
	@Override
	public Set<User> getFriend(String name) {
		return userDaoImpl.find(name).get(0).getFriends();
		
	}
	@Override
	public User getUser(int id) {
		return userDaoImpl.get(User.class, id);
	}
	@Override
	public User getUser(User user) {
		return userDaoImpl.get(User.class, user.getId());
	}
	@Override
	public void update(User user) {
		userDaoImpl.update(user);
		
		
	}
	@Override
	public List<User> list() {
		return	this.userDaoImpl.list();
		 
	}
	@Override
	public User getUserLa(int uid) {
		return userDaoImpl.load(uid);
	}
	@Override
	public List<Map<String, String>> getInterestGroupUserListJson(int num,int offset,int igid) {
		Set<User> uset = interestGroupServiceImpl.getInterestGroup(igid).getMember();
		List<User> list = new ArrayList<User>(uset);
		return Recombine(list.subList(offset, offset+num));
	}

	private List<Map<String,String>> Recombine(List<User> set)
	{
		List<Map<String,String>> ulist = new ArrayList<Map<String,String>>();
		Map<String,String> umap = null;
		Iterator<User> it = set.iterator();
		while(it.hasNext())
		{
			User u = it.next();
			umap = new HashMap<String, String>();
			umap.put("id", String.valueOf(u.getId()));
			umap.put("name", u.getName());
			umap.put("email", u.getEmail());
			umap.put("phone", u.getPhone());
			umap.put("realname", u.getRealName());
			umap.put("nearname", u.getNearName());
			ulist.add(umap);
		}
		return ulist;
		
	}
	@Override
	public User findUser(String propertyName, String value) {
		return userDaoImpl.search(propertyName, value);
	}
	@Override
	public long getUserNum() {
		
		return userDaoImpl.count();
	}
	@Override
	public List<Map<String,String>> getFriendList(int offset,int num,int uid) {
		List<User> list =new ArrayList<User>(userDaoImpl.get(User.class, uid).getFriends());
		
		return Recombine(list.subList(offset, offset+num));
	}
	@Override
	public List<Map<String, String>> getFriendList(int uid) {
		List<User> list =new ArrayList<User>(userDaoImpl.get(User.class, uid).getFriends());
		
		return Recombine(list);
	}
	@Override
	public void addFriend(String username, String friendname) {
		User user = userDaoImpl.get(username);
		User fuser = userDaoImpl.get(friendname);
		if(user!=null && fuser!=null)
		{
		
			Set<User> fset = user.getFriends();
			Set<User> fset2 = fuser.getFriends();
			if(!fset.contains(fuser))
			{
			
				fset.add(fuser);
				fset2.add(user);
			}
			
			user.setFriends(fset);
			fuser.setFriends(fset2);
		}
		userDaoImpl.merge(user);
		userDaoImpl.meger(fuser);
		userDaoImpl.flush();
	}
	
	@Override
	public String checkFriend(String username, String friendname) {
		User user = userDaoImpl.get(username);
		
		User fuser = userDaoImpl.get(friendname);
		if(user!=null && fuser!=null)
		{
			Set<User> fset = user.getFriends();
			if(!fset.contains(fuser))
				return "no";
			else
				return "yes";
		}
		return "error";
		
	}
	@Override
	public String checkFriend(int uid, int fid) {
		User user = userDaoImpl.get(User.class,uid);
		
		User fuser = userDaoImpl.get(User.class,fid);
		if(user!=null && fuser!=null)
		{
			Set<User> fset = user.getFriends();
			if(!fset.contains(fuser))
				return "no";
			else
				return "yes";
		}
		return "error";
		
	}
	@Override
	public void deleteUser(User user) {
		this.userDaoImpl.delete(user);
		
	}
	@Override
	public void updateLastTime(User user) {
		user.setLasttime(new Date().getTime());
		userDaoImpl.meger(user);
		userDaoImpl.flush();
		
	}
	@Override
	public long getActiveUserNum(Long date) {
		// TODO Auto-generated method stub
		return userDaoImpl.activeCount(date);
	}
	@Override
	public List<User> searchUser(User user) {
		String whereHql="";

		if(user.getName()!=""&&!user.getName().equals(""))
			whereHql+=" name=\'"+user.getName()+"\'";
		if(user.getEmail()!=""&&!user.getEmail().equals(""))
		{
			if(whereHql=="")
				whereHql+=" email=\'"+user.getEmail()+"\'";
			else
				whereHql+=" and email=\'"+user.getEmail()+"\'";
		}
			
		if(user.getRealName()!=""&&!user.getRealName().equals(""))
		{
			if(whereHql=="")
				whereHql+=" realName=\'"+user.getRealName()+"\'";
			else
				whereHql+=" and realName=\'"+user.getRealName()+"\'";
		}
		if(whereHql!="")
			whereHql=" where"+whereHql;
		
		return userDaoImpl.list(whereHql);
	
			
	}
	@Override
	public boolean deleteUser(int[] uids) {
		String where ="(";
		for(int a : uids)
		{
			where += a+",";
			User user = userDaoImpl.get(User.class, a);
			user.getFriends().clear();
			user.getFriendRequestReceiver().clear();
			user.getFriendRequestSender().clear();
			user.getGset().clear();
			user.getInterestGroup().clear();
			userDaoImpl.meger(user);
			userDaoImpl.flush();
		}
		where=where.substring(0, where.length()-1)+")";
		if(friendRequestServiceImpl.deleteFriendRequest(uids))
		{
			int n =userDaoImpl.delete(where);
			if(n!=0)
			{
				return true;
			}
			else{
				return false;
			}
		}
		else{
			return false;
		}
		
	}
	@Override
	public void deleteFriend(String username, String friendname) {
		User user = userDaoImpl.get(username);
		User fuser = userDaoImpl.get(friendname);
		if(user!=null && fuser!=null)
		{
		
			Set<User> fset = user.getFriends();
			Set<User> fset2 = fuser.getFriends();
			if(fset.contains(fuser))
			{
			
				fset.remove(fuser);
				fset2.remove(user);
			}
			
			user.setFriends(fset);
			fuser.setFriends(fset2);
		}
		userDaoImpl.merge(user);
		userDaoImpl.meger(fuser);
		userDaoImpl.flush();
		
	}
	@Override
	public void deleteFriend(int fid, int uid) {
		User user = userDaoImpl.get(User.class,uid);
		User fuser = userDaoImpl.get(User.class,fid);
		if(user!=null && fuser!=null)
		{
		
			Set<User> fset = user.getFriends();
			Set<User> fset2 = fuser.getFriends();
			if(fset.contains(fuser))
			{
			
				fset.remove(fuser);
				fset2.remove(user);
			}
			
			user.setFriends(fset);
			fuser.setFriends(fset2);
		}
		userDaoImpl.merge(user);
		userDaoImpl.meger(fuser);
		userDaoImpl.flush();
		
	}




}
