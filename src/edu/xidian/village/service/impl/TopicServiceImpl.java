package edu.xidian.village.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import edu.xidian.village.dao.TopicDao;
import edu.xidian.village.dao.UserDao;
import edu.xidian.village.service.TopicService;
import edu.xidian.village.service.UserService;
import edu.xidian.village.util.ComparatorTopic;
import edu.xidian.village.vo.Topic;
import edu.xidian.village.vo.User;
@Component
public class TopicServiceImpl implements TopicService{

	private TopicDao topicDaoImpl;
	private UserDao userDaoImpl;
	private UserService userServiceImpl;
	
	public UserService getUserServiceImpl() {
		return userServiceImpl;
	}
	@Resource
	public void setUserServiceImpl(UserService userServiceImpl) {
		this.userServiceImpl = userServiceImpl;
	}
	public UserDao getUserDaoImpl() {
		return userDaoImpl;
	}
	@Resource
	public void setUserDaoImpl(UserDao userDaoImpl) {
		this.userDaoImpl = userDaoImpl;
	}
	public TopicDao getTopicDaoImpl() {
		return topicDaoImpl;
	}
@Resource
	public void setTopicDaoImpl(TopicDao topicDaoImpl) {
		this.topicDaoImpl = topicDaoImpl;
	}
	
	@Override
	public List<Topic> getTopicList() {
		
		return topicDaoImpl.list();
	}

	@Override
	public List<Topic> getTopicList(int n, int offset) {
		// TODO Auto-generated method stub
		return topicDaoImpl.list(n, offset);
	}

	@Override
	public boolean Praise(int tid,int uid) {
		String userid = String.valueOf(uid);
		Topic topic = topicDaoImpl.get(tid);
		String praise_u = topic.getPraise_u();
		int praise_num = topic.getPraise_num();
		if(praise_num==0)
		{
			praise_u=userid;
			praise_num=1;

		}
		else if(praise_num==1){
			if(praise_u.equals(userid))
			{
				return false;
			}
			else{
				praise_u += ","+userid;
				praise_num=2;
				
			}
		}
		else{
			List<String> praises =Arrays.asList(praise_u.split(","));
			
			if(praises.contains(userid))
				return false;
			else{
				praise_u += ","+userid;
				praise_num++;
				
			}
			
		}
		topic.setPraise_num(praise_num);
		topic.setPraise_u(praise_u);
		topicDaoImpl.merge(topic);
		topicDaoImpl.flush();
		return true;
		
	}
	@Override
	public int addTopic(String message, Long datetime, String picture, int uid) {
		Topic topic = new Topic();
		topic.setMessage(message);
		topic.setDatetime(datetime);
		topic.setLandlord(userDaoImpl.get(User.class, uid));
		topic.setPicture(picture);
		topic.setPraise_num(0);
		topic.setIsHot('N');
		return topicDaoImpl.add(topic);
		
	}
	@Override
	public Topic getTopic(int tid) {
		
		return topicDaoImpl.get(tid);
	}
	@Override
	public List<Map<String, String>> getTopicJson(int n, int offset) {
		List<Topic> list = topicDaoImpl.list(n, offset);
		return Recombine(list);
	}
	@Override
	public List<Map<String, String>> getTopicJson(int n, int offset, int uid) {
		Set<User> fset = userServiceImpl.getUser(uid).getFriends();
		List<Topic> list = new ArrayList<Topic>();
		Iterator<User> itr = fset.iterator();
		while(itr.hasNext())
		{
			User f = itr.next();
			list.addAll(f.getTopic());
		}
		ComparatorTopic comparetor = new ComparatorTopic();
		Collections.sort(list,comparetor);
		return Recombine(list.subList(offset,offset+n));
	}


	private List<Map<String, String>> Recombine(List<Topic> list)
	{
		Iterator<Topic> it = list.iterator();
		Map<String,String> topic=null;
		List<Map<String,String>> tlist = new ArrayList<Map<String,String>>();
		while(it.hasNext())
		{
			topic = new HashMap<String, String>();
			Topic t = it.next();
			topic.put("id",String.valueOf(t.getId()));
			topic.put("datetime", String.valueOf(t.getDatetime()));
			topic.put("message", t.getMessage());
			topic.put("picture", t.getPicture());
			topic.put("landlord.name", t.getLandlord().getName());
			topic.put("landlord.nearname", t.getLandlord().getNearName());
			topic.put("praise_num", String.valueOf(t.getPraise_num()));
			topic.put("reply_num", String.valueOf(t.getReply().size()));
			
			tlist.add(topic);
		}
		return tlist;
	}
	@Override
	public boolean deleteTopic(int[] tids) {
		String where ="(";
		for(int a : tids)
		{
			where += a+",";
		}
		where=where.substring(0, where.length()-1)+")";
		
		int n =topicDaoImpl.delete(where);
		if(n!=0)
		{
			return true;
		}
		else{
			return false;
		}
	
	}
	@Override
	public void setTopicHot(int tid) {
		Topic topic = topicDaoImpl.get(tid);
		topic.setIsHot('y');
		topicDaoImpl.merge(topic);
		topicDaoImpl.flush();
		
	}
	@Override
	public void setTopicNormal(int tid) {
		Topic topic = topicDaoImpl.get(tid);
		topic.setIsHot('n');
		topicDaoImpl.merge(topic);
		topicDaoImpl.flush();
		
	}
	@Override
	public List<Topic> searchTopic(String username,Character isHot) {
		String whereHql="";

		if(username!=""&&!username.equals(""))
		{
			int uid = userServiceImpl.getUser(username).getId();
			whereHql+=" user_landlord_id=\'"+uid+"\'";
		}
		if(isHot!=null)
		{
			if(whereHql=="")
				whereHql+=" isHot=\'"+isHot+"\'";
			else
				whereHql+=" and isHot=\'"+isHot+"\'";
		}
			
		
		if(whereHql!="")
			whereHql=" where"+whereHql;
		return topicDaoImpl.search(whereHql);
		
	}
	@Override
	public List<Map<String, String>> getHotTopic() {
		String where = "where isHot ='y'";
		
		return Recombine(topicDaoImpl.search(where));
	}

}
