package edu.xidian.village.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import edu.xidian.village.dao.WantedHouseDao;
import edu.xidian.village.service.UserService;
import edu.xidian.village.service.WantedHouseService;
import edu.xidian.village.util.ComparatorWantedHouse;
import edu.xidian.village.vo.WantedHouse;
@Component
public class WantedHouseServiceImpl implements WantedHouseService{

	private WantedHouseDao wantedHouseDaoImpl;
	private UserService userServiceImpl;
	private ComparatorWantedHouse comparatorWantedHouse;


	@Override
	public List<WantedHouse> getWantedHouseList() {
		// TODO Auto-generated method stub
		return wantedHouseDaoImpl.list();
	}

	@Override
	public List<Map<String, String>> getWantedHouseList(int num, int page) {
		// TODO Auto-generated method stub
		return Recombine(wantedHouseDaoImpl.list(num, num*(page-1)));
	}

	@Override
	public List<WantedHouse> getWantedHouseList(String username) {
		// TODO Auto-generated method stub
		return userServiceImpl.getUser(username).getWantedHouse();
	}

	@Override
	public void addWantedHouse(WantedHouse wantedHouse) {
		this.wantedHouseDaoImpl.add(wantedHouse);
		
	}

	public WantedHouseDao getWantedHouseDaoImpl() {
		return wantedHouseDaoImpl;
	}
	@Resource
	public void setWantedHouseDaoImpl(WantedHouseDao wantedHouseDaoImpl) {
		this.wantedHouseDaoImpl = wantedHouseDaoImpl;
	}

	public UserService getUserServiceImpl() {
		return userServiceImpl;
	}
	@Resource
	public void setUserServiceImpl(UserService userServiceImpl) {
		this.userServiceImpl = userServiceImpl;
	}

	@Override
	public  List<Map<String, String>> getWantedHouseList(String username, int num,
			int page) {
		List<WantedHouse> list = userServiceImpl.getUser(username).getWantedHouse();
		Collections.sort(list,comparatorWantedHouse);
		int n =num*page-list.size();
		if(num*(page-1)>list.size())
			return null;
		else if(n>0)
			return Recombine(list.subList((page-1)*num,  page*num-n));
		else
			return Recombine(list.subList((page-1)*num, page*num));
	}

	@Override
	public void addWantedHouse(Character type,Character area,Character sex,String telephone,String money,String description,int uid)
	{
		WantedHouse wantedHouse = new WantedHouse(type, area, sex, telephone, money, description);
		wantedHouse.setWantedUser(userServiceImpl.getUser(uid));
		wantedHouse.setDatetime(new Date().getTime());
		wantedHouse.setStatus('1');
		wantedHouseDaoImpl.add(wantedHouse);
	}

	private List<Map<String, String>> Recombine(List<WantedHouse> list)
	{
		Iterator<WantedHouse> it = list.iterator();
		Map<String,String> ob=null;
		List<Map<String,String>> tlist = new ArrayList<Map<String,String>>();
		while(it.hasNext())
		{
			ob = new HashMap<String, String>();
			WantedHouse t = it.next();
			ob.put("id",String.valueOf(t.getId()));
			ob.put("datetime", String.valueOf(t.getDatetime()));
			ob.put("description", t.getDescription());
			ob.put("money", t.getMoney());
			ob.put("sex", String.valueOf(t.getSex()));
			ob.put("telephone", t.getTelephone());
			ob.put("type",String.valueOf(t.getType()));
			ob.put("area", String.valueOf(t.getArea()));
			ob.put("wantedUser.name", t.getWantedUser().getName());
			ob.put("wantedUser.nearname", t.getWantedUser().getNearName());
			ob.put("status", String.valueOf(t.getStatus()));
	
			
			tlist.add(ob);
		}
		return tlist;
	}
	
	public ComparatorWantedHouse getComparatorWantedHouse() {
		return comparatorWantedHouse;
	}
	@Resource
	public void setComparatorWantedHouse(ComparatorWantedHouse comparatorWantedHouse) {
		this.comparatorWantedHouse = comparatorWantedHouse;
	}
	
}
