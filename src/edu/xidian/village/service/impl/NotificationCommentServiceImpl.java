package edu.xidian.village.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;



import edu.xidian.village.dao.NotificationCommentDao;
import edu.xidian.village.dao.NotificationDao;
import edu.xidian.village.dao.UserDao;
import edu.xidian.village.service.NotificationCommentService;
import edu.xidian.village.vo.Notification;
import edu.xidian.village.vo.NotificationComment;
import edu.xidian.village.vo.User;
@Transactional
@Component("notificationCommentServiceImpl")
public class NotificationCommentServiceImpl implements NotificationCommentService {

	private NotificationCommentDao notificationCommentDaoImpl;
	private NotificationDao notificationDaoImpl;
	private UserDao userDaoImpl;
	
	public UserDao getUserDaoImpl() {
		return userDaoImpl;
	}
	@Resource
	public void setUserDaoImpl(UserDao userDaoImpl) {
		this.userDaoImpl = userDaoImpl;
	}
	public NotificationCommentDao getNotificationCommentDaoImpl() {
		return notificationCommentDaoImpl;
	}
	@Resource
	public void setNotificationCommentDaoImpl(
			NotificationCommentDao notificationCommentDaoImpl) {
		this.notificationCommentDaoImpl = notificationCommentDaoImpl;
	}
	public NotificationDao getNotificationDaoImpl() {
		return notificationDaoImpl;
	}
	@Resource
	public void setNotificationDaoImpl(NotificationDao notificationDaoImpl) {
		this.notificationDaoImpl = notificationDaoImpl;
	}

	@Override
	public boolean addNotificationComment(NotificationComment nc, int noid,int uid) {
		try
		{
			Notification no = notificationDaoImpl.get(Notification.class, noid);
			User user = userDaoImpl.get(User.class, uid);
			nc.setNotification(no);
			nc.setUser(user);
			notificationCommentDaoImpl.add(nc);
			return true;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return false;
		}
		
	}
	@Override
	public List<NotificationComment> getNotificationCommentList(int nid)  {
		
		return notificationCommentDaoImpl.list(nid);
	}
	@Override
	public List<Map<String,String>> getNotificationCommentList(int n,
			int offset, int nid) {
		List<NotificationComment> list =notificationCommentDaoImpl.list(n, offset, nid);
		return Recombine(list);
	}
	private List<Map<String,String>> Recombine(List<NotificationComment> list)
	{
		Iterator<NotificationComment> it = list.iterator();
		Map<String,String> ncmap =null;
		NotificationComment nc = new NotificationComment();
		List<Map<String ,String>> nclist = new ArrayList<Map<String,String>>();
		while(it.hasNext())
		{
			nc=it.next();
			ncmap = new HashMap<String, String>();
			ncmap.put("id", String.valueOf(nc.getId()));
			ncmap.put("datetime", String.valueOf(nc.getDatetime()));
			ncmap.put("message", nc.getContent());
			ncmap.put("reply_name", nc.getUser().getName());
			ncmap.put("reply_nearname", nc.getUser().getNearName());
			nclist.add(ncmap);
		}
		return nclist;
	}
	@Override
	public boolean deleteNC(int[] ncids) {
		String where ="(";
		for(int a : ncids)
		{
			where += a+",";
		}
		where=where.substring(0, where.length()-1)+")";
		
		int n =notificationCommentDaoImpl.delete(where);
		if(n!=0)
		{
			return true;
		}
		else{
			return false;
		}
		
	}
}
