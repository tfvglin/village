package edu.xidian.village.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import edu.xidian.village.dao.InterestGroupDao;
import edu.xidian.village.dao.UserDao;
import edu.xidian.village.service.InterestGroupService;
import edu.xidian.village.service.UserService;
import edu.xidian.village.util.jpush.JIMUtil;
import edu.xidian.village.vo.InterestGroup;
import edu.xidian.village.vo.User;
@Component
public class InterestGroupServiceImpl implements InterestGroupService{

	private InterestGroupDao interestGroupDaoImpl;
	private UserDao userDaoImpl;
	private UserService userServiceImpl;
	private JIMUtil jIMUtil;
	
	
	
	
	@Override
	public void addInterestGroup(InterestGroup group) {
		interestGroupDaoImpl.add(group); 
		
	}
	
	//no work
	@Override 
	public void addInterestGroup(String name, String introduce, int creatuserid,
			int[] mids) {
		
		InterestGroup interestGroup = new InterestGroup();
		interestGroup.setIntroduce(introduce);
		interestGroup.setName(name);
		Set<User> set = new HashSet<User>();
		 for (int uid : mids) {
			 set.add(userDaoImpl.get(User.class, uid));
		 }
		 User creatUser = userDaoImpl.get(User.class, creatuserid);
		 set.add(creatUser);
		 interestGroup.setCreatUser(creatUser);
		 interestGroup.setMember(set);
		 interestGroupDaoImpl.add(interestGroup);
		 interestGroupDaoImpl.flush();
		// TODO Auto-generated method stub
		
	}
	public InterestGroupDao getInterestGroupDaoImpl() {
		return interestGroupDaoImpl;
	}
	@Resource
	public void setInterestGroupDaoImpl(InterestGroupDao interestGroupDaoImpl) {
		this.interestGroupDaoImpl = interestGroupDaoImpl;
	}
	public UserDao getUserDaoImpl() {
		return userDaoImpl;
	}
	@Resource
	public void setUserDaoImpl(UserDao userDaoImpl) {
		this.userDaoImpl = userDaoImpl;
	}
	@Override
	public void modifyBaseInfo(int igid,String name, String introduce,String message) {
		InterestGroup interestGroup = interestGroupDaoImpl.get(igid);
		interestGroup.setIntroduce(introduce);
		interestGroup.setName(name);
		interestGroup.setMessage(message);
		interestGroupDaoImpl.merge(interestGroup);
		interestGroupDaoImpl.flush();
		
		
	}
	@Override
	public void addmember(int igid, int[] uids) {
		InterestGroup interestGroup = interestGroupDaoImpl.get(igid);
	
		String gid = interestGroup.getGid();
		Set<User> oldset = interestGroup.getMember();
		Set<User> uset = new HashSet<User>();
		for( int uid : uids)
		{
			User user =userDaoImpl.get(User.class, uid);
			if(!oldset.contains(user))
			{
					oldset.add(user);
					uset.add(user);
			}
		}
		if(jIMUtil.interestGroupAddMember(uset, gid))
		{
			interestGroup.setMember(oldset);
			interestGroupDaoImpl.merge(interestGroup);
			interestGroupDaoImpl.flush();
		}
		
	}
	
	
	@Override
	public void deletemember(int igid, int[] uids) {
		InterestGroup interestGroup = interestGroupDaoImpl.get(igid);
		String gid = interestGroup.getGid();
		Set<User> oldset = interestGroup.getMember();
		Set<User> uset = new HashSet<User>();
		for( int uid : uids)
		{
			User user =userDaoImpl.get(User.class, uid);
			if(oldset.contains(user))
			{
					oldset.remove(user);
					uset.add(user);
			}
		}
		if(jIMUtil.interestGroupDeleteMember(uset, gid))
		{
			interestGroup.setMember(oldset);
			interestGroupDaoImpl.merge(interestGroup);
			interestGroupDaoImpl.flush();
		}
		
	}
	@Override
	public String addInterestGroup(String name, String introduce, int creatuserid,String message,String picture,Long datetime) {
		InterestGroup interestGroup = new InterestGroup();
		interestGroup.setIntroduce(introduce);
		interestGroup.setName(name);
		interestGroup.setDatetime(datetime);
		interestGroup.setPicture(picture);
		interestGroup.setMessage(message);
		 User creatUser = userDaoImpl.get(User.class, creatuserid);
		 Set<User> set = new HashSet<User>();
		 set.add(creatUser);
		 interestGroup.setMember(set);
		 interestGroup.setCreatUser(creatUser);
		 String re = jIMUtil.interestGroupAdd(interestGroup);
		 if(!re.equals("error"))
		 {
			 interestGroup.setGid(re);
			 interestGroupDaoImpl.add(interestGroup);
			 interestGroupDaoImpl.flush();
		 }
		 return re;
		 
		 
		
	}
	@Override
	public List<InterestGroup> getInterestGroupList() {
		// TODO Auto-generated method stub
		return interestGroupDaoImpl.list();
	}
	@Override
	public List<InterestGroup> getInterestGroupList(int uid) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public List<Map<String, String>> getInterestGroupJson(int uid) {
		Set<InterestGroup> set = userServiceImpl.getUser(uid).getGset();
		return Recombine(set);
	}

	public UserService getUserServiceImpl() {
		return userServiceImpl;
	}
	@Resource
	public void setUserServiceImpl(UserService userServiceImpl) {
		this.userServiceImpl = userServiceImpl;
	}
	
	private List<Map<String,String>> Recombine(Collection<InterestGroup> collection)
	{
		List<Map<String,String>> iglist = new ArrayList<Map<String,String>>();
		Map<String,String>	igmap = null;
		Iterator<InterestGroup> it = collection.iterator();
		while(it.hasNext())
		{
			InterestGroup ig = it.next();
			igmap = new HashMap<String, String>();
			igmap.put("id",String.valueOf(ig.getId()));
			igmap.put("introduce", ig.getIntroduce());
			igmap.put("name", ig.getName());
			igmap.put("master_name", ig.getCreatUser().getName());
			igmap.put("message", ig.getMessage());
			igmap.put("picture", ig.getPicture());
			igmap.put("gid", String.valueOf(ig.getGid()));
			
			iglist.add(igmap);
		}
		return iglist;
	}
	@Override
	public InterestGroup getInterestGroup(int igid) {
		// TODO Auto-generated method stub
		return interestGroupDaoImpl.get(igid);
	}
	@Override
	public List<Map<String, String>> getInterestGroupJson(int num, int offset) {
		List<InterestGroup> list = interestGroupDaoImpl.list(num, offset);
		return Recombine(list);
	}
	@Override
	public InterestGroup findInterestGroup(String propertyName, String value) {
		
		return interestGroupDaoImpl.find(propertyName, value);
	}
	public JIMUtil getjIMUtil() {
		return jIMUtil;
	}
	@Resource
	public void setjIMUtil(JIMUtil jIMUtil) {
		this.jIMUtil = jIMUtil;
	}
}
