package edu.xidian.village.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import edu.xidian.village.dao.NotificationDao;
import edu.xidian.village.service.NotificationService;
import edu.xidian.village.vo.Notification;

//@Transactional
@Service("notificationServiceImpl")
public class NotificationServiceImpl<T> implements NotificationService<T> {

	private NotificationDao<T> notificationDaoImpl;
	
	




	public NotificationDao<T> getNotificationDaoImpl() {
		return notificationDaoImpl;
	}
	@Resource
	public void setNotificationDaoImpl(NotificationDao<T> notificationDaoImpl) {
		this.notificationDaoImpl = notificationDaoImpl;
	}

	@Override
	public List<Notification> getNotificationList(int n,int offset) throws Exception {
		return notificationDaoImpl.get(n,offset);
		
	}

	
	@Override
	public boolean addNotification(T t) {
		
			if(notificationDaoImpl.add(t)!=null)
			{
				//throw new RuntimeException("aa");
				return true;
				
			}
			else
				return false;
		
	}
	@Override
	public Notification getNotification(int noid) {
		// TODO Auto-generated method stub
		return notificationDaoImpl.get(Notification.class, noid);
	}
	@Override
	public void modifyBase(String title, String message,String text, int noid) {
		System.out.println(text);
		Notification notification = notificationDaoImpl.get(Notification.class, noid);
		notification.setTitle(title);
		notification.setMessage(message);
		notification.setText(text);
		notificationDaoImpl.merge(notification);
		notificationDaoImpl.flush();
		
	}
	@Override
	public long getNotificationNum() {
		return	notificationDaoImpl.count();
		 
	}
	@Override
	public boolean deleteNotification(int[] nids) {
		String where ="(";
		for(int a : nids)
		{
			where += a+",";
		}
		where=where.substring(0, where.length()-1)+")";
		
		int n =notificationDaoImpl.delete(where);
		if(n!=0)
		{
			return true;
		}
		else{
			return false;
		}
		
	}
	@Override
	public List<Notification> searchNotification(String title) {
		String whereHql="";
		if(title!=""&&!title.equals(""))
			whereHql+=" title=\'"+title+"\'";
		if(whereHql!="")
			whereHql=" where"+whereHql;
	
		return notificationDaoImpl.get(whereHql);
	
	}
	@Override
	public List<Notification> getNotificationList() {
		// TODO Auto-generated method stub
		return notificationDaoImpl.get();
	}

}
