package edu.xidian.village.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import edu.xidian.village.dao.RentHouseDao;
import edu.xidian.village.service.RentHouseService;
import edu.xidian.village.service.UserService;
import edu.xidian.village.util.ComparatorRentHouse;
import edu.xidian.village.vo.RentHouse;
@Component
public class RentHouseServiceImpl implements RentHouseService{
	
	private RentHouseDao rentHouseDaoImpl;
	private UserService UserServiceImpl;
	private ComparatorRentHouse comparatorRentHouse;
	
	public RentHouseDao getRentHouseDaoImpl() {
		return rentHouseDaoImpl;
	}
	@Resource
	public void setRentHouseDaoImpl(RentHouseDao rentHouseDaoImpl) {
		this.rentHouseDaoImpl = rentHouseDaoImpl;
	}
	public UserService getUserServiceImpl() {
		return UserServiceImpl;
	}
	@Resource
	public void setUserServiceImpl(UserService userServiceImpl) {
		UserServiceImpl = userServiceImpl;
	}
	@Override
	public void addRentHouse(String address, String introduce, String picture,
			Character type, Character area, String telephone, String money,
			int uid) {
		RentHouse rentHouse =new RentHouse(address, introduce, picture, type, area, telephone, money);
		rentHouse.setOwnUser(UserServiceImpl.getUser(uid));
		rentHouse.setStatus('1');
		rentHouse.setDatetime(new Date().getTime());
		rentHouseDaoImpl.add(rentHouse);
		
	}
	@Override
	public List<RentHouse> getRentHouseList() {
		
		return this.rentHouseDaoImpl.list();
	}
	@Override
	public List<Map<String, String>> getRentHouseList(int num, int page) {
		return Recombine(this.rentHouseDaoImpl.list(num, num*(page-1)));
		
	}
	@Override
	public List<RentHouse> getRentHouseList(String username) {
		return this.UserServiceImpl.getUser(username).getRentHouse();
		
	}
	@Override
	public List<Map<String, String>> getRentHouseList(String username, int num, int page) {
		List<RentHouse> list = UserServiceImpl.getUser(username).getRentHouse();
		Collections.sort(list, comparatorRentHouse);
		
		int n =num*page-list.size();
		if(num*(page-1)>list.size())
			return null;
		else if(n>0)
			return Recombine(list.subList((page-1)*num,  page*num-n));
		else
			return Recombine(list.subList((page-1)*num, page*num));
		
	}
	
	
	private List<Map<String, String>> Recombine(List<RentHouse> list)
	{
		Iterator<RentHouse> it = list.iterator();
		Map<String,String> ob=null;
		List<Map<String,String>> tlist = new ArrayList<Map<String,String>>();
		while(it.hasNext())
		{
			ob = new HashMap<String, String>();
			RentHouse t = it.next();
			ob.put("id",String.valueOf(t.getId()));
			ob.put("datetime", String.valueOf(t.getDatetime()));
			ob.put("address",t.getAddress());
			ob.put("money", t.getMoney());
			ob.put("introduce", t.getIntroduce());
			ob.put("picture", t.getPicture());
			ob.put("telephone", t.getTelephone());
			ob.put("type",String.valueOf(t.getType()));
			ob.put("area", String.valueOf(t.getArea()));
			ob.put("ownUser.name", t.getOwnUser().getName());
			ob.put("ownUser.nearname", t.getOwnUser().getNearName());
			ob.put("status", String.valueOf(t.getStatus()));
			
			tlist.add(ob);
		}
		return tlist;
	}
	public ComparatorRentHouse getComparatorRentHouse() {
		return comparatorRentHouse;
	}
	@Resource
	public void setComparatorRentHouse(ComparatorRentHouse comparatorRentHouse) {
		this.comparatorRentHouse = comparatorRentHouse;
	}

}
