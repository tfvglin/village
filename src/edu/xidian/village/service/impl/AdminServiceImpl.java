package edu.xidian.village.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import edu.xidian.village.dao.AdminDao;
import edu.xidian.village.dao.NotificationCommentDao;
import edu.xidian.village.dao.NotificationDao;
import edu.xidian.village.dao.UserDao;
import edu.xidian.village.service.AdminService;
import edu.xidian.village.vo.Admin;
import edu.xidian.village.vo.Notification;
import edu.xidian.village.vo.NotificationComment;
import edu.xidian.village.vo.User;
@Component("adminServiceImpl")
public class AdminServiceImpl implements AdminService{

	private AdminDao adminDaoImpl;
	private UserDao userDaoImpl;
	private NotificationCommentDao notificationCommentDaoImpl;
	private NotificationDao<Notification> notificationDaoImpl;
	
	public NotificationCommentDao getNotificationCommentDaoImpl() {
		return notificationCommentDaoImpl;
	}
	@Resource
	public void setNotificationCommentDaoImpl(
			NotificationCommentDao notificationCommentDaoImpl) {
		this.notificationCommentDaoImpl = notificationCommentDaoImpl;
	}
	public NotificationDao<Notification> getNotificationDaoImpl() {
		return notificationDaoImpl;
	}
	@Resource
	public void setNotificationDaoImpl(NotificationDao<Notification> notificationDaoImpl) {
		this.notificationDaoImpl = notificationDaoImpl;
	}
	public UserDao getUserDaoImpl() {
		return userDaoImpl;
	}
	@Resource
	public void setUserDaoImpl(UserDao userDaoImpl) {
		this.userDaoImpl = userDaoImpl;
	}
	public AdminDao getAdminDaoImpl() {
		return adminDaoImpl;
	}
	@Resource
	public void setAdminDaoImpl(AdminDao adminDaoImpl) {
		this.adminDaoImpl = adminDaoImpl;
	}

	@Override
	public boolean login(Admin admin) {
		try {
			System.out.println(admin.getName());
			Admin ad =adminDaoImpl.getAdmin(admin.getName());
			if(ad.getPassword()==admin.getPassword()||ad.getPassword().equals(admin.getPassword()))
				return true;
			
		} catch (Exception e) {
			
			e.printStackTrace();
			return false;
		}
		return false;
	}

	@Override
	public boolean logout() {
		// TODO Auto-generated method stub
		return false;
	}

	
	
	@Override
	public List<User> getUserList() {
		// TODO Auto-generated method stub
		return userDaoImpl.list();
	}
	@Override
	public void postMessage() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void sentNotification() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public List<Notification> getNotificationList() throws Exception {
		return notificationDaoImpl.get();
		
	}
	@Override
	public List<User> getUserList(User user) {
		String whereHql="";

		if(user.getName()!=""&&!user.getName().equals(""))
			whereHql+=" name=\'"+user.getName()+"\'";
		if(user.getEmail()!=""&&!user.getEmail().equals(""))
		{
			if(whereHql=="")
				whereHql+=" email=\'"+user.getEmail()+"\'";
			else
				whereHql+=" and email=\'"+user.getEmail()+"\'";
		}
			
		if(user.getRealName()!=""&&!user.getRealName().equals(""))
		{
			if(whereHql=="")
				whereHql+=" realName=\'"+user.getRealName()+"\'";
			else
				whereHql+=" and realName=\'"+user.getRealName()+"\'";
		}
		if(whereHql!="")
			whereHql=" where"+whereHql;
		
		return userDaoImpl.list(whereHql);
	
	}
	@Override
	public List<Notification> getNotificationList(Notification notification)
			throws Exception {
		String whereHql="";
		if(notification.getTitle()!=""&&!notification.getTitle().equals(""))
			whereHql+=" title=\'"+notification.getTitle()+"\'";
		if(whereHql!="")
			whereHql=" where"+whereHql;
		return notificationDaoImpl.get(whereHql);
	}
	@Override
	public void deleteUser(User user) {
		
		userDaoImpl.delete(user);
	}
	@Override
	public void deleteUser(List<User> ulist) {
		userDaoImpl.delete(ulist);
		
	}
	@Override
	public void deleteNotificationComment(NotificationComment nc) {
		notificationCommentDaoImpl.delete(nc);
		
	}
	@Override
	public void deleteNotificationComment(List<NotificationComment> nclist) {
		notificationCommentDaoImpl.delete(nclist);
		
	}
	@Override
	public void deleteNotification(Notification no) {
		notificationDaoImpl.delet(no);
		
	}
	@Override
	public void deleteNotification(List<Notification> nolist) {
		notificationDaoImpl.delet(nolist);
		
	}
	@Override
	public Admin getAdmin(Admin admin) {
		return adminDaoImpl.get(admin.getName());
		
	}
	@Override
	public boolean checkPassword(Admin admin) {
		
		return false;
	}
	@Override
	public void updateUser(User user) {
		this.userDaoImpl.update(user);
		
	}
	@Override
	public User getUser(User user) {
		return userDaoImpl.get(User.class, user.getId());
		
	}
	@Override
	public Notification getNotification(Notification no) {
		return notificationDaoImpl.get(Notification.class, no.getId());
		
	}
	@Override
	public void updateNotification(Notification no) {
		notificationDaoImpl.update(no);
		
	}

}
