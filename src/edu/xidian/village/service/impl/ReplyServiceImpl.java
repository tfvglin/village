package edu.xidian.village.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import edu.xidian.village.dao.ReplyDao;
import edu.xidian.village.service.ReplyService;
import edu.xidian.village.service.TopicService;
import edu.xidian.village.service.UserService;
import edu.xidian.village.vo.Reply;
@Component
public class ReplyServiceImpl implements ReplyService{


	private ReplyDao replyDaoImpl;
	private UserService userServiceImpl;
	private TopicService topicServiceImpl;
	

	public ReplyDao getReplyDaoImpl() {
		return replyDaoImpl;
	}
@Resource
	public void setReplyDaoImpl(ReplyDao replyDaoImpl) {
		this.replyDaoImpl = replyDaoImpl;
	}

	

	public UserService getUserServiceImpl() {
		return userServiceImpl;
	}
	@Resource
	public void setUserServiceImpl(UserService userServiceImpl) {
		this.userServiceImpl = userServiceImpl;
	}
	public TopicService getTopicServiceImpl() {
		return topicServiceImpl;
	}
	@Resource
	public void setTopicServiceImpl(TopicService topicServiceImpl) {
		this.topicServiceImpl = topicServiceImpl;
	}
	
	
	@Override
	public List<Reply> getReplyList(int tid, int n, int offset) {
		return replyDaoImpl.list(n, offset, tid);
	}

	@Override
	public List<Reply> getReplyList(int tid) {
		// TODO Auto-generated method stub
		return topicServiceImpl.getTopic(tid).getReply();
	}
	@Override
	public void addReply(int tid, int uid, String message,Long datetime) {
		Reply reply = new Reply();
		reply.setMessage(message);
		reply.setDatetime(datetime);
		reply.setReplyer(userServiceImpl.getUser(uid));
		reply.setTopic(topicServiceImpl.getTopic(tid));
		replyDaoImpl.add(reply);
		replyDaoImpl.flush();
		
	}
	@Override
	public List<Map<String, String>> getReplyJson(int tid, int n, int offset) {
		List<Reply> list = replyDaoImpl.list(n, offset, tid);
		
		return Recombine(list);
	}
	private List<Map<String,String>> Recombine(List<Reply> list)
	{
		List<Map<String,String>> rlist = new ArrayList<Map<String,String>>();
		Reply r ;
		Iterator<Reply> it = list.iterator();
		Map<String,String> reply = null;
		while(it.hasNext())
		{
			r=it.next();
			reply = new HashMap<String, String>();
			reply.put("id", String.valueOf(r.getId()));
			reply.put("datetime", String.valueOf(r.getDatetime()));
			reply.put("message", r.getMessage());
			reply.put("reply_name", r.getReplyer().getName());
			reply.put("reply_nearname", r.getReplyer().getNearName());
			rlist.add(reply);
		}
		return rlist;
	}
	@Override
	public boolean deleteReply(int[] rids) {
		String where ="(";
		for(int a : rids)
		{
			where += a+",";
		}
		where=where.substring(0, where.length()-1)+")";
		
		int n =replyDaoImpl.delete(where);
		if(n!=0)
		{
			return true;
		}
		else{
			return false;
		}
	}

}
