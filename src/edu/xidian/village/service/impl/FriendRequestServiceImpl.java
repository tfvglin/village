package edu.xidian.village.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import edu.xidian.village.dao.FriendRequestDao;
import edu.xidian.village.service.FriendRequestService;
import edu.xidian.village.service.UserService;
import edu.xidian.village.util.ComparatorFriendRequest;
import edu.xidian.village.vo.FriendRequest;

@Component("friendRequestServiceImpl")
public class FriendRequestServiceImpl implements FriendRequestService {

	private FriendRequestDao friendRequestDaoImpl;
	private UserService userServiceImpl;
	public FriendRequestDao getFriendRequestDaoImpl() {
		return friendRequestDaoImpl;
	}
@Resource
	public void setFriendRequestDaoImpl(FriendRequestDao friendRequestDaoImpl) {
		this.friendRequestDaoImpl = friendRequestDaoImpl;
	}

	@Override
	public void sendRequest(FriendRequest friendRequest) {
		friendRequestDaoImpl.add(friendRequest);
		
	}
	@Override
	public void modifyState(FriendRequest friendRequest, String state) {
		friendRequest.setState(state);
		friendRequestDaoImpl.update(friendRequest);
		friendRequestDaoImpl.flush();
		
	}
	@Override
	public void modifyState(int frid, String state) {
		FriendRequest friendRequest = friendRequestDaoImpl.get(frid);
		friendRequest.setState(state);
		friendRequestDaoImpl.update(friendRequest);
		friendRequestDaoImpl.flush();
		
	}
	@Override
	public FriendRequest getFriendRequest(int frid) {
		
		return friendRequestDaoImpl.get(frid);
	}
	@Override
	public List<Map<String, String>> sendRequestJson(int uid) {
		List<FriendRequest> list = userServiceImpl.getUser(uid).getFriendRequestSender();
		ComparatorFriendRequest comparator = new ComparatorFriendRequest();
		Collections.sort(list,comparator);
		return Recombine(list);
	}
	@Override
	public List<Map<String, String>> receiveRequestJson(int uid) {
		List<FriendRequest> list = userServiceImpl.getUser(uid).getFriendRequestReceiver();
		ComparatorFriendRequest comparator = new ComparatorFriendRequest();
		Collections.sort(list,comparator);
		return Recombine(list);
	}
	public UserService getUserServiceImpl() {
		return userServiceImpl;
	}
	@Resource
	public void setUserServiceImpl(UserService userServiceImpl) {
		this.userServiceImpl = userServiceImpl;
	}

	private List<Map<String,String>> Recombine(List list)
	{
		Iterator<FriendRequest> it = list.iterator();
		Map<String,String> frmap = null;
		List<Map<String,String>> frlist = new ArrayList<Map<String,String>>();
		FriendRequest  fr = null;
		while(it.hasNext())
		{
			frmap = new HashMap<String, String>();
			fr=it.next();
			frmap.put("id",String.valueOf(fr.getId()));
			frmap.put("message", fr.getMessage());
			frmap.put("state", fr.getState());
			frmap.put("datetime",String.valueOf(fr.getDatetime()));
			frmap.put("sender_name", fr.getUserSender().getName());
			frmap.put("recevie_name", fr.getUserReceiver().getName());
			frlist.add(frmap);
		}
		return frlist;
	}
	@Override
	public boolean deleteFriendRequest(int[] uids) {
		String where ="(";
		for(int a : uids)
		{
			where += a+",";
		}
		where=where.substring(0, where.length()-1)+")";
		
		int n =friendRequestDaoImpl.delete(where);
		if(n!=0)
		{
			return true;
		}
		else{
			return false;
		}
		
	}
}
