package edu.xidian.village.service;

import java.util.List;
import java.util.Map;

import edu.xidian.village.vo.WantedHouse;

public interface WantedHouseService {

	public void addWantedHouse(Character type,Character area,Character sex,String telephone,String money,String description,int uid);
	
	public void addWantedHouse(WantedHouse wantedHouse);
	
	public List<WantedHouse> getWantedHouseList();
	
	public List<Map<String, String>> getWantedHouseList(int num,int page);
	
	public  List<Map<String, String>> getWantedHouseList(String username,int num,int page);

	public List<WantedHouse> getWantedHouseList(String username);
}
