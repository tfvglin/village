package edu.xidian.village.service;

import java.util.List;
import java.util.Map;

import edu.xidian.village.vo.InterestGroup;

public interface InterestGroupService {

	
	void addInterestGroup(InterestGroup group);
	
	void addInterestGroup(String name,String introduce,int creatuserid,int[] mids);
	String addInterestGroup(String name,String introduce,int creatuserid,String message,String picture,Long datetime);
	
	void modifyBaseInfo(int igid,String name,String introduce,String message);
	
	void addmember(int igid,int[] uids);
	
	void deletemember(int igid,int[] uids);
	
	List<InterestGroup> getInterestGroupList();

	List<InterestGroup> getInterestGroupList(int uid);
	
	List<Map<String,String>> getInterestGroupJson(int uid);
	
	List<Map<String,String>> getInterestGroupJson(int num,int offset);
	InterestGroup getInterestGroup(int igid);
	InterestGroup findInterestGroup(String propertyName,String value);
	
}
