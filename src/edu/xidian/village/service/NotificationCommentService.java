package edu.xidian.village.service;

import java.util.List;
import java.util.Map;

import edu.xidian.village.vo.NotificationComment;

public interface NotificationCommentService {

	public boolean addNotificationComment(NotificationComment nc,int noid,int uid);
	
	public List<NotificationComment> getNotificationCommentList(int nid);

	List<Map<String,String>> getNotificationCommentList(int n,int offset,int nid);
	
	public boolean deleteNC(int[] ncids);
	
}
