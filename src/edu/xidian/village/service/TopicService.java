package edu.xidian.village.service;

import java.util.List;
import java.util.Map;

import edu.xidian.village.vo.Topic;

public interface TopicService {

	Topic getTopic(int tid);
	
	List<Topic> getTopicList();
	List<Topic> getTopicList(int n , int offset);
	
	List<Map<String,String>> getTopicJson(int n,int offset);
	List<Map<String,String>> getTopicJson(int n,int offset,int uid);
	List<Map<String,String>> getHotTopic();
	
	boolean Praise(int tid,int uid);
	
	int addTopic(String message,Long datetime,String picture,int uid);
	
	boolean deleteTopic(int[] tids);
	
	void setTopicHot(int tid);
	void setTopicNormal(int tid);
	
	List<Topic> searchTopic(String username,Character isHot);
	
	
}
