package edu.xidian.village.util;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import org.springframework.stereotype.Component;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

@Component
public class ImgStream {

	private static  BASE64Encoder encoder = new sun.misc.BASE64Encoder();      
    private static BASE64Decoder decoder = new sun.misc.BASE64Decoder();  
	
 public static boolean base64StringToImage(String base64String,String name,String path){   
		 
		 
		 
	        try {      
	            byte[] bytes1 = decoder.decodeBuffer(base64String);      
	                
	            ByteArrayInputStream bais = new ByteArrayInputStream(bytes1);      
	            BufferedImage bi1 =ImageIO.read(bais);   
	            
	            File w2 = new File(path+name+".jpg");  
	            
	            return  ImageIO.write(bi1, "jpg", w2); 
	              
	           
	        } catch(IOException   ie) {      
	        	
	            return false;
	        }  
	        catch(IllegalArgumentException e)
	        {
	        	return false;
	        }
	    }      
}
