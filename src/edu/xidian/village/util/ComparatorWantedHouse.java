package edu.xidian.village.util;

import java.util.Comparator;

import org.springframework.stereotype.Component;

import edu.xidian.village.vo.WantedHouse;
@Component
public class ComparatorWantedHouse implements Comparator<WantedHouse>{



	@Override
	public int compare(WantedHouse o1, WantedHouse o2) {
		// TODO Auto-generated method stub
		return o2.getDatetime().compareTo(o1.getDatetime());
	}
}
