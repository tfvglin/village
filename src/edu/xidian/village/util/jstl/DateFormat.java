package edu.xidian.village.util.jstl;

import java.io.IOException;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.jsp.tagext.SimpleTagSupport;



public class DateFormat extends SimpleTagSupport{

	private String value;
	private String parttern;
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getParttern() {
		return parttern;
	}
	public void setParttern(String parttern) {
		this.parttern = parttern;
	}
	
	
	
	@Override
	public void doTag()
	{
	/*	try {
			Writer out = getJspContext().getOut();
			out.write(value);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		 String returnValue=null;
		if(value!=null && !value.equals(""))
		{
			long valueL = Long.valueOf(value);
			Date datetime = new Date(valueL);
			SimpleDateFormat df = new SimpleDateFormat(parttern);
            returnValue = df.format(datetime);
           
		}
		Writer out = getJspContext().getOut();
		try {
			
			out.write(returnValue);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
