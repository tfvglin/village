package edu.xidian.village.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import org.springframework.stereotype.Component;

@Component
public class HttpConnectUtil {
	
	String responseMessage = "";  
    StringBuffer response = new StringBuffer();  
    HttpsURLConnection httpConnection = null;  
   
    OutputStreamWriter outputStreamWriter = null;
    BufferedReader reader = null;  
	
	public String Post(String url,String param)
	{
	   
        String responseMessage = "";  
        StringBuffer response = new StringBuffer();  
        HttpsURLConnection httpConnection = null;  
       
        OutputStreamWriter outputStreamWriter = null;
        BufferedReader reader = null;  
        try {  
            URL urlPost = new URL(url);  
            httpConnection = (HttpsURLConnection) urlPost.openConnection();
   
            httpConnection.setHostnameVerifier(new CustomizedHostnameVerifier());
          
            
            httpConnection.setDoOutput(true);  
            httpConnection.setDoInput(true);  
          
            httpConnection.setRequestMethod("POST");  
          
            httpConnection.setUseCaches(false);  
         
            httpConnection.setInstanceFollowRedirects(true);
            httpConnection.setRequestProperty("Content-Type", "application/json");
            httpConnection.setRequestProperty("Charset", "UTF-8");
            httpConnection.setRequestProperty("Authorization", "Basic OTQzNjVkYjhkZWRlNGYwYTY2YTJlNmQ4Ojc4Y2Q1MDM4NGFiNjRkNmNkZDZlMTMxMg==");
           
            httpConnection.connect();  
       
            outputStreamWriter = new OutputStreamWriter(httpConnection.getOutputStream());
         //   System.out.println(param);
            outputStreamWriter.write(param);
             outputStreamWriter.flush();
            
            reader = new BufferedReader(new InputStreamReader(  
                    httpConnection.getInputStream(),"utf-8"));  
            while ((responseMessage = reader.readLine()) != null) {  
                response.append(responseMessage);  
            }  
           // System.out.println(response.toString());
            
            return response.toString();
        }
        catch(Exception e1){
        	e1.printStackTrace();
        	return "error";
        }
        finally {  
       
            try {  
                if (null != outputStreamWriter) {  
                	outputStreamWriter.close();  
                }  
                if (null != reader) {  
                    reader.close();  
                }  
                if (null != httpConnection) {  
                    httpConnection.disconnect();  
                }  
            } catch (Exception e2) {  
               return "error";
            }  
        }
		
     }  
}
	
	
   

