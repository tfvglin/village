package edu.xidian.village.util;

import java.util.Comparator;

import edu.xidian.village.vo.FriendRequest;

public class ComparatorFriendRequest implements Comparator<FriendRequest>{

	@Override
	public int compare(FriendRequest o1, FriendRequest o2) {
		// TODO Auto-generated method stub
		return o2.getDatetime().compareTo(o1.getDatetime());
	}

	

}
