package edu.xidian.village.util.jpush;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.springframework.stereotype.Component;

import edu.xidian.village.util.HttpConnectUtil;
import edu.xidian.village.vo.InterestGroup;
import edu.xidian.village.vo.User;
@Component
public class JIMUtil {
	private static final String IMURL = "https://api.im.jpush.cn/";
	private HttpConnectUtil httpConnectUtil;

	public HttpConnectUtil getHttpConnectUtil() {
		return httpConnectUtil;
	}
	@Resource
	public void setHttpConnectUtil(HttpConnectUtil httpConnectUtil) {
		this.httpConnectUtil = httpConnectUtil;
	}
	
	public String userRegist(User user)
	{
		String param ="["+ this.userToJSON(user)+"]";
		String	url = IMURL+"v1/users/";
		String res =httpConnectUtil.Post(url, param);
		if(res.equals("error"))
			return res;
		else
			return "success";
	}
	
	public String interestGroupAdd(InterestGroup interestGroup)
	{
		String param =this.interestGroupToJSON(interestGroup);
		String url = IMURL+"v1/groups/";
		String res = httpConnectUtil.Post(url, param);

		if(res.equals("error"))
			return "error";
		else
		{
			JSONObject json = JSONObject.fromObject(res);
			return String.valueOf(json.get("gid"));
		}
			
	}
	
	public boolean interestGroupAddMember(Set<User> set,String gid)
	{
		String param ="{\"add\":"+this.interestGroupMemberToJSON(set)+"}";
		System.out.println(param);
		String url = IMURL+"v1/groups/"+gid+"/members";
		String res = httpConnectUtil.Post(url, param);
		System.out.println(res);
		if(res.equals("error"))
			return false;
		else
		return true;
	}
	public boolean interestGroupDeleteMember(Set<User> set,String gid)
	{
		String param ="{\"remove\":"+this.interestGroupMemberToJSON(set)+"}";
		String url = IMURL+"v1/groups/"+gid+"/members";
		String res = httpConnectUtil.Post(url, param);
		
		if(res.equals("error"))
			return false;
		else
			return true;
	}
	
	private String interestGroupMemberToJSON(Set<User> set)
	{
		Iterator<User> it = set.iterator();
		Set<String> uset = new HashSet<String>();
		while(it.hasNext())
		{
			String name = it.next().getName();
			uset.add(name);
		}
		
		JSONArray jsonObject = JSONArray.fromObject(uset);
		return jsonObject.toString();
	}
	private String interestGroupToJSON(InterestGroup interestGroup)
	{
		 Map<String,String> map = new HashMap<String, String>();
	     map.put("owner_username", interestGroup.getCreatUser().getName());
	     map.put("name",interestGroup.getName());
	     map.put("desc",interestGroup.getIntroduce());
	       
	     JSONObject jsonObject = JSONObject.fromObject( map );    
	        
	     return jsonObject.toString();
	}
	
	 private String userToJSON(User user){
	     Map<String,String> map = new HashMap<String, String>();
	     map.put("username", user.getName());
	     map.put("password",user.getPassword());
	       
	     JSONObject jsonObject = JSONObject.fromObject( map );    
	        
	      return jsonObject.toString();
	    }
}
