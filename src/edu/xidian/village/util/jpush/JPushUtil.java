package edu.xidian.village.util.jpush;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import sun.misc.BASE64Encoder;

import cn.jpush.api.JPushClient;
import cn.jpush.api.common.resp.APIConnectionException;
import cn.jpush.api.common.resp.APIRequestException;
import cn.jpush.api.push.PushResult;
import cn.jpush.api.push.model.Platform;
import cn.jpush.api.push.model.PushPayload;
import cn.jpush.api.push.model.audience.Audience;
import cn.jpush.api.push.model.notification.Notification;

import edu.xidian.village.action.MessageAction;
import edu.xidian.village.vo.Message;
@Component
public class JPushUtil {

	 protected static final Logger LOG = LoggerFactory.getLogger(MessageAction.class);

	    // demo App defined in resources/jpush-api.conf 
		private static final String appKey ="94365db8dede4f0a66a2e6d8";
		private static final String masterSecret = "78cd50384ab64d6cdd6e1312";
		
		public static  String TITLE = "";
	    public static  String ALERT = "";
	    public static  String MSG_CONTENT = "";
	    public static final String REGISTRATION_ID = "0900e8d85ef";
	    public static final String TAG = "tag_api";

		public  String sendPush(Message message) {
		    // HttpProxy proxy = new HttpProxy("localhost", 3128);
		    // Can use this https proxy: https://github.com/Exa-Networks/exaproxy
	        JPushClient jpushClient = new JPushClient(masterSecret, appKey, 10000000);
	        ALERT = message.getContent();
	        TITLE = message.getTitle();
	        MSG_CONTENT = message.getContent();
	        // For push, all you need do is to build PushPayload object.
	        PushPayload payload = buildPushObject_android_all_WithTitle();
	        
	        try {
	            PushResult result = jpushClient.sendPush(payload);
	            LOG.info("Got result - " + result);
	            return "success";
	            
	        } catch (APIConnectionException e) {
	            LOG.error("Connection error. Should retry later. ", e);
	            return "error";
	        } catch (APIRequestException e) {
	            LOG.error("Error response from JPush server. Should review and fix it. ", e);
	            LOG.info("HTTP Status: " + e.getStatus());
	            LOG.info("Error Code: " + e.getErrorCode());
	            LOG.info("Error Message: " + e.getErrorMessage());
	            LOG.info("Msg ID: " + e.getMsgId());
	            return "error";
	        }
		}
		
		public static PushPayload buildPushObject_all_all_alert() {
		    return PushPayload.alertAll(ALERT);
		}
		
	    public static PushPayload buildPushObject_all_alias_alert() {
	        return PushPayload.newBuilder()
	                .setPlatform(Platform.all())
	                .setAudience(Audience.alias("alias1"))
	                .setNotification(Notification.alert(ALERT))
	                .build();
	    }
	    
	    public static PushPayload buildPushObject_android_all_WithTitle() {
	        return PushPayload.newBuilder()
	                .setPlatform(Platform.android())
	                .setAudience(Audience.all())
	                .setNotification(Notification.android(ALERT, TITLE, null))
	                //.setNotification(Notification.alert("aaaaaaaaaaaaa"))
	                .build();
	    }
	    
	    public String userRegist() throws Exception
	    {
	    	String  url =  "https://api.jpush.cn/v1/users";  
			
	        String responseMessage = "";  
	        StringBuffer resposne = new StringBuffer();  
	        HttpURLConnection httpConnection = null;  
	       
	        OutputStreamWriter outputStreamWriter = null;
	        BufferedReader reader = null;  
	        String base64Str =new BASE64Encoder().encode("94365db8dede4f0a66a2e6d8:78cd50384ab64d6cdd6e1312".getBytes());
	        try {  
	            URL urlPost = new URL(url);  
	            httpConnection = (HttpURLConnection) urlPost.openConnection();  
	            httpConnection.setDoOutput(true);  
	            httpConnection.setDoInput(true);  
	          
	            httpConnection.setRequestMethod("POST");  
	          
	            httpConnection.setUseCaches(false);  
	         
	            httpConnection.setInstanceFollowRedirects(true);  
	         
	            httpConnection.setRequestProperty("Content-Type", "application/json");
	            httpConnection.setRequestProperty("Charset", "UTF-8");
	            httpConnection.setRequestProperty("Authorization", "Basic OTQzNjVkYjhkZWRlNGYwYTY2YTJlNmQ4Ojc4Y2Q1MDM4NGFiNjRkNmNkZDZlMTMxMg==");
	           
	      
	            httpConnection.connect();  
	            outputStreamWriter = new OutputStreamWriter(httpConnection.getOutputStream());
	           String param = "{\"platform\":\"all\",\"audience\":\"all\",\"notification\":{\"alert\":\"Hi,JPush!\",\"title\":\"aaa\"}}";
	           String users="[{\"username\":\"dev_fang\", \"password\": \"password\"},"+ 
	                         "{\"username\": \"dev_fang\", \"password\": \"password\"}]";
	          
	           outputStreamWriter.write(users);
	            outputStreamWriter.flush();
	         
	            reader = new BufferedReader(new InputStreamReader(  
	                    httpConnection.getInputStream(),"utf-8"));  
	            while ((responseMessage = reader.readLine()) != null) {  
	                resposne.append(responseMessage);  
	            }  
	  
	          
	          
	            System.out.println(resposne.toString());
	            return resposne.toString();
	        }catch(Exception e1){
	        	e1.printStackTrace();
	        	throw e1;
	        }
	        finally {  
	       
	            try {  
	                if (null != outputStreamWriter) {  
	                	outputStreamWriter.close();  
	                }  
	                if (null != reader) {  
	                    reader.close();  
	                }  
	                if (null != httpConnection) {  
	                    httpConnection.disconnect();  
	                }  
	            } catch (Exception e2) {  
	                throw e2;
	            }  
	        }  
	    }
	    
	    
}
