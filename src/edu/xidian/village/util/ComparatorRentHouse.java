package edu.xidian.village.util;

import java.util.Comparator;

import org.springframework.stereotype.Component;

import edu.xidian.village.vo.RentHouse;
@Component
public class ComparatorRentHouse implements Comparator<RentHouse>{



	@Override
	public int compare(RentHouse o1, RentHouse o2) {
		// TODO Auto-generated method stub
		return o2.getDatetime().compareTo(o1.getDatetime());
	}
}
