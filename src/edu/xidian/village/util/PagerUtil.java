package edu.xidian.village.util;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;
@Component("pagerUtil")
public class PagerUtil {
	private List bigList;                      
	private int curentPageIndex = 1 ;          
	private int countPerpage = 10 ;            
	private List smallList;                     
	private int pageCount;						
	private int recordCount;					
	private int prePageIndex;				
	private int nextPageIndex;					
	private boolean firstPage;					
	private boolean lastPage;					

	
	public void setCurentPageIndex(int curentPageIndex)
	{
		this.curentPageIndex = curentPageIndex;
		
		prePageIndex = curentPageIndex - 1 ;
		nextPageIndex = curentPageIndex +1 ;
		if(curentPageIndex == 1)
		{
			firstPage = true ;
		}
		else
		{
			firstPage = false;
		}
		if(curentPageIndex == pageCount)
		{
			lastPage = true ;
		}
		else
		{
			lastPage = false;
		}
		
		smallList = new ArrayList();
		 for(int i=(curentPageIndex-1)*countPerpage; i<curentPageIndex*countPerpage&&i<recordCount; i++)
		{
			smallList.add(bigList.get(i));
		}
	}
	public List getBigList() {
		return bigList;
	}
	public void setBigList(List bigList) {
		this.bigList = bigList;
		recordCount = bigList.size();
		if(recordCount%countPerpage==0)
		{
			pageCount = recordCount / countPerpage;
		}
		else
		{
			pageCount = recordCount / countPerpage + 1;
		}
	}
	public int getCurentPageIndex() {
		return curentPageIndex;
	}

	public int getCountperpage() {
		return countPerpage;
	}
	public void setCountperpage(int countperpage) {
		this.countPerpage = countperpage;
	}
	public List getSmallList() {
		return smallList;
	}
	public void setSmallList(List smallList) {
		this.smallList = smallList;
	}
	public int getPageCount() {
		return pageCount;
	}
	public void setPageCount(int pageCount) {
		this.pageCount = pageCount;
	}
	public int getRecordCount() {
		return recordCount;
	}
	public void setRecordCount(int recordCount) {
		this.recordCount = recordCount;
	}
	public int getPrePageIndex() {
		return prePageIndex;
	}
	public void setPrePageIndex(int prePageIndex) {
		this.prePageIndex = prePageIndex;
	}
	public int getNextPageIndex() {
		return nextPageIndex;
	}
	public void setNextPageIndex(int nextPageIndex) {
		this.nextPageIndex = nextPageIndex;
	}
	public boolean isFirstPage() {
		return firstPage;
	}
	public void setFirstPage(boolean firstPage) {
		this.firstPage = firstPage;
	}
	public boolean isLastPage() {
		return lastPage;
	}
	public void setLastPage(boolean lastPage) {
		this.lastPage = lastPage;
	}
	
	public void Pager(List list,HttpServletRequest request) {
		PagerUtil pagerUtil = new PagerUtil();
		if (request.getSession(false).getAttribute("pager") == null) {
			request.getSession(false).setAttribute("pager", pagerUtil);
		}
		pagerUtil = (PagerUtil) request.getSession().getAttribute("pager");
		pagerUtil.setBigList(list);
		if (request.getParameter("PageIndex") == null) {
			pagerUtil.setCurentPageIndex(1);

		} else {
			pagerUtil.setCurentPageIndex(Integer.parseInt(request
					.getParameter("PageIndex")));
		}
		request.getSession(false).setAttribute("pager", pagerUtil);
	}
	
	

}
