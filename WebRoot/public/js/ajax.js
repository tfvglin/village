function userList() {
	
	$.ajax({
		type : "POST",
		cache : false,
		url : url_userList,
		data : {
		},
		dataType : "text", //预期服务器返回的数据类型
		async : true, //true 请求为同步执行  false 请求为异步执行
		beforeSend: loading, 
		success : function(data) {
			loadingsuccess();
			$('#content').html(data);
		}
	});
}
function userDelete(tid)
{

	var title = $("input[name='ulist']");
	var ulist = new Array();
	
	title.each(function(){
		if(this.checked)
			ulist.push($(this).val());
	});
	$.ajax({
		type : "POST",
		cache : false,
		url : url_userDelete,
		data : {
			'ulist[]':ulist,
		},
		dataType : "text", //预期服务器返回的数据类型
		async : true, //true 请求为同步执行  false 请求为异步执行
		
		success : function(data) {
			if(data=="success")
			{
			$('#successModal').modal('show');
			userList();
			}
			else{
				$('#failModal').modal('show');
			}
		}
		
	});
}
function notificationList() {
	
	$.ajax({
		type : "POST",
		cache : false,
		url : url_notificationList,
		data : {
		},
		dataType : "text", //预期服务器返回的数据类型
		async : true, //true 请求为同步执行  false 请求为异步执行
		beforeSend: loading, 
		success : function(data) {
			loadingsuccess();
			$('#content').html(data);
		}
	});
}
function notificationIssuePage() {
	
	$.ajax({
		type : "POST",
		cache : false,
		url : url_notificationIssuePage,
		data : {
		},
		dataType : "text", //预期服务器返回的数据类型
		async : true, //true 请求为同步执行  false 请求为异步执行
		//beforeSend: loading, 
		success : function(data) {
			//loadingsuccess();
			$('#content').html(data);
		}
	});
}
function notificationImgSave() {
	
	$.ajax({
		type : "POST",
		cache : false,
		url : url_notificationImgSave,
		data : {
			"x":x,
			"y":y,
			"width":width,
			"height":height,
			"title":timestamp
		},
		dataType : "text", //预期服务器返回的数据类型
		async : true, //true 请求为同步执行  false 请求为异步执行
		beforeSend: loading, 
		success : function(data) {
			loadingsuccess();
			if(data=="success")
				{
				$("#label").html("上传成功！");
				 $("input[name='notification.picture']").val(timestamp);
				
				}
			else{
				$("#label").html("上传失败！");
			}
		}
	});
}
function userSearch()
{
	var username = $('#userSearch input[name="user.name"]').val();
	var realname = $('#userSearch input[name="user.realName"]').val();
	var email = $('#userSearch input[name="user.email"]').val();
	if(username=="" && realname=="" && email==""){alert("请填写至少一个查询条件！");return false;}

	$.ajax({
		type : "POST",
		cache : false,
		url : url_userSearch,
		data : {
			'user.name':username,
			'user.realName':realname,
			'user.email':email
		},
		dataType : "text", //预期服务器返回的数据类型
		async : true, //true 请求为同步执行  false 请求为异步执行
		beforeSend: loading, 
		success : function(data) {
			loadingsuccess();
			$('#content').html(data);
		}
	});
}
function notificationSearch()
{
	var title = $('#notificationSearch input[name="title"]').val();
	
	if(title==""){alert("请填写标题！");return false;}
	
	$.ajax({
		type : "POST",
		cache : false,
		url : url_notificationSearch,
		data : {
			'title':title
			
		},
		dataType : "text", //预期服务器返回的数据类型
		async : true, //true 请求为同步执行  false 请求为异步执行
		beforeSend: loading, 
		success : function(data) {
			loadingsuccess();
			$('#content').html(data);
		}
	});
}
function notificationPushPage() {
	
	$.ajax({
		type : "POST",
		cache : false,
		url : url_notificationPushPage,
		data : {
		},
		dataType : "text", //预期服务器返回的数据类型
		async : true, //true 请求为同步执行  false 请求为异步执行
		success : function(data) {
			
			$('#content').html(data);
		}
	});
}
function notificationModifyPage(noid) {
	
	$.ajax({
		type : "POST",
		cache : false,
		url : url_notificationModifyPage,
		data : {
			'noid':noid,
		},
		dataType : "text", //预期服务器返回的数据类型
		async : true, //true 请求为同步执行  false 请求为异步执行
		success : function(data) {
			
			$('#content').html(data);
		}
	});
}
function notificationIssue()
{
	var message=ue.getContent();
	var text=ue.getContentTxt();
	
	var title = $("input[name='notification.title']").val();
	var picture = $("input[name='notification.picture']").val();
	$.ajax({
		type : "POST",
		cache : false,
		url : url_notificationIssue,
		data : {
			'notification.message':message,
			'notification.title':title,
			'notification.picture':picture,
			'notification.text':text,
		},
		dataType : "text", //预期服务器返回的数据类型
		async : true, //true 请求为同步执行  false 请求为异步执行
		beforeSend: loading, 
		success : function(data) {
			loadingsuccess();
			if(data=="success")
				$('#successModal').modal('show');
			else{
				$('#failModal').modal('show');
			}
		}
	}); 
}
function notificationModify()
{
	var message=ue.getContent();
	var text = ue.getContentTxt();
	var title = $("input[name='notification.title']").val();
	var noid = $("input[name='notification.id']").val();
 	$.ajax({
		type : "POST",
		cache : false,
		url : url_notificationModify,
		data : {
			'message':message,
			'title':title,
			'text':text,
			'noid':noid,
		},
		dataType : "text", //预期服务器返回的数据类型
		async : true, //true 请求为同步执行  false 请求为异步执行
		beforeSend: loading, 
		success : function(data) {
			loadingsuccess();
			if(data=="success")
				$('#successModal').modal('show');
			else{
				$('#failModal').modal('show');
			}
		}
	}); 
}
function notificationDelete()
{
	
	var title = $("input[name='nolist']");
	var nolist = new Array();

	title.each(function(){
		if(this.checked)
			nolist.push($(this).val());
	});
	if(nolist.length==0)
	{
		alert("没有选中任何内容！");
		return false;
	}
	//console.log(nolist);
	$.ajax({
		type : "POST",
		cache : false,
		url : url_notificationDelete,
		data : {
			"nolist[]":nolist,
		},
		dataType : "text", //预期服务器返回的数据类型
		async : true, //true 请求为同步执行  false 请求为异步执行
		beforeSend: loading, 
		success : function(data) {
			loadingsuccess();
			if(data=="success")
				{
				$('#successModal').modal('show');
				$.ajax({
					type : "POST",
					cache : false,
					url : url_notificationList,
					data : {
					},
					dataType : "text", //预期服务器返回的数据类型
					async : true, //true 请求为同步执行  false 请求为异步执行
					beforeSend: loading, 
					success : function(data) {
						loadingsuccess();
						$('#content').html(data);
					}
				});
				}
			else{
				$('#failModal').modal('show');
			}
		}
	});
}
function userInfoPage(uid)
{
	
	$.ajax({
		type : "POST",
		cache : false,
		url : url_userInfoPage,
		data : {
			'user.id':uid,
		},
		dataType : "text", //预期服务器返回的数据类型
		async : true, //true 请求为同步执行  false 请求为异步执行
		success : function(data) {
			$('#content').html(data);
		}
	});
}
function indexPage()
{
	$('.banner_nav li').removeClass("active");
	$.ajax({
		type : "POST",
		cache : false,
		url : url_indexPage,
		data : {
			
		},
		dataType : "text", //预期服务器返回的数据类型
		async : true, //true 请求为同步执行  false 请求为异步执行
		success : function(data) {
			$('.banner_nav li.collapseIndex').addClass("active");
			$('#content').html(data);
		}
	});
}
function topicList()
{
	$.ajax({
		type : "POST",
		cache : false,
		url : url_topicList,
		data : {
		},
		dataType : "text", //预期服务器返回的数据类型
		async : true, //true 请求为同步执行  false 请求为异步执行
		beforeSend: loading, 
		success : function(data) {
			loadingsuccess();
			$('#content').html(data);
		}
	});
}
function topicDelete()
{
	var title = $("input[name='tlist']");
	var tlist = new Array();

	title.each(function(){
		if(this.checked)
			tlist.push($(this).val());
	});
	$.ajax({
		type : "POST",
		cache : false,
		url : url_topicDelete,
		data : {
			'tlist[]':tlist,
		},
		dataType : "text", //预期服务器返回的数据类型
		async : true, //true 请求为同步执行  false 请求为异步执行
		beforeSend: loading, 
		success : function(data) {
			loadingsuccess();
			//$('#content').html(data);
			topicList();
		}
	});
}
function topicSearch()
{
	var username = $('#topicSearch input[name="username"]').val();
	var isHot = $('#topicSearch select[name="isHot"]').val();

	if(username==""&&isHot==""){alert("条件不能为空！");return false;}
	
	$.ajax({
		type : "POST",
		cache : false,
		url : url_topicSearch,
		data : {
			'username':username,
			'isHot':isHot,
			
		},
		dataType : "text", //预期服务器返回的数据类型
		async : true, //true 请求为同步执行  false 请求为异步执行
		beforeSend: loading, 
		success : function(data) {
			loadingsuccess();
			$('#content').html(data);
		}
	});
}
function replyDelete(tid)
{

	var title = $("input[name='rlist']");
	var rlist = new Array();
	
	title.each(function(){
		if(this.checked)
			rlist.push($(this).val());
	});
	$.ajax({
		type : "POST",
		cache : false,
		url : url_replyDelete,
		data : {
			'rlist[]':rlist,
		},
		dataType : "text", //预期服务器返回的数据类型
		async : true, //true 请求为同步执行  false 请求为异步执行
		beforeSend: loading, 
		success : function(data) {
			if(data=="success")
			{
			$('#successModal').modal('show');
			topicViewJsp(tid);
			}
			else{
				$('#failModal').modal('show');
			}
		}
	});
}
function notificationCommentList(noid)
{
	$.ajax({
		type : "POST",
		cache : false,
		url : url_notificationCommentList,
		data : {
			"noid":noid,
		},
		dataType : "text", //预期服务器返回的数据类型
		async : true, //true 请求为同步执行  false 请求为异步执行
		beforeSend: loading, 
		success : function(data) {
			loadingsuccess();
			$('#content').html(data);
		}
	});
}
function notificationCommentDelete()
{
	var noid = $("input[name='noid']").val();

	var title = $("input[name='nclist']");
	var nclist = new Array();

	title.each(function(){
		if(this.checked)
			nclist.push($(this).val());
	});
	if(nclist.length==0)
	{
		alert("没有选中任何内容！");
		return false;
	}
	console.log(nclist);
	$.ajax({
		type : "POST",
		cache : false,
		url : url_notificationCommentDelete,
		data : {
			"nclist[]":nclist,
		},
		dataType : "text", //预期服务器返回的数据类型
		async : true, //true 请求为同步执行  false 请求为异步执行
		beforeSend: loading, 
		success : function(data) {
			loadingsuccess();
			if(data=="success")
				{
				$('#successModal').modal('show');
				notificationCommentList(noid);
				
				}
			else{
				$('#failModal').modal('show');
			}
		}
	});
}
function topicViewJsp(tid)
{
	$.ajax({
		type : "POST",
		cache : false,
		url : url_topicViewJsp,
		data : {
			"tid":tid,
		},
		dataType : "text", //预期服务器返回的数据类型
		async : true, //true 请求为同步执行  false 请求为异步执行
		beforeSend: loading, 
		success : function(data) {
			loadingsuccess();
			$('#content').html(data);
		}
	});
}
function topicSetHot(tid)
{
	$.ajax({
		type : "POST",
		cache : false,
		url : url_topicSetHot,
		data : {
			"tid":tid,
		},
		dataType : "text", //预期服务器返回的数据类型
		async : true, //true 请求为同步执行  false 请求为异步执行
		beforeSend: loading, 
		success : function(data) {
			loadingsuccess();
			if(data=="success")
				{
				$('#successModal').modal('show');
				topicList();
				
				}
			else{
				$('#failModal').modal('show');
			}
		}
	});
}
function topicSetNormal(tid)
{
	$.ajax({
		type : "POST",
		cache : false,
		url : url_topicSetNormal,
		data : {
			"tid":tid,
		},
		dataType : "text", //预期服务器返回的数据类型
		async : true, //true 请求为同步执行  false 请求为异步执行
		beforeSend: loading, 
		success : function(data) {
			loadingsuccess();
			if(data=="success")
			{
				$('#successModal').modal('show');
				topicList();
				
			}
			else{
				$('#failModal').modal('show');
			}
		}
	});
}
function messagePushPage() {
	
	$.ajax({
		type : "POST",
		cache : false,
		url : url_messagePushPage,
		data : {
		},
		dataType : "text", //预期服务器返回的数据类型
		async : true, //true 请求为同步执行  false 请求为异步执行
		success : function(data) {
			
			$('#content').html(data);
		}
	});
}
function messagePush()
{
	var message=ue.getContentTxt();
	var title = $("input[name='title']").val();
	//var title = $("input[name='notification.title']").val();
	//var picture = $("input[name='notification.picture']").val();
	$.ajax({
		type : "POST",
		cache : false,
		url : url_messagePushStream,
		data : {
			'content':message,
			'title':title
		},
		dataType : "text", //预期服务器返回的数据类型
		async : true, //true 请求为同步执行  false 请求为异步执行
		beforeSend: loading, 
		success : function(data) {
			loadingsuccess();
			if(data=="success")
				$('#successModal').modal('show');
			else{
				$('#failModal').modal('show');
			}
		}
	}); 
}
function messageList()
{
	$.ajax({
		type : "POST",
		cache : false,
		url : url_messageList,
		data : {
			
		},
		dataType : "text", //预期服务器返回的数据类型
		async : true, //true 请求为同步执行  false 请求为异步执行
		beforeSend: loading, 
		success : function(data) {
			loadingsuccess();
			$('#content').html(data);
		}
	});
}
function wantedHouseList()
{
	$.ajax({
		type : "POST",
		cache : false,
		url : url_wantedHouseList,
		data : {
			
		},
		dataType : "text", //预期服务器返回的数据类型
		async : true, //true 请求为同步执行  false 请求为异步执行
		beforeSend: loading, 
		success : function(data) {
			loadingsuccess();
			$('#content').html(data);
		}
	});
}
function rentHouseList()
{
	$.ajax({
		type : "POST",
		cache : false,
		url : url_rentHouseList,
		data : {
			
		},
		dataType : "text", //预期服务器返回的数据类型
		async : true, //true 请求为同步执行  false 请求为异步执行
		beforeSend: loading, 
		success : function(data) {
			loadingsuccess();
			$('#content').html(data);
		}
	});
}
function interestGroupList()
{
	$.ajax({
		type : "POST",
		cache : false,
		url : url_interestGroupList,
		data : {
			
		},
		dataType : "text", //预期服务器返回的数据类型
		async : true, //true 请求为同步执行  false 请求为异步执行
		beforeSend: loading, 
		success : function(data) {
			loadingsuccess();
			$('#content').html(data);
		}
	});
}
function interestGroupViewJsp(igid)
{
	$.ajax({
		type : "POST",
		cache : false,
		url : url_interestGroupViewJsp,
		data : {
			"igid":igid,
		},
		dataType : "text", //预期服务器返回的数据类型
		async : true, //true 请求为同步执行  false 请求为异步执行
		beforeSend: loading, 
		success : function(data) {
			loadingsuccess();
			$('#content').html(data);
		}
	});
}
