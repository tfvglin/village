﻿    <!--  commons/sidebar.jsp begin -->
    	<div class="span3" style="margin-left:10px; width:19%;">
      		<div class="panel-group" id="accordion">
  				<div class="panel panel-default">
    				<div class="panel-heading">
      					<h4 class="panel-title">
        					<span class="glyphicon glyphicon-user"></span>&nbsp;&nbsp;<a class="hone" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">用户管理<div class="pull-right"><span class="right glyphicon glyphicon-chevron-down" style="font-size:12px;"></span></div></a>
      					</h4>
    				</div>
   	 				<div id="collapseOne" class="panel-collapse collapse">
      					<div class="panel-body">
        					<a href='javascript:void(0)'  onclick="userList()"><p>用户列表</p></a>
        					<a href='javascript:void(0)'  onclick="interestGroupList()"><p>圈子列表</p></a>
        					<!-- <a href="#"><p>...</p></a> -->
      					</div>
    				</div>
  				</div>
 
  				<div class="panel panel-default">
    				<div class="panel-heading">
      					<h4 class="panel-title">
        					<span class="glyphicon glyphicon-file"></span>&nbsp;&nbsp;<a class="hone" data-toggle="collapse" data-parent="#accordion" href='#collapseTwo'>社区论坛<div class="pull-right"><span class="right glyphicon glyphicon-chevron-down" style="font-size:12px;"></span></div></a>
      					</h4>
    				</div>
    				<div id="collapseTwo" class="panel-collapse collapse">
      					<div class="panel-body">
        					<a href="javascript:void(0)" onclick="topicList()"><p>话题列表</p></a>
        					<!-- <a href="#"><p>...</p></a> -->
        				</div>
    				</div>
  				</div>
				
  				<div class="panel panel-default">
    				<div class="panel-heading">
      					<h4 class="panel-title">
        					<span class="glyphicon glyphicon-comment"></span>&nbsp;&nbsp;<a class="hone" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">新闻管理<div class="pull-right"><span class="right glyphicon glyphicon-chevron-down" style="font-size:12px;"></span></div></a>
      					</h4>
    				</div>
    				<div id="collapseThree" class="panel-collapse collapse">
      					<div class="panel-body">
        					<a href='javascript:void(0)' onclick="notificationIssuePage()"><p>发布新闻</p></a>
        					<a href='javascript:void(0)'  onclick="notificationList()"><p>新闻列表</p></a>
      					</div>
    				</div>
  				</div>
  				<div class="panel panel-default">
    				<div class="panel-heading">
      					<h4 class="panel-title">
        					<span class="glyphicon glyphicon-th"></span>&nbsp;&nbsp;<a class="hone" data-toggle="collapse" data-parent="#accordion" href="#collapseFour">推送管理<div class="pull-right"><span class="right glyphicon glyphicon-chevron-down" style="font-size:12px;"></span></div></a>
      					</h4>
    				</div>
    				<div id="collapseFour" class="panel-collapse collapse">
      					<div class="panel-body">
        				
        					<a href='javascript:void(0)' onclick="messagePushPage()"><p>推送消息</p></a>
      					
        					<a href='javascript:void(0)' onclick="messageList()"><p>消息列表</p></a>
      					</div>
    				</div>
  				</div>
  				
  				<div class="panel panel-default">
    				<div class="panel-heading">
      					<h4 class="panel-title">
        					<span class="glyphicon glyphicon-user"></span>&nbsp;&nbsp;<a class="hone" data-toggle="collapse" data-parent="#accordion" href="#collapseFive">租房找房<div class="pull-right"><span class="right glyphicon glyphicon-chevron-down" style="font-size:12px;"></span></div></a>
      					</h4>
    				</div>
   	 				<div id="collapseFive" class="panel-collapse collapse">
      					<div class="panel-body">
        					<a href='javascript:void(0)'  onclick="wantedHouseList()"><p>找房列表</p></a>
        					<a href='javascript:void(0)'  onclick="rentHouseList()"><p>租房列表</p></a>
        					
      					</div>
    				</div>
  				</div>
			</div>
			
			<div class="img-rounded">
				<img src="">
			</div>
    	</div>
    	    <!--  commons/sidebar.jsp end -->