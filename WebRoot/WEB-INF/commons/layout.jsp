﻿<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">

<title>智慧社区管理平台</title>

<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="village admin index">



<link href="public/css/index.css" rel="stylesheet" media="screen" type="text/css" />
<link href="public/css/admin.css" rel="stylesheet" media="screen" type="text/css" />
 <link href="public/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="public/css/bootstrap-responsive.css" rel="stylesheet" type="text/css" />

<link rel="stylesheet" href="public/uploadify/uploadify.css" type="text/css"></link>
<link href="http://libs.baidu.com/bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet">

<script type="text/javascript" src="public/js/jquery-1.11.1.js"></script>
<s:include value="/WEB-INF/jsp/script_url.jsp"/>
<script type="text/javascript" src="public/js/uploadify.js"></script>
<script type="text/javascript" src="public/js/ajax.js"></script>
<script type="text/javascript" src="public/js/share.js"></script>


<script src="public/js/bootstrap.js" type="text/javascript"></script>
<script src="http://libs.baidu.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>

<script type="text/javascript" src="public/uploadify/jquery.uploadify.js"></script>

<!--icon文件-->
<link rel="shortcut icon" href="favicon.ico">
<link rel="Bookmark" href="favicon.ico">
<!--icon文件-->		
</head>


<!-- JCROP -->
<script src="public/jcrop/js/jquery.Jcrop.js"></script>
<link rel="stylesheet" href="public/jcrop/css/jquery.Jcrop.css" type="text/css" />

<!-- ueditor -->
<script type="text/javascript" charset="utf-8"
	src="public/Ueditor/ueditor.config.js"></script>
<script type="text/javascript" charset="utf-8"
	src="public/Ueditor/ueditor.all.js">
</script>
<!--建议手动加在语言，避免在ie下有时因为加载语言失败导致编辑器加载失败-->
<!--这里加载的语言文件会覆盖你在配置项目里添加的语言类型，比如你在配置项目里配置的是英文，这里加载的中文，那最后就是中文-->
<script type="text/javascript" charset="utf-8"
	src="public/Ueditor/lang/zh-cn/zh-cn.js"></script>

<body>
	
		<div class="head">
			<tiles:insertAttribute name="HEAD"></tiles:insertAttribute>
		</div>

		<div class="center">
			<div class="container" style="min-height:600px;">
				<div class="row">
					<tiles:insertAttribute name="SIDEBAR"></tiles:insertAttribute>
				
			
				<div id="content">
					<tiles:insertAttribute name="CONTENT"></tiles:insertAttribute>
				</div>
				
				</div>
			</div>
		</div>
		<tiles:insertAttribute name="FOOT"></tiles:insertAttribute>
		
<div id="loading_img" style=""><img src="<%=path%>/public/images/loading.gif"></div>
<div id="loading_back" ></div>
<s:include value="/WEB-INF/commons/modal.jsp"/>

</body>

</html>
