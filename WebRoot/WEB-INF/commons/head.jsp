﻿<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!--  commons/head.jsp  begin -->
<div class="header_banner">
<div class="container">

	<div class="row">
		
		<div class="span2"><p class="header_title">智慧社区管理平台</p></div>
		<div class="span6" style="margin-left:53px;">
			<div class="banner_nav">
				<ul class="nav nav-tabs" style="width:600px;">
     				<li class="active collapseIndex"><a href="javascript:void(0)" onclick="indexPage()"><span class="glyphicon glyphicon-home"></span>首页</a> </li>
     				<li class="collapseOne"><a href="javascript:void(0)"><span class="glyphicon glyphicon-user"></span>用户管理</a></li>
     				<li class="collapseTwo"><a href="javascript:void(0)"><span class="glyphicon glyphicon-file"></span>社区论坛</a></li>
     				<li class="collapseThree"><a href="javascript:void(0)"><span class="glyphicon glyphicon-comment"></span>新闻管理</a></li>
     				<li class="collapseFour"><a href="javascript:void(0)"><span class="glyphicon glyphicon-th"></span>推送管理</a></li>
     				<%-- <li class="collapseFive"><a href="javascript:void(0)"><span class="glyphicon glyphicon-th"></span>其他服务</a></li> --%>
	 			</ul>
	 		</div>
	 	</div>
		<div class="span2" style="padding-top: 45px; padding-left: 25px; margin-left: 0px;">
          当前用户登录：<span style="color: #52BBDD;font-weight:bold;"><s:property value="#session.admin.name"/></span>
      </div>
			<div class="span1" style="padding-top:35px;">
      <div class="btn-group"><a href="<%=path%>/adminLogout"><button type="button" class="btn btn-default"><!--<span class="glyphicon glyphicon-log-out"></span>--><span class="glyphicon glyphicon-off"></span>注销登录</button></a>
      </div>
	 		</div>
	</div>
		<div class="clear"></div>
</div>
</div>


<div class="clear"></div>

<!--  commons/head.jsp  end -->