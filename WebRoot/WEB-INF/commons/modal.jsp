﻿<!--  commons/modal.jsp begin -->

<!--Success Modal -->
<div id="successModal" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" >
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
    <div class="alert alert-success" role="alert">操作成功！</div>  
     <button type="button" class="btn btn-default closebtn hide" data-dismiss="modal">Close</button>
       
    </div>
  </div>
</div>
<!--Fail Modal -->
<div id="failModal" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" >
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
    <div class="alert alert-danger" role="alert">操作失败！</div>  
     <button type="button" class="btn btn-default closebtn hide" data-dismiss="modal">Close</button>
       
    </div>
  </div>
</div>

<!--  commons/modal.jsp end -->