<!--  jsp/script_url.jsp  begin -->

<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<script>

var url_indexPage="<%=path%>/indexPage";
var url_notificationCommentList="<%=path%>/nc/notificationCommentListJsp";
var url_notificationCommentDelete="<%=path%>/nc/notificationCommentDeleteStream";
var url_userList="<%=path%>/user/userListJsp";
var url_topicList="<%=path%>/topic/topicListJsp";
var url_userInfoPage="<%=path%>/user/userInfoJsp";
var url_userDelete="<%=path%>/user/userDeleteStream";
var url_notificationList="<%=path%>/no/notificationListJsp";
var url_notificationModify="<%=path%>/no/notificationModifyStream";
var url_notificationDelete="<%=path%>/no/notificationDeleteStream";
var url_notificationIssue="<%=path%>/no/notificationIssueStream";
var url_notificationPushPage="<%=path%>/notificationPushPage";
var url_notificationIssuePage="<%=path%>/no/notificationIssuePage";
var url_notificationModifyPage="<%=path%>/no/notificationModifyJsp";
var url_userSearch="<%=path%>/user/userSearchJsp";
var url_notificationSearch="<%=path%>/no/notificationSearchJsp";
var uploadifyswf = "<%=path%>/public/uploadify/uploadify.swf";
var url_notificationUpload = "<%=path%>/notificationUpload";
var url_notificationImgSave="<%=path%>/notificationImgSave";
var url_topicViewJsp="<%=path%>/topic/topicViewJsp";
var url_topicDelete="<%=path%>/topic/topicDeleteStream";
var url_topicSetHot="<%=path%>/topic/topicSetHotStream";
var url_topicSetNormal="<%=path%>/topic/topicSetNormalStream";
var url_topicSearch="<%=path%>/topic/topicSearchJsp";
var url_messageList="<%=path%>/message/messageListJsp";
var url_messagePushPage="<%=path%>/message/messagePushPage";
var url_messagePushStream="<%=path%>/message/messagePushStream";
var url_replyDelete="<%=path%>/reply/replyDeleteStream";
var url_wantedHouseList="<%=path%>/wh/wantedHouseListJsp";
var url_rentHouseList="<%=path%>/rh/rentHouseListJsp";
var url_interestGroupList="<%=path%>/ig/interestGroupListJsp";
var url_interestGroupViewJsp="<%=path%>/ig/interestGroupViewJsp";


var url_test = "<%=path%>/test";
var url_verifyCode = "<%=path%>/getRandomCode";

</script>
<!--  jsp/script_url.jsp  end -->