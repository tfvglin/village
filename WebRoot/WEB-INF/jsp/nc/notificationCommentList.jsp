﻿<!-- jsp/notificationList.jsp begin -->

<%@ taglib uri="/struts-tags" prefix="s"%>
<%@ taglib uri="http://edu.xidian.village.datejstl/tags" prefix="date"%>
<div class="span9" style="margin-left: 15px;">


	
    	<div class="btn-group">
    		<button type="button" onclick="checkAll()" class="btn btn-default">全选</button>
    		
    		<button type="button" onclick="checkInverse()" class="btn btn-default">反选</button>
    		
    		<button type="button" class="btn btn-default" onclick="notificationCommentDelete()">删除</button>
    
    		
    	</div>
		<button type="button" class="btn btn-default" onclick="notificationList()">返回</button>
    	<div class="middle_table"><table class="table table-hover">
   			<!--<caption>悬停表格布局</caption>-->
   			<thead>
      			<tr >
         			<th style="text-align:center;">序号</th>
        	 		<th style="text-align:center;">评论人</th>
        	 		<th style="width:50%;text-align:center;">内容</th>
         			<th style="text-align:center;">评论时间</th>
         			
      			</tr>
   			</thead>
   			<tbody style="font-size:12px;">
   			<s:set name="num" value="(#session.pager.curentPageIndex-1)*10" />
      			<s:iterator value="#session.pager.smallList" var="n">
      			<input type="hidden" value="<s:property value="#n.notification.id" />" name="noid"/>
				<tr >
					<s:set name="num" value="#num+1" />
					<td style="text-align:center;" >
					<input type="checkbox" style="width: 12px; margin-top: 2px;" name="nclist" value="<s:property value="#n.id" />"/>
					<s:property value="#num" /></td>
					<td style="text-align:center;"><s:property value="#n.user.name" /></td>

					<td style="text-align:center;">
						
						<s:if test="#n.content.length()>30"><s:property value="#n.content.substring(0,29)"/><span>.........</span></s:if>
						<s:else><s:property value="#n.content" /></s:else>
						
					</td>
					<td style="text-align:center;">
						<s:date name="#n.datetime" format="yyyy-MM-dd" />
						<date:date parttern="yyyy-MM-dd" value="${n.datetime}"></date:date>
					</td>
					
				</tr>
			</s:iterator>
      			
   			</tbody>

		</table>
			
		</div>
			<div class="well" style="margin-top:5px;padding:10px 15px;">
			<s:include value="/WEB-INF/jsp/pager.jsp"></s:include>
			</div>
    	</div>

<!-- jsp/notificationList.jsp end -->

