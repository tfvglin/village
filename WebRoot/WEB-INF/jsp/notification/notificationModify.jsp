﻿<!--  jsp/notificationModify.jsp begin -->

<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>

<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!-- ueditor -->
<script type="text/javascript" charset="utf-8" src="public/Ueditor/ueditor.config.js"></script>
<script type="text/javascript" charset="utf-8" src="public/Ueditor/ueditor.all.js"> </script>
<!--建议手动加在语言，避免在ie下有时因为加载语言失败导致编辑器加载失败-->
<!--这里加载的语言文件会覆盖你在配置项目里添加的语言类型，比如你在配置项目里配置的是英文，这里加载的中文，那最后就是中文-->
<script type="text/javascript" charset="utf-8" src="public/Ueditor/lang/zh-cn/zh-cn.js"></script>
<script type="text/javascript">

var options = {
    toolbars: [
    ['fullscreen', 'source', 'undo', 'redo', 'bold', 'italic', 'underline', 'fontborder', 'backcolor', 'fontsize', 'fontfamily', 'justifyleft', 'justifyright', 'justifycenter', 'justifyjustify', 'strikethrough', 'superscript', 'subscript', 'removeformat', 'formatmatch', 'autotypeset', 'blockquote', 'pasteplain', '|', 'forecolor', 'backcolor', 'insertorderedlist', 'insertunorderedlist', 'selectall', 'cleardoc', 'link', 'unlink', 'emotion', 'help']
    ]
    };
  var ue = UE.getEditor('editor',options);





</script>


<div class="span9" style="margin-left: 15px;">
    	<!--<p><button>添加</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button>删除</button></p>-->
    	  <p style="font-size:16px;padding-left:5px;border-bottom:1px solid #e4e4e4;width:16%;"><span class="glyphicon glyphicon-pencil" style="font-size:14px;"></span> 编辑内容 
    	 </p>
		  
    	 
		<div class="well" style="margin-bottom: 10px;">
			<span class="glyphicon glyphicon-picture"></span> 新闻图片<s:if test="#request.no.picture!=''"><img src="<%=path%>/uploads/notification/<s:property value="#request.no.picture"/>.jpg"  class="show_picture"/></s:if>
			<s:else>没有上传</s:else>
			<!--<div style="margin-left:40%;margin-top:25px;"><button type="button" class="btn btn-default" onclick="notificationList()" ><span class="glyphicon glyphicon-arrow-left"></span> 返回</button></div>-->
		  </div>
          <div class="middle_table">         
          
              <form class="bs-example bs-example-form" role="form">
     
              <div class="input-group" style="margin: 4px 12px 10px;">
                  <span class="input-group-addon">标题</span>
                  <input type="text" class="form-control" placeholder="输入标题" name="notification.title" value="<s:property value="#request.no.title"/>">
              </div> 
              <input type="hidden" name="notification.id" value="<s:property value="#request.no.id"/>"/>
           <!--     <textarea id="editor" style="height:400px;"></textarea>
             -->    <script id="editor" name="notification.message" type="text/plain" style="height:400px;">
<s:property value="#request.no.message" escape='false'/>
				</script>
                   <p style="margin-top: 15px; margin-left: 39%;"><button type="button" class="btn btn-default" onclick="notificationList()" ><span class="glyphicon glyphicon-arrow-left"></span> 返回</button> &nbsp;&nbsp;&nbsp;<button type="button" onclick="notificationModify()" class="btn btn-default"><span class="glyphicon glyphicon-saved"></span> 保存修改</button></p>
                 
              </form>
              <div>
              </div>
          </div>
    	</div>
    	<!--  jsp/notificationModify.jsp end -->