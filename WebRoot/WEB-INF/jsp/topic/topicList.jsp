﻿<!-- jsp/notificationList.jsp begin -->

<%@ taglib uri="/struts-tags" prefix="s"%>
<%@ taglib uri="http://edu.xidian.village.datejstl/tags" prefix="date"%>


	<div class="span9" style="margin-left: 15px;">
		<div class="well" style="margin-bottom:10px;">
			<div class="search">	
				<form id="topicSearch" method="post" >
					<!--<span>发布人<input type="text"  name="username"/> </span>-->
					<div class="span2" style="margin: 0px 10px 0px 0px ;">
						<div class="input-group">
							<span class="input-group-addon">发布人</span> 
							<input type="text" class="form-control" name="username">
						</div>
					</div>
					
					<div>
					
						<span class="pull-left" style="margin: 8px 5px;">状态</span>
						<div class="span2" style="margin-left:0px;">
						
							<select name="isHot" class="form-control">
								<option value="" selected>请选择话题状态</option>
								<option value="y">热门话题</option>
								<option value="n">普通话题</option>
							</select>
						
						</div>
						&nbsp;<button type="button" id="search-btn" class="btn btn-default" onclick="topicSearch()"><span class="glyphicon glyphicon-search"></span> 搜索</button>
					</div>
					<!--<span><input type="button" id="search-btn" value="搜索" onclick="topicSearch()" /> </span>-->
					
				</form>	
			</div>
		</div>
    	<div class="btn-group">
    		<button type="button" onclick="checkAll()" class="btn btn-default">全选</button>
    		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    		<button type="button" onclick="checkInverse()" class="btn btn-default">反选</button>
    		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    		<button type="button" class="btn btn-default" onclick="topicDelete()">删除</button>
    	
    	</div>
    	<div class="middle_table"><table class="table table-hover">
   			<!--<caption>悬停表格布局</caption>-->
   			<thead>
      			<tr >
         			<th>序号</th>
        	 		<th>发布者</th>
        	 		<th>内容</th>
         			<th>发布时间</th>
         			<th>状态</th>
         			<th>评论数</th>
         			<th>点赞数</th>
         			
      			</tr>
   			</thead>
   			<tbody>
   			<s:set name="num" value="(#session.pager.curentPageIndex-1)*10" />
      			<s:iterator value="#session.pager.smallList" var="n">
				<tr >
					<s:set name="num" value="#num+1" />
					<td>
					<input type="checkbox" name="tlist" style="width: 12px; " value="<s:property value="#n.id" />"/>
					<s:property value="#num" /></td>
					<td><s:property value="#n.landlord.name" /></td>

					<td>
						<a href="javascript:void(0)" onclick="topicViewJsp(<s:property value="#n.id" />)">
							<s:if test="#n.message.length()>20"><s:property value="#n.message.substring(0,19)"/>....</s:if>
							<s:else><s:property value="#n.message" /></s:else>
						
						</a>
					</td>
					<td>
						<s:date name="#n.datetime" format="yyyy-MM-dd" />
						<date:date parttern="yyyy-MM-dd" value="${n.datetime}"></date:date>
					</td>
					<td><s:if test="#n.isHot=='y'"><span style="color:red;">热门 &nbsp;&nbsp;</span><a href="javascript:void(0)" onclick="topicSetNormal(<s:property value="#n.id" />)" title="设置为普通话题" data-toggle="tooltip" data-placement="right"><span class="glyphicon glyphicon-star-empty"></span><span class="glyphicon glyphicon-star-empty"></span></a></s:if>
						<s:else>普通 &nbsp;&nbsp;<a href="javascript:void(0)" onclick="topicSetHot(<s:property value="#n.id" />)" title="设置为热门话题" data-toggle="tooltip" data-placement="right"><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span></a></s:else>	</td>
				
					<td>
						<s:property value="#n.reply.size()"/>
					</td>
				
					<td>
						<s:property value="#n.praise_num"/>	
					</td>
				
				</tr>
			</s:iterator>
      			
   			</tbody>

		</table></div>
		<script>
			$(function () { $("[data-toggle='tooltip']").tooltip(); });
			
		</script>
			<div class="well" style="margin-top:5px;padding:10px 15px;">
			
			
				<s:include value="/WEB-INF/jsp/pager.jsp"></s:include>
			</div>
    	</div>

<!-- jsp/notificationList.jsp end -->

