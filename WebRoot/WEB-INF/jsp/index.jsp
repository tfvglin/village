﻿<!--  jsp/index.jsp -->
<%@ taglib uri="/struts-tags" prefix="s"%>
<%@ taglib uri="http://edu.xidian.village.datejstl/tags" prefix="date"%>
<div class="span9" style="margin-left:20px;margin-top:5px;">
<div class="panel panel-default">
	<div class="panel-heading">
		<h1 class="panel-title"><span class="glyphicon glyphicon-paperclip"></span> 首页信息</h1>
		<div class="panel-body">
			西电社区管理员守则：<br />
			1、不得泄露用户个人信息<br />
			2、不得为个人利益向用户推送广告信息<br />
			3、不得散播淫秽信息<br />
			4、全心全意为社区美好生活服务<br />

		</div>
	</div>
</div>
<div class="span4" style="margin-left:0px;">
<div class="panel panel-default">
	<div class="panel-heading" >
		<h1 class="panel-title"><span class="glyphicon glyphicon-cog"></span> 系统信息</h1>
		<div class="panel-body">
			登陆时间：<date:date parttern="yyyy-MM-dd" value="${session.logintime}"></date:date>  <br />
			注册用户数：<s:property value="#session.usernum" /> <br />
			当日活跃用户数：<s:property value="#session.activeusernum" /> <br />
			新闻通知数：<s:property value="#session.notificationnum" /><br />
		
			
			

		</div>
	</div>
</div>
</div>
<div class="span5">
<div class="panel panel-default">	
	<div class="panel-heading">
		<h1 class="panel-title"><span class="glyphicon glyphicon-signal"></span> 服务状态</h1>
		<div class="panel-body">

			物业服务：正常 <span class="glyphicon glyphicon-ok"></span><br />
			失物招领服务：正常 <span class="glyphicon glyphicon-ok"></span><br />
			电话查询：正常 <span class="glyphicon glyphicon-ok"></span><br />
			找房租房：正常 <span class="glyphicon glyphicon-ok"></span><br />
			

		</div>
	</div>
</div>
</div>
</div>