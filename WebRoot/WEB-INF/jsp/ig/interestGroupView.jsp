﻿<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ taglib uri="http://edu.xidian.village.datejstl/tags" prefix="date"%>
<div class="span9" style="margin-left: 15px;">
	<p style="font-size:16px; padding-left:5px; border-bottom:1px solid #e4e4e4;width:16%;" ><span class="glyphicon glyphicon-pushpin" style="font-size:14px;"></span> 话题</p>
	  
    	 
	<div class="well" style="margin-bottom:15px;">
	
		<p>创建人：<span class="topic_contents"><s:property value="#request.interestGroup.creatUser.name"/></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;创建时间：<span class="topic_contents"><date:date parttern="yyyy-MM-dd" value="${request.interestGroup.datetime}"></date:date></span></p>	
		
		<p>介绍：<span class="topic_contents"><s:property value="#request.interestGroup.introduce"/></span></p>
		<p>公告：<span class="topic_contents"><s:property value="#request.interestGroup.message"/></span></p>
		
		<p>图片：
			<s:if test="#request.interestGroup.picture==''">
				没有上传图片
				</s:if>
				<s:else>
				<img src="uploads/interestGroup/<s:property value='#request.interestGroup.picture'/>.jpg" class="show_picture1"/>
			</s:else>
		</p>
		<br />
		<span style="margin-left:40%;"><button type="button" class="btn btn-default" onclick="interestGroupList()"><span class="glyphicon glyphicon-arrow-left"></span> 返回</button></span>
	</div>
	<h4 style="border-left:3px solid #52bbdd; border-bottom:1px solid #e4e4e4;padding:4px; font: 15px 微软雅黑;">成员列表</h4>
	<div class="btn-group">
		<button type="button" onclick="checkAll()" class="btn btn-default">全选</button>
		<button type="button" onclick="checkInverse()" class="btn btn-default">反选</button>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	</div>
	<div class="middle_table">
		<table class="table table-hover">
   			<!--<caption>悬停表格布局</caption>-->
   			<thead>
      			<tr>
         			<th>序号</th>
        	 		<th>用户名</th>
         			<th>真实姓名</th>
         			<th>电话</th>
         			<th>邮箱</th>
      			</tr>
   			</thead>
			<tbody>
				<s:set name="num" value="(#session.pager.curentPageIndex-1)*10" />
				<s:iterator value="#session.pager.smallList" var="u" >
				<s:set name="num" value="#num+1" />
				<tr >
   				<td>
   					<input type="checkbox" name="rlist" style="width:12px;" value="<s:property value="#u.id" />">
   					<s:property value="#num" />
   				</td>
   				<td>
   					<s:property value="#u.name" />
   				</td>
   			
   				<td>
   					<s:property value="#u.realName" />
   				</td>
   				<td>
   					<s:property value="#u.phone" />
   				</td>
   				<td>
   					<s:property value="#u.email" />
   				</td>
   				</tr>
				</s:iterator>		
			</tbody>
		</table>	
	</div>	
	<div class="well" style="margin-top:5px;padding:10px 15px;">
	<s:include value="/WEB-INF/jsp/pager.jsp"></s:include>
	</div>
</div>