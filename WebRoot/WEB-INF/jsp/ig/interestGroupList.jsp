﻿<!-- jsp/rentHouseList.jsp begin -->

<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ taglib uri="http://edu.xidian.village.datejstl/tags" prefix="date"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<base href="<%=basePath%>">


	</head>
  
	<body>
		<div class="span9" style="margin-left: 15px;">
			<div class="search">
				<div class="well" style="margin-bottom:10px;">	
					<form id="notificationSearch" method="post"  class="form-inline" role="form">
						<!--<span>标题<input type="text"  name="title"/> </span>
						<span><input type="button" id="search-btn" value="搜索" onclick="notificationSearch()" /> </span> -->
						<div class="form-group" >
							<!--<span class="input-group-addon">用户名</span><input type="text" class="form-control" name="user.name"/>-->
							<label class="sr-only" for="pass">名称</label>
							<input type="text" class="form-control"  name="title" placeholder="名称">
						</div> 
						
						<button type="button" id="search-btn" class="btn btn-default" onclick="notificationSearch()"><span class="glyphicon glyphicon-search"></span> 搜索</button>
						
					</form>		
				</div>
			</div>	
			<div class="btn-group">
				<button type="button" onclick="checkAll()" class="btn btn-default">全选</button>   		
				<button type="button" onclick="checkInverse()" class="btn btn-default">反选</button>    		
				<button type="button" class="btn btn-default" onclick="">删除</button>    	
			</div>
			<div class="btn-group">
				<button type="button" class="btn btn-default" onclick="rentHouseList()" >返回</button>
			</div>
			<div class="middle_table"><table class="table table-hover">
				<!--<caption>悬停表格布局</caption>-->
			<thead>
				<tr >
         			<th>序号</th>
        	 		<th>组名</th>
        	 		<th>创建人</th>
					<th>简介</th>
         			<th>创建时间</th>
         			<!--<th>是否有图片</th>-->
         			<th>当前人数</th>
         			<th>极光ID</th>
					
      			</tr>
   			</thead>
   			<tbody>
   			<s:set name="num" value="(#session.pager.curentPageIndex-1)*10" />
      			<s:iterator value="#session.pager.smallList" var="n">
				<tr >
					<s:set name="num" value="#num+1" />
					<td>
					<input type="checkbox" name="nolist" style="width:12px;" value="<s:property value="#n.id"/>"/>
					<s:property value="#num" />
					</td>
					
					<td>
						<a href="javascript:void(0)" onclick="interestGroupViewJsp(<s:property value="#n.id" />)">
						<s:property value="#n.name" />
						</a>
					</td>

					<td>
						<s:property value="#n.creatUser.name" />
					</td>
					<td><s:property value="#n.introduce" /></td>
					<td>
						<s:date name="#n.datetime" format="yyyy-MM-dd" />
						<date:date parttern="yyyy-MM-dd" value="${n.datetime}"></date:date>
					</td>
					<!--<td><s:if test="#n.picture!=''">有</s:if><s:else> 无</s:else>	</td>-->
					<td>
						<s:property value="#n.member.size()" />
					</td>
					<td>
						<s:property value="#n.gid" />
					</td>
				
				</tr>
			</s:iterator>
      			
   			</tbody>

		</table>
			
		</div>
			<div class="well" style="margin-top:5px;padding:10px 15px;">
			<s:include value="/WEB-INF/jsp/pager.jsp"></s:include>
			</div>
    	</div>
	</body>
</html>
