﻿<!-- jsp/notification.jsp begin -->

<!-- ueditor -->
<script type="text/javascript" charset="utf-8"
	src="public/Ueditor/ueditor.config.js"></script>
<script type="text/javascript" charset="utf-8"
	src="public/Ueditor/ueditor.all.js"></script>

<script type="text/javascript">
	var options = {
          toolbars: [
               ['fullscreen', 'source', 'undo', 'redo', 'bold', 'italic', 'underline', 'fontborder', 'backcolor', 'fontsize', 'fontfamily', 'justifyleft', 'justifyright', 'justifycenter', 'justifyjustify', 'strikethrough', 'superscript', 'subscript', 'removeformat', 'formatmatch', 'autotypeset', 'blockquote', 'pasteplain', '|', 'forecolor', 'backcolor', 'insertorderedlist', 'insertunorderedlist', 'selectall', 'cleardoc', 'link', 'unlink', 'emotion', 'help']
          		]
    		};
		var ue= UE.getEditor('editor',options);
  	</script>
<div class="span9" style="margin-left: 15px;">
<p style="font-size:16px;padding-left:5px;border-bottom:1px solid #e4e4e4;width:16%;"><span class="glyphicon glyphicon-pushpin" style="font-size:14px;"></span> 消息推送</p>
<div class="middle_table">


	<form method="post" action="">
		<div class="input-group" style="margin: 12px 18px;">
			<span class="input-group-addon">标题</span><input type="text" class="form-control" placeholder="输入标题" name="title"><!--<input type="text" name="title" />-->
		</div>
		<textarea id="editor" name="editor" style="width: 96%;height: 400px;margin-left: 2%;border: 1px solid #cfcfcf;border-radius:4px;margin-top: 12px;box-shadow: 0 0 8px #e4e4e4;padding: 15px;"></textarea>
		<!--<input class="btn btn-default" type="submit" value="确认提交" style="margin:10px 0 0 610px;">-->
		<p style="margin-top: 15px; margin-left: 44%;">
			<button type="button" class="btn btn-default" onclick="messagePush()"><span class="glyphicon glyphicon-send"></span> 确认提交</button>
		</p>
		
	</form>
</div>
</div>

<!-- jsp/notification.jsp end -->