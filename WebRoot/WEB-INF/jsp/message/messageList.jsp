﻿<!-- jsp/notificationList.jsp begin -->

<%@ taglib uri="/struts-tags" prefix="s"%>
<%@ taglib uri="http://edu.xidian.village.datejstl/tags" prefix="date"%>
<div class="span9" style="margin-left: 15px;">
<div class="search">
<div class="well" style="margin-bottom:10px;">	
<form id="notificationSearch" method="post"  class="form-inline" role="form">
	<!--<span>标题<input type="text"  name="title"/> </span>
	<span><input
		type="button" id="search-btn" value="搜索" onclick="notificationSearch()" /> </span> -->
	<div class="form-group" >
			<!--<span class="input-group-addon">用户名</span><input type="text" class="form-control" name="user.name"/>-->
			<label class="sr-only" for="pass">标题</label>
			<input type="text" class="form-control"  name="title" placeholder="标题">
			<button type="button" id="search-btn" class="btn btn-default" onclick="messageSearch()"><span class="glyphicon glyphicon-search"></span> 搜索</button>
	</div> 
</form>		
</div>
</div>

	
    	<div class="btn-group">
    		<button type="button" onclick="checkAll()" class="btn btn-default">全选</button>
    		
    		<button type="button" onclick="checkInverse()" class="btn btn-default">反选</button>
    		
    		<button type="button" class="btn btn-default" onclick="messageDelete()">删除</button>
    		 
    	
    	</div>
		<button type="button" class="btn btn-default" onclick="messageList()" >返回</button>
    	<div class="middle_table"><table class="table table-hover">
   			<!--<caption>悬停表格布局</caption>-->
   			<thead>
      			<tr >
         			<th>序号</th>
        	 		<th>标题</th>
        	 		<th style="width:50%;">消息</th>
         			<th>发布时间</th>
         		
      			</tr>
   			</thead>
   			<tbody>
   			<s:set name="num" value="(#session.pager.curentPageIndex-1)*10" />
      			<s:iterator value="#session.pager.smallList" var="n">
				<tr >
					<s:set name="num" value="#num+1" />
					<td>
					<input type="checkbox" name="nolist" style="width: 12px; " value="<s:property value="#n.id" />"/>
					<s:property value="#num" /></td>
					<td><s:property value="#n.title" /></td>

					<td>
						<a href="javascript:void(0)" onclick="notificationModifyPage(<s:property value="#n.id" />)">
						<s:if test="#n.content.length()>20"><s:property value="#n.content.substring(0,19)"/><span>.....</span></s:if>
						<s:else><s:property value="#n.content" /></s:else>
						</a>
					</td>
					<td>
						<s:date name="#n.datetime" format="yyyy-MM-dd" />
						<date:date parttern="yyyy-MM-dd" value="${n.datetime}"></date:date>
					</td>
					<%-- <td><s:if test="#n.picture!=''">有</s:if><s:else> 无</s:else>	</td> --%>
				<%-- 	<td>
					<a href="javascript:void()" onclick="notificationCommentList(<s:property value="#n.id"/>)">
					<s:property value="#n.notificationComment.size()"/>
					</a>
					</td> --%>
				</tr>
			</s:iterator>
      			
   			</tbody>

		</table>
			
		</div>
			<div class="well" style="margin-top:5px;padding:10px 15px;">
			<s:include value="/WEB-INF/jsp/pager.jsp"></s:include>
			</div>
    	</div>

<!-- jsp/notificationList.jsp end -->

