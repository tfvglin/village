<!--  jsp/userInfo.jsp  begin -->
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s"  uri="/struts-tags"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<div class="span9" style="margin-left: 15px;">
    <div class="user_table"><table class="table table-hover">
   			<caption><span class="glyphicon glyphicon-link"></span> 用户信息</caption>
   			  
    	 
   			<tbody>
      			<tr>
         			<td>头像</td>
         			<td>
         			
         			<s:if test="#request.user.photo==1">
         			<img src="<%=path%>/uploads/userhead/<s:property value="#request.user.name"/>.jpg"/>
         			</s:if>
         			<s:else>没有上传头像</s:else>
         			</td>
         			
      			</tr>
      			<tr>
         			<td>账号</td>
         			<td><s:property value="#request.user.name"/></td>
         			
      			</tr>
      			<tr>
         			<td>真实姓名</td>
         			<td><s:property value="#request.user.realName"/></td>
         			
      			</tr>
            <tr>
              <td>邮箱</td>
              <td><s:property value="#request.user.email"/></td>
              
            </tr>
            <tr>
              <td>电话</td>
              <td><s:property value="#request.user.phone"/></td>
              
            </tr>
           <tr>
              <td>昵称</td>
              <td><s:property value="#request.user.nearName"/></td>
              
            </tr>
           
            

   			</tbody>
			
		</table>
		<p style="margin-left: 40%; margin-bottom:20px;"><button type="button" class="btn btn-default" onclick="userList()"><span class="glyphicon glyphicon-arrow-left"></span> 返回</button></p>
		</div>
    	</div>
<!--  jsp/userInfo.jsp  end -->