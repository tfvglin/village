﻿<!-- jsp/userList.jsp begin -->
<%@ taglib uri="/struts-tags" prefix="s" %>
<div class="span9" style="margin-left: 15px;">
<div class="well" style="margin-bottom:10px;">
	<form id="userSearch" method="post" action="admin/userSearch" class="form-inline" role="form">
	
		<div class="form-group" >
			<!--<span class="input-group-addon">用户名</span><input type="text" class="form-control" name="user.name"/>-->
			<label class="sr-only" for="pass">用户名</label>
			<input type="text" class="form-control"  name="user.name" placeholder="用户名">
		</div>
		<div class="form-group" >
			<!--<span class="input-group-addon">真实姓名</span><input type="text" class="form-control" name="user.realName"/>-->
			<label class="sr-only" for="pass">真实姓名</label>
			<input type="text" class="form-control"  name="user.realName" placeholder="真实姓名">
		</div>
		<div class="form-group" > 
			<!--<span class="input-group-addon">邮箱</span><input type="text" class="form-control" name="user.email" />-->
			<label class="sr-only" for="pass">邮箱</label>
			<input type="text" class="form-control"  name="user.email" placeholder="邮箱">
		
		</div>
	
	<!--<div class="pull-right" style="margin-right:20%;">
		<input type="button" id="search-btn" onclick="userSearch()" value="搜索" />-->
		
		<button type="button" id="search-btn" class="btn btn-default" onclick="userSearch()"><span class="glyphicon glyphicon-search"></span> 搜索</button>
		
	<!--</div>-->
	</form>	
</div>

<div class="btn-group">
	<button type="button" onclick="checkAll()" class="btn btn-default">全选</button>
	<button type="button" onclick="checkInverse()" class="btn btn-default">反选</button>

	<button type="button" class="btn btn-default" onclick="userDelete()">删除</button>

  
</div>
<div class="btn-group">
	<button type="button" class="btn btn-default" onclick="userList()">返回</button>
</div>
<div class="middle_table"><table class="table table-hover">
   			<!--<caption>悬停表格布局</caption>-->
   			<thead>
      			<tr>
         			<th>序号</th>
        	 		<th>账号</th>
         			<th>真实姓名</th>
         			<th>邮箱</th>
         			<th>电话</th>
      			</tr>
   			</thead>
		<tbody>
		<s:set name="num" value="(#session.pager.curentPageIndex-1)*10" />
			<s:iterator value="#session.pager.smallList" var="u" >
   			<s:set name="num" value="#num+1" />
   			<tr >
   				<td>
   					<input type="checkbox" name="ulist"  style="width: 12px;" value="<s:property value="#u.id" />">
   					<s:property value="#num" />
   				</td>
   				<td>
   					<a href="javascript:void(0)" onclick="userInfoPage(<s:property value="#u.id" />)"><s:property value="#u.name" /></a>
   				</td>
   			
   				<td>
   					<s:property value="#u.realName" />
   				</td>
   				<td>
   					<s:property value="#u.email" />
   				</td>
   				<td>
   					<s:property value="#u.phone" />
   				</td>
   				</tr>
   			</s:iterator>
		
		</tbody>
	</table>
	
	
</div>	
	<div class="well" style="margin-top:5px;padding:10px 15px;">
	<s:include value="/WEB-INF/jsp/pager.jsp"></s:include>
	</div>
</div>
<!-- jsp/userList.jsp end -->
