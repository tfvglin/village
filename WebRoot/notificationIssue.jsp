

<script>
	var timestamp = new Date().getTime(), x, y, width, height;
	$("input[name='title']").val(timestamp);
	jQuery(function($) {

		var jcrop_api, boundx, boundy, $preview = $('#preview-pane'), $pcnt = $('#preview-pane .preview-container'), $pimg = $('#preview-pane .preview-container img'), xsize = $pcnt
				.width(), ysize = $pcnt.height();

		console.log('init', [ xsize, ysize ]);

		$('#srcImg').Jcrop({
			onChange : updatePreview,
			onSelect : updatePreview,
			maxSize : [ 1000, 1000 ],
			minSize : [ 100, 100 ],

			 setSelect: [ 60, 70, 540, 330 ],
			allowResize : true,
			allowMove : true,
			aspectRatio : xsize / ysize
		}, function() {
			alert("aa");
			//	this.setImage(src);
			var bounds = this.getBounds();
			boundx = bounds[0];
			boundy = bounds[1];

			jcrop_api = this;

			// Move the preview into the jcrop container for css positioning
			$preview.appendTo(jcrop_api.ui.holder);
		});

		function updatePreview(c) {
			x = c.x;

			y = c.y;
			width = c.w;
			height = c.h;
			$("#x").val(c.x);
			$("#y").val(c.y);
			$("#w").val(c.w);
			$("#h").val(c.h);
			if (parseInt(c.w) > 0) {
				var rx = xsize / c.w;
				var ry = ysize / c.h;

				$pimg.css({
					width : Math.round(rx * boundx) + 'px',
					height : Math.round(ry * boundy) + 'px',
					marginLeft : '-' + Math.round(rx * c.x) + 'px',
					marginTop : '-' + Math.round(ry * c.y) + 'px'
				});
			}
		}

	});
	var options = {
    toolbars: [
    ['fullscreen', 'source', 'undo', 'redo', 'bold', 'italic', 'underline', 'fontborder', 'backcolor', 'fontsize', 'fontfamily', 'justifyleft', 'justifyright', 'justifycenter', 'justifyjustify', 'strikethrough', 'superscript', 'subscript', 'removeformat', 'formatmatch', 'autotypeset', 'blockquote', 'pasteplain', '|', 'forecolor', 'backcolor', 'insertorderedlist', 'insertunorderedlist', 'selectall', 'cleardoc', 'link', 'unlink', 'emotion', 'help']
    ]
    };
    UE.getEditor('editor',options);
	
</script>
<script type="text/javascript" src="public/js/uploadify.js"></script>
<div class="span9" style="margin-left:10px;">
	<p style="font-size:20px;">编辑内容</p>
	<div class="middle_table">

		<div>
			<input type="hidden" name="title" /> 
			<input type="file" id="img_notification" name="uploadify">
		</div>

		<div id="originImg">
			<img src="" id="srcImg" alt="" width="400px" height="300px" />
		</div>
		<div id="preview-pane">
			<div class="preview-container" id="newImg">
				<img src="" id="previewImg" class="jcrop-preview" alt="Preview" />
			</div>
		</div>

		<div>
			
			<input type="button" value="确认提交" onclick="notificationImgSave()" />
			<div id="label"></div>
		</div>


		 <form method="post" action="no/notificationIssue">
			<input type="hidden" name="notification.picture" value="" />
			<div class="input-group">
				<span class="input-group-addon">标题</span> 
				<input type="text" class="form-control" placeholder="输入标题">
			</div>
			<script id="editor" type="text/plain" style="width:100%;height:400px;">
			</script>

			<p style="margin-top: 15px; margin-left: 46%;">
				<button type="submit" class="btn btn-default">保存提交</button>
			</p>
		</form> 
	</div>
</div>

