﻿<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="charset=utf-8" />
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="village admin index">
<title>智慧社区管理平台</title>

<link href="<%=path%>/public/css/index.css" rel="stylesheet" media="screen" type="text/css" />
 
<link rel="shortcut icon" href="favicon.ico">
<link rel="Bookmark" href="favicon.ico"> 
<script type="text/javascript" src="<%=path%>/public/js/jquery-1.11.1.js"></script>

<s:include value="/WEB-INF/jsp/script_url.jsp"/>
<script type="text/javascript" src="<%=path%>/public/js/share.js"></script>
</head>
   
<body>
<!-- header -->
<div class="header">
	<div class="banner">
		<p><img src="<%=path%>/public/images/tubiao1.png" /><span>智慧社区管理平台</span></p>
	</div>
</div>
<!-- /header -->
<div class="clear"></div>
<div class="wrap">
	<div class="main">
		
		<div class="middle_left">
			<img src="<%=path%>/public/images/bg04.jpg" />
		</div>
		<div class="middle_right">
			<h3>&nbsp;后台登录</h3>
			<form method="post" action="<%=path%>/adminIndex">
			<p><span class="u_logo"></span><input type="text" id="account" name="admin.name" class="inputUP" placeholder="用户名或邮箱" /></p>
			<p><span class="p_logo"></span><input type="password" id="pwd" name="admin.password" class="inputUP" placeholder="密码" /></p>
			<p><input type="text" id="checkNum" name=randomCode class="inputcheck" placeholder=" 输入验证码" />&nbsp;<span id="checkNumResult"><img src="<%=path%>/getRandomCode" id="VerifyCodeImage" align="absbottom" alt="验证码" title="点击更换验证码" onclick="changeVerifyCode()"/></span></p>
			<p><input type="submit" onclick="" id="toLogin" value="登　录" /></p>
			</form>
		<s:actionmessage />
		</div>
		<div class="clear"></div>
	</div>
	
</div>

  
<!-- footer -->
<div class="clear"></div>
<div class="bottom_login">
	<div class="copyright">
		<p>Copyright © 2015 网络精灵实验室. All Rights Reserved.</p>
	</div>
</div>
<!-- /footer -->
</body>
</html>
