package action.user;

import java.util.HashSet;
import java.util.Set;

import javax.annotation.Resource;

import junit.framework.TestCase;

import org.hibernate.SessionFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.orm.hibernate3.HibernateTemplate;

import edu.xidian.village.vo.FriendRequest;
import edu.xidian.village.vo.InterestGroup;
import edu.xidian.village.vo.User;

public class UserTest extends TestCase{
	
	private SessionFactory sessionFactory;
	
	
	
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}


	@Resource
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	public void testUser()
	{
	
		ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
		UserTest userDaoImpl = (UserTest) ctx.getBean("userTest");
		HibernateTemplate ht = new HibernateTemplate(userDaoImpl.getSessionFactory());
		//Session session = notificationTest.getSessionFactory().getCurrentSession();
		FriendRequest fr = new FriendRequest();
		User user1 = ht.get(User.class, 30);
		User user2 = ht.get(User.class, 27);
		fr.setMessage("加好友");
		fr.setUserReceiver(user1);
		fr.setUserSender(user2);
		//NotificationComment nc = new NotificationComment();
		//nc.setContent("asdhf");
		//nc.setUser(user1);
		InterestGroup g = new InterestGroup();
		Set<User> set = new HashSet<User>();
		set.add(user2);
		set.add(user1);
		g.setMember(set);
		ht.save(g);
		
		
		/*User user1 = ht.get(User.class, 30);
		Set<User> fset = user1.getFriends();
		User user2 = ht.get(User.class, 29);
		fset.add(user2);
		user1.setFriends(fset);
		ht.merge(user1);*/
		//userDaoImpl.getHibernateTemplate();
	}
}
