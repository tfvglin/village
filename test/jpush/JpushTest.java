package jpush;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import junit.framework.TestCase;
import sun.misc.BASE64Encoder;
import edu.xidian.village.util.CustomizedHostnameVerifier;
public class JpushTest extends TestCase {
	 public void testuserRegist() throws Exception
	    {
	    	
		    String users="[{\"username\":\"dev_cc\", \"password\": \"password\"},"+ 
                    "{\"username\": \"dev_dd\", \"password\": \"password\"}]";
   
	        String  url =  "https://api.im.jpush.cn/v1/users";  
			
	        String responseMessage = "";  
	        StringBuffer resposne = new StringBuffer();  
	        HttpsURLConnection httpConnection = null;  
	       
	        OutputStreamWriter outputStreamWriter = null;
	        BufferedReader reader = null;  
	        try {  
	            URL urlPost = new URL(url);  
	            httpConnection = (HttpsURLConnection) urlPost.openConnection();
	   
	            httpConnection.setHostnameVerifier(new CustomizedHostnameVerifier());
	          
	            
	            httpConnection.setDoOutput(true);  
	            httpConnection.setDoInput(true);  
	          
	            httpConnection.setRequestMethod("POST");  
	          
	            httpConnection.setUseCaches(false);  
	         
	            httpConnection.setInstanceFollowRedirects(true);
	            httpConnection.setRequestProperty("Content-Type", "application/json");
	            httpConnection.setRequestProperty("Charset", "UTF-8");
	            httpConnection.setRequestProperty("Authorization", "Basic OTQzNjVkYjhkZWRlNGYwYTY2YTJlNmQ4Ojc4Y2Q1MDM4NGFiNjRkNmNkZDZlMTMxMg==");
	           
	            httpConnection.connect();  
	       
	            outputStreamWriter = new OutputStreamWriter(httpConnection.getOutputStream());
	            outputStreamWriter.write(users);
	             outputStreamWriter.flush();
	            
	            reader = new BufferedReader(new InputStreamReader(  
	                    httpConnection.getInputStream(),"utf-8"));  
	            while ((responseMessage = reader.readLine()) != null) {  
	                resposne.append(responseMessage);  
	            }  
	          //  System.out.println(resposne.toString());
	        }
	        catch(Exception e1){
	        	e1.printStackTrace();
	        	throw e1;
	        }
	        finally {  
	       
	            try {  
	                if (null != outputStreamWriter) {  
	                	outputStreamWriter.close();  
	                }  
	                if (null != reader) {  
	                    reader.close();  
	                }  
	                if (null != httpConnection) {  
	                    httpConnection.disconnect();  
	                }  
	            } catch (Exception e2) {  
	                throw e2;
	            }  
	        }  
	    }
	public String testJPush() throws Exception
	{
		String  url =  "https://api.jpush.cn/v3/push";  
		
        String responseMessage = "";  
        StringBuffer resposne = new StringBuffer();  
        HttpURLConnection httpConnection = null;  
       
        OutputStreamWriter outputStreamWriter = null;
        BufferedReader reader = null;  
        String base64Str =new BASE64Encoder().encode("94365db8dede4f0a66a2e6d8:78cd50384ab64d6cdd6e1312".getBytes());
        try {  
            URL urlPost = new URL(url);  
            httpConnection = (HttpURLConnection) urlPost.openConnection();  
            httpConnection.setDoOutput(true);  
            httpConnection.setDoInput(true);  
          
            httpConnection.setRequestMethod("POST");  
          
            httpConnection.setUseCaches(false);  
         
            httpConnection.setInstanceFollowRedirects(true);  
         
            httpConnection.setRequestProperty("Content-Type", "application/json");
            httpConnection.setRequestProperty("Charset", "UTF-8");
            httpConnection.setRequestProperty("Authorization", "Basic OTQzNjVkYjhkZWRlNGYwYTY2YTJlNmQ4Ojc4Y2Q1MDM4NGFiNjRkNmNkZDZlMTMxMg==");
           
      
            httpConnection.connect();  
            outputStreamWriter = new OutputStreamWriter(httpConnection.getOutputStream());
           String param = "{\"platform\":\"all\",\"audience\":\"all\",\"notification\":{\"alert\":\"Hi,JPush!\",\"title\":\"aaa\"}}";
            outputStreamWriter.write(param);
            outputStreamWriter.flush();
         
            reader = new BufferedReader(new InputStreamReader(  
                    httpConnection.getInputStream(),"utf-8"));  
            while ((responseMessage = reader.readLine()) != null) {  
                resposne.append(responseMessage);  
            }  
  
          
          
            System.out.println(resposne.toString());
            return resposne.toString();
        }catch(Exception e1){
        	e1.printStackTrace();
        	throw e1;
        }
        finally {  
       
            try {  
                if (null != outputStreamWriter) {  
                	outputStreamWriter.close();  
                }  
                if (null != reader) {  
                    reader.close();  
                }  
                if (null != httpConnection) {  
                    httpConnection.disconnect();  
                }  
            } catch (Exception e2) {  
                throw e2;
            }  
        }  
	}
}
