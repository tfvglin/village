package Util;

import java.text.SimpleDateFormat;
import java.util.Date;

import junit.framework.TestCase;

public class DateTest extends TestCase{

	
	public void testDate()
	{
		Long time=new Date().getTime();
		Date date = new Date(time);
		//System.out.println(date);
	       if (date != null) {
	            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
	            String returnValue = df.format(date);
	            System.out.println(returnValue);   
	        }
	}
}
